/*

 * encoder.c
 *
 *  Created on: 9 de mar de 2020
 *      Author: oscar
 */

#include "encoder.h"

//########################## ENCODER  PAYLOAD ###############################

void fn_encoder_report_frame(char * frame) {

	unsigned int int_buffer_position = 0;

	memset(frame, 0, 24);

	char buffer_longitude[6];
	char buffer_latitude[6];
	char buffer_distance[2];
	char buffer_BLEdistance[2];
	char buffer_temperature[2];
	char buffer_battery[2];
	char header_buffer[2];
	char buffer_position[2];

	decHex(st_data_sensor_e.header, header_buffer);
	check_size_info(2, header_buffer);
	strcpy(frame, header_buffer);

	decHex(st_data_sensor_e.battery, buffer_battery);
	check_size_info(2, buffer_battery);
	strcat(frame, buffer_battery);

	decHex(st_data_sensor_e.temperature, buffer_temperature);
	check_size_info(2, buffer_temperature);
	strcat(frame, buffer_temperature);

	decHex(st_data_sensor_e.latitude, buffer_latitude);
	check_size_info(6, buffer_latitude);
	strcat(frame, buffer_latitude);

	decHex(st_data_sensor_e.longitude, buffer_longitude);
	check_size_info(6, buffer_longitude);
	strcat(frame, buffer_longitude);

	decHex(st_data_sensor_e.distance, buffer_distance);
	check_size_info(2, buffer_distance);
	strcat(frame, buffer_distance);

	/*	decHex(st_data_sensor_e.angle, buffer_angle);
	 check_size_info(2, buffer_angle);
	 strcat(frame, buffer_angle);*/
	/*	if (ble_distance[0] == 0 && ble_distance[1] == 0) {
	 ble_distance[0] = 'F';
	 ble_distance[1] = 'F';
	 }*/
	strcat(buffer_BLEdistance, ble_distance);
	check_size_info(2, buffer_BLEdistance);
	strcat(frame, buffer_BLEdistance);

	if (st_data_sensor_e.us_angle <= 9 && st_data_sensor_e.us_angle >= 3
			&& st_data_sensor_e.device_pos == 0)
		int_buffer_position = st_data_sensor_e.us_angle - 1;
	else if (st_data_sensor_e.us_angle <= 9 && st_data_sensor_e.us_angle >= 3
			&& st_data_sensor_e.device_pos == 1)
		int_buffer_position = st_data_sensor_e.us_angle + 6;

	decHex(int_buffer_position, buffer_position);
	check_size_info(2, buffer_position);
	strcat(frame, buffer_position);

}

//########################## ENCODER HEXA/DEC/BIN ###############################
int OnehextoOneDec(char hex[]) {
	int i = 0;
	int decimal = 0;
	/* Find the decimal representation of hex[i] */
	if (hex[i] >= '0' && hex[i] <= '9') {
		decimal = hex[i] - 48;
	} else if (hex[i] >= 'a' && hex[i] <= 'f') {
		decimal = hex[i] - 97 + 10;
	} else if (hex[i] >= 'A' && hex[i] <= 'F') {
		decimal = hex[i] - 65 + 10;
	}

	return decimal;
}

int hexDec(char *hex) {
	int len = strlen(hex);
	int i = 0;
	//int val = 0;
	long long base = 1; //, decimal;
	//int len;

	int decimal = 0;
	int tamanho = len;
	/* Find the length of total number of hex digit */

	//len--;
	/*
	 * Iterate over each hex digit
	 */
	for (i = tamanho--; i >= 0; i--) {

		/* Find the decimal representation of hex[i] */
		if (hex[i] >= '0' && hex[i] <= '9') {
			decimal += (hex[i] - 48) * base;
			base *= 16;
		} else if (hex[i] >= 'A' && hex[i] <= 'F') {
			decimal += (hex[i] - 55) * base;
			base *= 16;
		} else if (hex[i] >= 'a' && hex[i] <= 'f') {
			decimal += (hex[i] - 87) * base;
			base *= 16;
		}
	}
	return decimal;
}

void decHex(uint32_t number, char buff[7]) {

	char hex[7]; /*bcoz it contains characters A to F*/
	memset(buff, 0x00, 7); //sizeof(buff));
	memset(hex, 0x00, 7); //sizeof(buff));
	int cnt, i;
	cnt = 0; /*initialize index to zero*/
	if (number == 0) {
		strcpy(buff, "0");
	}
	while (number > 0) {
		switch (number % 16) {
		case 10:
			hex[cnt] = 'A';
			break;
		case 11:
			hex[cnt] = 'B';
			break;
		case 12:
			hex[cnt] = 'C';
			break;
		case 13:
			hex[cnt] = 'D';
			break;
		case 14:
			hex[cnt] = 'E';
			break;
		case 15:
			hex[cnt] = 'F';
			break;
		default:
			hex[cnt] = (number % 16) + 0x30; /*converted into char value*/
		}
		number = number / 16;
		cnt++;
	}
	for (i = (cnt - 1); i >= 0; i--) {
		int j = cnt - 1 - i;
		buff[j] = hex[i];
	}
}

void decToBinary(int number, char binary[32]) {
	int cnt, i;
	int bin[32];
	cnt = 0; /*initialize index to zero*/
	while (number > 0) {
		bin[cnt] = number % 2;
		number = number / 2;
		cnt++;
	}
	for (i = (cnt - 1); i >= 0; i--)
		binary[i] = bin[i];
}

int num_hex_digits(unsigned n) {
	if (!n)
		return 1;

	int ret = 0;
	for (; n; n >>= 4) {
		++ret;
	}
	return ret;
}

void hexBin(char *hex, char *dec) {

	unsigned int i = 0, size;
	memset(dec, 0, strlen(dec));
	size = strlen(hex);
	char new_hex[size];
	strcpy(new_hex, hex);
	/* Extract first digit and find binary of each hex digit */
	for (i = 0; i < size; i++) {
		switch (new_hex[i]) {
		case '0':
			strcat(dec, "0000");
			break;
		case '1':
			strcat(dec, "0001");
			break;
		case '2':
			strcat(dec, "0010");
			break;
		case '3':
			strcat(dec, "0011");
			break;
		case '4':
			strcat(dec, "0100");
			break;
		case '5':
			strcat(dec, "0101");
			break;
		case '6':
			strcat(dec, "0110");
			break;
		case '7':
			strcat(dec, "0111");
			break;
		case '8':
			strcat(dec, "1000");
			break;
		case '9':
			strcat(dec, "1001");
			break;
		case 'a':
		case 'A':
			strcat(dec, "1010");
			break;
		case 'b':
		case 'B':
			strcat(dec, "1011");
			break;
		case 'c':
		case 'C':
			strcat(dec, "1100");
			break;
		case 'd':
		case 'D':
			strcat(dec, "1101");
			break;
		case 'e':
		case 'E':
			strcat(dec, "1110");
			break;
		case 'f':
		case 'F':
			strcat(dec, "1111");
			break;
		default:
			break;
		}
	}
}

int binaryToDec(int num) {
	int decimal_val = 0, base = 1, rem = 0;

	//printf("Enter a binary number(1s and 0s) \n");
	//scanf("%d", &num); /* maximum five digits */
	//int binary_val = num;
	while (num > 0) {
		rem = num % 10;
		decimal_val = decimal_val + rem * base;
		num = num / 10;
		base = base * 2;
	}
	return decimal_val;
}

int bin_to_dec(char *bin) {

	int i, tam, novoValor = 0;
	tam = strlen(bin); //verifica quantos d�gitos tem no n�mero

	//pega os d�gitos da direita para a esquerda
	for (i = tam - 1; i >= 0; i--) {
		//printf("%c|", numero[i]);
		if (bin[i] == '1') {
			novoValor += pow(2, tam - 1 - i);
		}
	}

	return novoValor;
}
//########################## ENCODER AUXILIAR PAYLOAD ###############################
void RemoveSpaces(char source[]) {
	//int tam = strlen(source);
	char* i = source;
	char* j = source;

	while (*j != 0) {
		*i = *j++;
		if (*i != ' ')
			i++;
	}
	*i = 0;

}

void check_size_info(int size, char*buff) {
	if (size == 2) {
		char dado[4] = "";
		switch (strlen(buff)) {
		case (1):
			dado[0] = 48;
			//dado[1] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (2):
			/*			dado[0] = 48;
			 strcat(dado, buff);
			 strcpy(buff, dado);*/
			break;
			/*		case (3):
			 break;*/
		default:
			buff[0] = 48;
			buff[1] = 48;
			break;
		}
	} else if (size == 4) {
		char dado[5] = "";
		switch (strlen(buff)) {
		case (1):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (2):
			dado[0] = 48;
			dado[1] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (3):
			dado[0] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (4):
			break;
		default:
			buff[0] = 48;
			buff[1] = 48;
			buff[2] = 48;
			buff[3] = 48;
			buff[4] = 0;
			break;
		}
	} else if (size == 6) {
		char dado[7] = "";
		switch (strlen(buff)) {
		case (1):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			dado[3] = 48;
			dado[4] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (2):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			dado[3] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (3):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (4):
			dado[0] = 48;
			dado[1] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (5):
			dado[0] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (6):
			break;
		default:
			buff[0] = 48;
			buff[1] = 48;
			buff[2] = 48;
			buff[3] = 48;
			buff[4] = 48;
			buff[5] = 48;
			buff[6] = 0;
			break;
		}

	}
}

int fn_get_seconsForTimeStemp(int TS_Total) {
	//TS_Total = 355195;
	int TS_hour, TS_day, TS_minute, TS_secons;
	float TS_hour_aux, TS_day_aux, TS_minute_aux;

	TS_day_aux = TS_Total / 1440.000;
	TS_day = TS_day_aux;

	TS_hour_aux = (TS_day_aux - TS_day) * 24.000;
	TS_hour = TS_hour_aux;

	TS_minute_aux = (TS_hour_aux - TS_hour) * 60.00;
	TS_minute = round(TS_minute_aux);

	TS_secons = 60 * (TS_minute + (TS_hour * 60));

	return TS_secons;
}
//########################## ENCODER AUXILIAR GPS ###############################

uint8_t find_between(const char *first, const char *last, char *buff,
		char *buff_return) {
	uint8_t ok = 0;
	//const char *last = "*";
	//const char *buff = _gps;
	char *target = NULL;
	char *start, *end;
	start = strstr(buff, first);
	end = strstr(start, last);

	if (start) {
		start += strlen(first);
		if (end) {
			target = (char *) malloc(end - start + 1);
			memcpy(target, start, end - start);
			target[end - start] = '\0';
			ok = 1;
		}
	}
	strcpy(buff_return, target);
	free(target);
	return ok;
}

int find_word(char *where, char *word) {
	int size_where = strlen(where);
	int size_word = strlen(word);
	int ok = 0;
	int n = 0;
	int m = 0;

	for (int var = 0; var < size_where; ++var) {
		// if first character of search string matches
		while (where[n] == word[m]) {
			n++;
			m++;
		}
		// if we sequence of characters matching with the length of searched string
		if (m == size_word) {
			ok = 1;
			break;
		}
		n++;
		m = 0; // reset the counter to start from first character of the search string.
	}

	return ok;
}

int countOccurrences(char * str, char * toSearch) {
	int i, j, found, count;
	int stringLen, searchLen;

	stringLen = strlen(str);      // length of string
	searchLen = strlen(toSearch); // length of word to be searched

	count = 0;

	for (i = 0; i <= stringLen - searchLen; i++) {
		/* Match word with string */
		found = 1;
		for (j = 0; j < searchLen; j++) {
			if (str[i + j] != toSearch[j]) {
				found = 0;
				break;
			}
		}

		if (found == 1) {
			count++;
		}
	}

	return count;
}

//########################## DECODER AUXILIAR GPRS ###############################

uint8_t get_downlink_payload(char* data) {

	uint8_t responce = 0;

	memset(downlink_data, 0, 16);

	int size_w = strlen(data);

	for (int var = 0; var < size_w; ++var) {
		if (data[var + 1] == 88 && data[var + 2] == 61) {
			downlink_data[0] = data[var + 3];
			downlink_data[1] = data[var + 4];
			downlink_data[2] = data[var + 5];
			downlink_data[3] = data[var + 6];
			downlink_data[4] = data[var + 7];
			downlink_data[5] = data[var + 8];
			downlink_data[6] = data[var + 9];
			downlink_data[7] = data[var + 10];
			downlink_data[8] = data[var + 11];
			downlink_data[9] = data[var + 12];
			downlink_data[10] = data[var + 13];
			downlink_data[11] = data[var + 14];
			downlink_data[12] = data[var + 15];
			downlink_data[13] = data[var + 16];
			downlink_data[14] = data[var + 17];
			downlink_data[15] = data[var + 18];
			responce = 1;
			break;
		}
	}
	fn_fprint("\r\nDOWNLINK: ");
	fn_fprint(downlink_data);
	fn_fprint("\r\n");

	char RX_buff_bin[64] = { 0 };
	//char volume_ref[3] = { 0 };
	/*
	 char gas_thresh[2] = { 0 };
	 char vol_thresh[2] = { 0 };
	 char ang_thresh[2] = { 0 };
	 char tmp_thresh[2] = { 0 };*/
	char timebyte[6] = { 0 };
	char undTimeByte[2] = { 0 };
	//char time_stamp[6] = { 0 };

	//int TimeStamp = 0;
	int timebyte_int = 0, undTimeByte_int = 0, time_byte_value =
			0;

	/*	size = strlen(RX);
	 for (int var = 0; var < size; ++var) {
	 if (RX[var] == 84 && RX[var + 1] == 88 && RX[var + 2] == 61) {
	 downlinkRX[0] = RX[var + 3];
	 downlinkRX[1] = RX[var + 4];
	 downlinkRX[2] = RX[var + 5];
	 downlinkRX[3] = RX[var + 6];
	 downlinkRX[4] = RX[var + 7];
	 downlinkRX[5] = RX[var + 8];
	 downlinkRX[6] = RX[var + 9];
	 downlinkRX[7] = RX[var + 10];
	 downlinkRX[8] = RX[var + 11];
	 downlinkRX[9] = RX[var + 12];
	 downlinkRX[10] = RX[var + 13];
	 downlinkRX[11] = RX[var + 14];
	 downlinkRX[12] = RX[var + 15];
	 downlinkRX[13] = RX[var + 16];
	 downlinkRX[14] = RX[var + 17];
	 downlinkRX[15] = RX[var + 18];
	 }
	 }*/

	if (responce == 1) {
		/*time_stamp[0] = downlink_data[6];
		time_stamp[1] = downlink_data[7];
		time_stamp[2] = downlink_data[4];
		time_stamp[3] = downlink_data[5];
		time_stamp[4] = downlink_data[2];
		time_stamp[5] = downlink_data[3];*/

		//volume_ref[0] = downlink_data[10];
		//volume_ref[1] = downlink_data[11];
		/*
		 vol_thresh[0] = downlinkRX[15];
		 ang_thresh[0] = downlinkRX[14];
		 tmp_thresh[0] = downlinkRX[13];
		 gas_thresh[0] = downlinkRX[12];*/
		HAL_Delay(10);
		hexBin(downlink_data, RX_buff_bin);

		fn_fprint("\r\nDOWNLINK BINARY: ");
		fn_fprint(RX_buff_bin);

		//choice of transmission resource
		//if (RX_buff_bin[4] == 49)
		//st_flag.SigFox = 1;
		//if (RX_buff_bin[4] == 48)
		//st_flag.SigFox = 0;
		if (RX_buff_bin[3] == 49)
			st_flag.GPRS = 1;
		if (RX_buff_bin[3] == 48)
			st_flag.GPRS = 0;
		if (RX_buff_bin[2] == 49)
			st_flag.BLE = 1;
		if (RX_buff_bin[2] == 48)
			st_flag.BLE = 0;

		undTimeByte[0] = RX_buff_bin[32]; //0
		undTimeByte[1] = RX_buff_bin[33]; //1
		timebyte[0] = RX_buff_bin[34]; //0
		timebyte[1] = RX_buff_bin[35]; //1
		timebyte[2] = RX_buff_bin[36]; //1
		timebyte[3] = RX_buff_bin[37]; //1
		timebyte[4] = RX_buff_bin[38]; //1
		timebyte[5] = RX_buff_bin[39]; //0
		HAL_Delay(10);
		timebyte_int = atoi(timebyte);
		HAL_Delay(10);
		undTimeByte_int = atoi(undTimeByte);
		HAL_Delay(10);
		time_byte_value = binaryToDec(timebyte_int); //transmission period in hours
		HAL_Delay(10);
		//distance_threshold = hexDec(volume_ref);
		/*int Vol_Threshold = hexDec(vol_thresh);
		 int Gas_Threshold = hexDec(gas_thresh);
		 int Ang_Threshold = hexDec(ang_thresh);
		 int Tmp_Threshold = hexDec(tmp_thresh);*/

		//TimeStamp = hexDec(time_stamp); //timestamp minutes-sigmais protocol

		//TimeCounter = fn_get_seconsForTimeStemp(TimeStamp); //seconds elapsed in the day
		HAL_Delay(10);

		if (time_byte_value != 0) {
			if (undTimeByte_int == 00) //if 00 = time unid its seconds
					{
				KeepAlive_Timer = time_byte_value;
			}
			if (undTimeByte_int == 01) // if 01 = time unid its minutes
					{
				KeepAlive_Timer = (time_byte_value * 60);
			}
			if (undTimeByte_int == 10) // if 10 = time unid its hours
					{
				KeepAlive_Timer = (time_byte_value * 3600);
			}
			if (undTimeByte_int == 11) // if 11 = time unid its days
					{
				KeepAlive_Timer = (time_byte_value * 86400);
			}
		}
	}
	return responce;
}

uint16_t counter_world_size(char* world) {
	uint16_t size = 0;

	while (world[size] != '\0') {
		size++;
	}
	return size;
}

//########################## DECODER DOWNLINK ###############################

void fn_decoder_downlink() {
	char numbuff[] = "\0";
	char timeStamp[6] = { 0 }, binario[64] = { 0 };
	char buffer_downlink_hex[16] = { 0 };
	char buffer_downlink_bin[64] = { 0 };

	fn_fprint("Downlink number= ");
	fn_fprintnumber(downlink_data_ok);
	fn_fprint("\r\n");
	HAL_Delay(100);

	strcpy(buffer_downlink_hex, downlink_data);

	timeStamp[0] = downlink_data[8];
	timeStamp[1] = downlink_data[9];
	timeStamp[2] = downlink_data[6];
	timeStamp[3] = downlink_data[7];
	timeStamp[4] = downlink_data[4];
	timeStamp[5] = downlink_data[5];

	HAL_Delay(100);
	fn_fprint("Downlink= ");
	fn_fprint(downlink_data);
	fn_fprint("\r\nTimeStamp (HEX)= ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) timeStamp, 6, 100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);
	//TimeCounter = hexDec(timeStamp); //timestamp minutes-sigmais protocol
	HAL_Delay(100);
	//TimeCounter = fn_get_seconsForTimeStemp(TimeStamp); //seconds elapsed in the day
	//fn_fprint("TimeStamp (dec)=");
	//utoa(TimeCounter, numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);
	hexBin(buffer_downlink_hex, buffer_downlink_bin);
	HAL_Delay(100);
	strncpy(binario, buffer_downlink_bin, 64);
	HAL_Delay(100);
	fn_fprint("Downlink binario= ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) binario, 64, 100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	switch (downlink_data[0]) {
	case 48:
		break;
	case 49:
		KeepAlive_Timer = 600;
		break;
	case 50:
		KeepAlive_Timer = 900;
		break;
	case 51:
		KeepAlive_Timer = 1200;
		break;
	case 52:
		KeepAlive_Timer = 1800;
		break;
	case 53:
		KeepAlive_Timer = 3600;
		break;
	case 54:
		KeepAlive_Timer = 7200;
		break;
	case 55:
		KeepAlive_Timer = 14400;
		break;
	case 56:
		KeepAlive_Timer = 21600;
		break;
	case 57:
		KeepAlive_Timer = 28800;
		break;
	case 58:
		KeepAlive_Timer = 36000;
		break;
	case 59:
		KeepAlive_Timer = 43200;
		break;
	case 60:
		KeepAlive_Timer = 86400;
		break;
	case 61:
		break;
	case 62:
		break;
	case 63:
		break;
	default:
		break;

	}

	fn_fprint("keep alive = ");
	utoa(KeepAlive_Timer, numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	fn_fprint("ble mode = ");

	switch (downlink_data[3]) {
	case 48:
		st_flag.en_BLE = 0;
		fn_fprint("off");
		break;
	case 49:
		st_flag.en_BLE = 1;
		e_Current_nina_config_type = SERIAL_BLE_MODE;
		fn_fprint("serial");
		break;
	case 50:
		st_flag.en_BLE = 1;
		e_Current_nina_config_type = IBEACON_MODE;
		fn_fprint("beacon");
		break;
	case 51:
		st_flag.en_BLE = 1;
		e_Current_nina_config_type = EDDYSTONE_BEACON_MODE;
		fn_fprint("eddystone");
		break;
	case 52:
		st_flag.en_BLE = 1;
		e_Current_nina_config_type = SCAN_BEACON_MODE;
		fn_fprint("scan");
		break;

	default:
		break;
	}

	st_flag.en_GPS = (buffer_downlink_bin[11] - 48);
	st_flag.en_tipper = (buffer_downlink_bin[46] - 48);
	st_flag.volume_measurement_type = (buffer_downlink_bin[47] - 48);

	fn_fprint("\r\ngps=");
	fn_fprintnumber(st_flag.en_GPS);

	fn_fprint("\r\ntripper=");
	fn_fprintnumber(st_flag.en_tipper);

	fn_fprint("\r\nvolume type=");
	fn_fprintnumber(st_flag.volume_measurement_type);

	fn_fprint("\r\n___________________________________\r\n");

}

//########################## DECODER AUXILIAR BLE ###############################

int find_location_word(char *where, char *word) {
	int size_where = strlen(where);
	int size_word = strlen(word);
	int retourn = 0;

	int n = 0;
	int m = 0;

	for (int var = 0; var < size_where; ++var) {
		// if first character of search string matches
		while (where[n] == word[m]) {
			n++;
			m++;
		}
		// if we sequence of characters matching with the length of searched string
		if (m == size_word) {
			retourn = n;
			break;
		}
		n++;
		m = 0; // reset the counter to start from first character of the search string.
	}

	return retourn;
}

void fn_encoder_ble_values(char* retorno) {

	//fn_get_stm32_temperature();
	//fn_get_stm32_volts();
	uint32_t distancias = st_data_sensor_e.distance;	//fn_get_dist_sen031x();

	fn_fprintnumber(distancias);

	char dados[44] = { 0 };
	char bateria[4] = { 0 };
	char temperatura_1[2] = { 0 };
	char temperatura_2[2] = "00";
	char distancia[8] = { 0 };
	char time[8] = { 0 };

	decHexBLE(st_stm_adc_variables.battery, bateria, 4);
	decHexBLE(st_stm_adc_variables.temperature, temperatura_1, 2);
	decHexBLE(distancias, distancia, 8);
	decHexBLE(TimeCounter, time, 8);

	strncpy(dados, "0303AAFE1116AAFE2000", 20);
	strncat(dados, bateria, 4);
	strncat(dados, temperatura_1, 2);
	strncat(dados, temperatura_2, 2);
	strncat(dados, distancia, 8);
	strncat(dados, time, 8);

	strcpy(retorno, dados);
}

void decHexBLE(uint32_t number, char* buff, uint32_t size) {

	char hex[size]; /*bcoz it contains characters A to F*/
	memset(buff, 0x00, size); //sizeof(buff));
	memset(hex, 0x00, size); //sizeof(buff));
	int cnt, i;
	cnt = 0; /*initialize index to zero*/
	if (number == 0) {
		strcpy(buff, "0");
	}
	while (number > 0) {
		switch (number % 16) {
		case 10:
			hex[cnt] = 'A';
			break;
		case 11:
			hex[cnt] = 'B';
			break;
		case 12:
			hex[cnt] = 'C';
			break;
		case 13:
			hex[cnt] = 'D';
			break;
		case 14:
			hex[cnt] = 'E';
			break;
		case 15:
			hex[cnt] = 'F';
			break;
		default:
			hex[cnt] = (number % 16) + 0x30; /*converted into char value*/
		}
		number = number / 16;
		cnt++;
	}
	while (cnt != size) {

		hex[cnt] = '0';
		cnt++;
	}
	for (i = (cnt - 1); i >= 0; i--) {
		int j = cnt - 1 - i;
		buff[j] = hex[i];
	}

}

//########################## DECODER AUXILIAR SMS ###############################

void fn_get_status_for_sms() {

}

void fn_get_payload_for_sms() {
	memset(SMS_buffer, 0, 160);
	char init_sms[2] = "#*";
	char meddle_sms[1] = "*";
	char end_sms[2] = "*#";
	char sms_mensage[74] = { 0 };	//2+15+1+24+1+2+1+1+1+5+1+5+1+3+1+8+2+1

	itoa(sequencialMensage, sara_SEQNUMBER, 10);
	itoa((int) TimeCounter, sara_TIMESTAMP, 10);
	itoa((int) KeepAlive_Timer, sara_KEEPALIVE, 10);

	strncpy(sms_mensage, init_sms, 2);
	strncat(sms_mensage, sara_imei, 15);
	strncat(sms_mensage, meddle_sms, 1);
	//strncat(sms_mensage, "02EC1126EB034A07F400FF", 22);
	strncat(sms_mensage, sara_data, 24);
	strncat(sms_mensage, meddle_sms, 1);
	strncat(sms_mensage, sara_RSSI, 2);
	strncat(sms_mensage, meddle_sms, 1);
	strncat(sms_mensage, "0", 1);
	strncat(sms_mensage, meddle_sms, 1);
	strncat(sms_mensage, sara_TIMESTAMP, 5);
	strncat(sms_mensage, meddle_sms, 1);
	strncat(sms_mensage, sara_KEEPALIVE, 5);
	strncat(sms_mensage, meddle_sms, 1);
	strncat(sms_mensage, sara_SEQNUMBER, 3);
	strncat(sms_mensage, meddle_sms, 1);
	strncat(sms_mensage, FIRMWARE_VERSION, 8);
	strncat(sms_mensage, end_sms, 2);

	strcpy(SMS_buffer, sms_mensage);
}

void fn_get_data_for_sms() {
	fn_get_sensors_char_values();
	memset(SMS_buffer, 0, 160);

	strcpy(SMS_buffer, "DADOS DOS SENSORES\n");
	strcat(SMS_buffer, "ANGULO ");
	strcat(SMS_buffer, st_sensor_char_values.angle);
	strcat(SMS_buffer, "\n");
	strcat(SMS_buffer, "TEMPERATURA ");
	strcat(SMS_buffer, st_sensor_char_values.temperature);
	strcat(SMS_buffer, "\n");
	strcat(SMS_buffer, "TENSAO ");
	strcat(SMS_buffer, st_sensor_char_values.battery);
	strcat(SMS_buffer, "\n");
	strcat(SMS_buffer, "DISTANCIA ");
	strcat(SMS_buffer, st_sensor_char_values.distance);
	strcat(SMS_buffer, "\n");
	strcat(SMS_buffer, "GPS LATITUDE ");
	strcat(SMS_buffer, st_sensor_char_values.latitude);
	strcat(SMS_buffer, "\n");
	strcat(SMS_buffer, "GPS LONGITUDE ");
	strcat(SMS_buffer, st_sensor_char_values.longitude);

}

void fn_send_config_sms() {

}
