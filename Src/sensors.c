/*
 * sensors.c
 *
 *  Created on: 16 de mar de 2020
 *      Author: oscar
 */

#include "sensors.h"



void fn_init_sensors() {
	st_data_sensor_e.battery = 0;
	st_data_sensor_e.distance = 0;
	st_data_sensor_e.latitude = 0;
	st_data_sensor_e.longitude = 0;
	st_data_sensor_e.referenceVol = 200;
	st_data_sensor_e.temperature = 0;
}

void fn_get_sensors_values() {
	st_flag.sensors = 1;
	//fn_get_us_angle_value();
	st_data_sensor_e.us_angle = 5;
	//fn_get_angle_value();
	fn_get_volume_value();
	fn_get_temperature_value();
	fn_get_battery_value();
	fn_get_lat_lon_values();




}

void fn_get_sensors_char_values() {

	//fn_get_angle_value();
	//HAL_Delay(100);
	fn_get_volume_value();
	HAL_Delay(100);
	//fn_get_lat_lon_values();
	fn_get_temperature_value();
	HAL_Delay(100);
	fn_get_battery_value();
	HAL_Delay(100);

	int temp_bff = st_stm_adc_variables.temperature;
	int volts_buff = st_stm_adc_variables.battery;
	int dist_buff = st_data_sensor_e.distance;

	//itoa((int) st_data_sensor_e.angle, st_sensor_char_values.angle, 10);
	itoa(volts_buff, st_sensor_char_values.battery, 10);
	itoa(dist_buff, st_sensor_char_values.distance, 10);
	itoa((int) st_data_sensor_e.latitude, st_sensor_char_values.latitude, 10);
	itoa((int) st_data_sensor_e.longitude, st_sensor_char_values.longitude, 10);
	itoa((int) st_data_sensor_e.referenceVol,
			st_sensor_char_values.referenceVol, 10);
	itoa(temp_bff, st_sensor_char_values.temperature, 10);
	itoa((int) st_data_sensor_e.volume, st_sensor_char_values.volume, 10);

}

//**************** PROCESSING SENSORS VALUES *********************
void fn_get_angle_value() {
	/*st_data_sensor_previwes_e.angle = st_data_sensor_e.angle;

	 if (st_flag.acelerometer == 1) {
	 fn_get_lsm303ah();
	 } else if (st_flag.acelerometer == 2) {
	 fn_init_lsm303agr();
	 }
	 if (st_accelerometer.pitch < 0)
	 st_accelerometer.pitch *= (-1);
	 st_data_sensor_e.angle = round(st_accelerometer.pitch / 10);
	 */}

uint8_t fn_get_us_angle_value(void) {
#ifndef greenPCB
	volatile uint16_t copyOf_ADCxConvertedData = 0, usAngleOffSet1 = 2448, // valor anotado usado apenas para testes
	//usAngleOffSet2 = 1785, // valor anotado usado apenas para testes
	usAngleOffSet3 = 947;// valor anotado usado apenas para testes
	//uint8_t tx_buffer[6];

	US_ON
	HAL_Delay(500);
	copyOf_ADCxConvertedData = fn_get_sample_us_angle();
	US_OFF
	// C�lculo hipot�tico.
	// Aguardar finaliza��o do enclosure para fazer equa��o definitiva.
//	if(copyOf_ADCxConvertedData < usAngleOffSet1) {
//		st_data_sensor_e.us_angle = ;
//		}
//	else if(copyOf_ADCxConvertedData < usAngleOffSet3) {
//		// TODO: Aqui h� um casting de float para int proposital. Fazer expl�cito depois
//		st_data_sensor_e.us_angle = ((float)(copyOf_ADCxConvertedData - usAngleOffSet1) * 160) / (usAngleOffSet3 - usAngleOffSet1);
//	}
//	else {
//		st_data_sensor_e.us_angle = 160;
//	}

	// TODO: Aqui h� um casting de float para int proposital. Fazer expl�cito depois
	st_data_sensor_e.us_angle = ((float) (copyOf_ADCxConvertedData
					- usAngleOffSet1)) * 50 / (usAngleOffSet3 - usAngleOffSet1) + 40;
	//st_data_sensor_e.us_angle = copyOf_ADCxConvertedData;
//	sprintf((char*)tx_buffer, "%d\r\n", st_data_sensor_e.us_angle);
//	HAL_UART_Transmit(&hlpuart1, tx_buffer, sizeof(tx_buffer), 100);

	// As linhas de c�digo abaixo foram inseridas apenas para fazer o teste da interface com o usu�rio
	if ((st_data_sensor_e.us_angle > 0) && (st_data_sensor_e.us_angle <= 5))
	st_data_sensor_e.us_angle = 0;
	if ((st_data_sensor_e.us_angle > 5) && (st_data_sensor_e.us_angle <= 15))
	st_data_sensor_e.us_angle = 1;
	if ((st_data_sensor_e.us_angle > 15) && (st_data_sensor_e.us_angle <= 25))
	st_data_sensor_e.us_angle = 2;
	if ((st_data_sensor_e.us_angle > 25) && (st_data_sensor_e.us_angle <= 35))
	st_data_sensor_e.us_angle = 3;
	if ((st_data_sensor_e.us_angle > 35) && (st_data_sensor_e.us_angle <= 45))
	st_data_sensor_e.us_angle = 4;
	if ((st_data_sensor_e.us_angle > 45) && (st_data_sensor_e.us_angle <= 55))
	st_data_sensor_e.us_angle = 5;
	if ((st_data_sensor_e.us_angle > 55) && (st_data_sensor_e.us_angle <= 65))
	st_data_sensor_e.us_angle = 6;
	if ((st_data_sensor_e.us_angle > 65) && (st_data_sensor_e.us_angle <= 75))
	st_data_sensor_e.us_angle = 7;
	if ((st_data_sensor_e.us_angle > 75) && (st_data_sensor_e.us_angle <= 85))
	st_data_sensor_e.us_angle = 8;
	if ((st_data_sensor_e.us_angle > 85) && (st_data_sensor_e.us_angle <= 95))
	st_data_sensor_e.us_angle = 9;
	if ((st_data_sensor_e.us_angle > 95) && (st_data_sensor_e.us_angle <= 105))
	st_data_sensor_e.us_angle = 10;
	if ((st_data_sensor_e.us_angle > 105) && (st_data_sensor_e.us_angle <= 115))
	st_data_sensor_e.us_angle = 11;
	if ((st_data_sensor_e.us_angle > 115) && (st_data_sensor_e.us_angle <= 125))
	st_data_sensor_e.us_angle = 12;
	if ((st_data_sensor_e.us_angle > 125) && (st_data_sensor_e.us_angle <= 135))
	st_data_sensor_e.us_angle = 13;
	if ((st_data_sensor_e.us_angle > 135) && (st_data_sensor_e.us_angle <= 145))
	st_data_sensor_e.us_angle = 14;
	if ((st_data_sensor_e.us_angle > 145) && (st_data_sensor_e.us_angle <= 360))
	st_data_sensor_e.us_angle = 15;
	//if((st_data_sensor_e.us_angle > 145) && (st_data_sensor_e.us_angle <= 155)) st_data_sensor_e.us_angle = 15;
	//if((st_data_sensor_e.us_angle > 155) && (st_data_sensor_e.us_angle <= 165)) st_data_sensor_e.us_angle = 16;

//	sprintf((char*)tx_buffer, "%d\r\n", st_data_sensor_e.us_angle);
//	HAL_UART_Transmit(&hlpuart1, tx_buffer, sizeof(tx_buffer), 100);
#endif

#ifdef greenPCB
	st_data_sensor_e.us_angle = 5;
#endif

	return st_data_sensor_e.us_angle;

}

void fn_get_volume_value() {
	//float buffer_distance;
	st_data_sensor_previwes_e.distance = st_data_sensor_e.distance;
	st_data_sensor_e.distance = fn_get_dist_sen031x();
	if (st_data_sensor_e.distance > 255)
		st_data_sensor_e.distance = 255;
	if (st_data_sensor_e.distance < 23 && st_data_sensor_e.distance != 0)
		st_data_sensor_e.distance = 23;
}

void fn_get_lat_lon_values() {
	st_flag.location_GPS = 0;

	if (st_flag.en_GPS && !st_flag.location_GPS && !st_flag.location_CELL) {

		st_data_sensor_previwes_e.latitude = st_data_sensor_e.latitude;
		st_data_sensor_previwes_e.longitude = st_data_sensor_e.longitude;

		fn_fprint("\r\n\r\nGPS ON\r\n");
		GPS_ON
		GPStimer_EN = st_timer.GPSenable + TimeCounter;

		while (!st_flag.location_GPS && (GPStimer_EN > TimeCounter)) {
			st_flag.location_GPS = fn_get_gps_values_camM8();
		}

		fn_fprint("\r\n\r\nGPS OFF\r\n");
		GPS_OFF
	}

	if (hour || minute || second)
		TimeCounter = (hour * 3600 + minute * 60 + second);

}

void fn_get_latitude_value() {
	//st_data_sensor_e.latitude = st_gps_data_current.latitude;
}

void fn_get_longitude_value() {
	//st_data_sensor_e.longitude = st_gps_data_previous.longitude;
}

void fn_get_temperature_value() {
	st_data_sensor_previwes_e.temperature = st_data_sensor_e.temperature;
	fn_get_stm32_temperature();
	fn_get_stm32_temperature();
	fn_get_stm32_temperature();

	if (st_stm_adc_variables.temperature > 50) {
		fn_get_stm32_temperature();
		if (st_stm_adc_variables.temperature > 50) {
			fn_get_stm32_temperature();
			if (st_stm_adc_variables.temperature > 50) {
				fn_get_stm32_temperature();
			}
		}
	}

		st_data_sensor_e.temperature = st_stm_adc_variables.temperature;
	if (st_data_sensor_e.temperature > 255)
		st_data_sensor_e.temperature = 255;

}

void fn_get_battery_value() {
	st_data_sensor_previwes_e.battery = st_data_sensor_e.battery;

	fn_get_stm32_volts();
	fn_get_stm32_volts();
	fn_get_stm32_volts();

	if (st_stm_adc_variables.battery > 2550)
		st_data_sensor_e.battery = (st_stm_adc_variables.battery - 2550) / 10;
	else
		st_data_sensor_e.battery = 255;
}

I2C_HandleTypeDef hi2c1;

int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp, uint16_t len) {

	uint32_t i2c_add = (uint32_t) handle;
	if ((i2c_add == LSM303AGR_I2C_ADD_XL) || (i2c_add == LSM303AH_I2C_ADD_XL)) {
		/* enable auto incremented in multiple read/write commands */
		Reg |= 0x80;
	}
	HAL_I2C_Mem_Write(&hi2c1, i2c_add, Reg,
	I2C_MEMADD_SIZE_8BIT, Bufp, len, 1000);
	return 0;
}

int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp, uint16_t len) {
	uint32_t i2c_add = (uint32_t) handle;
	if ((i2c_add == LSM303AGR_I2C_ADD_XL) || (i2c_add == LSM303AH_I2C_ADD_XL)) {
		/* enable auto incremented in multiple read/write commands */
		Reg |= 0x80;
	}
	HAL_I2C_Mem_Read(&hi2c1, (uint8_t) i2c_add, Reg,
	I2C_MEMADD_SIZE_8BIT, Bufp, len, 1000);
	return 0;
}
