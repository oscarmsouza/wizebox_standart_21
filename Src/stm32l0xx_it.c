/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32l0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l0xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ninaB1.h"
#include "saraG450.h"
#include "iot.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c1;
extern LPTIM_HandleTypeDef hlptim1;
extern UART_HandleTypeDef hlpuart1;
extern UART_HandleTypeDef huart2;
extern RTC_HandleTypeDef hrtc;
extern TIM_HandleTypeDef htim2;
extern WWDG_HandleTypeDef hwwdg;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0+ Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable Interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
	  /* USER CODE BEGIN W1_HardFault_IRQn 0 */
	 	  fn_fprint("\r\nCRITICAL ERROR");
	 	  HAL_WWDG_Init(&hwwdg);
	     /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */
	  msCount++;
  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32L0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32l0xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles Window watchdog interrupt.
  */
void WWDG_IRQHandler(void)
{
  /* USER CODE BEGIN WWDG_IRQn 0 */

  /* USER CODE END WWDG_IRQn 0 */
  HAL_WWDG_IRQHandler(&hwwdg);
  /* USER CODE BEGIN WWDG_IRQn 1 */

  /* USER CODE END WWDG_IRQn 1 */
}

/**
  * @brief This function handles RTC global interrupt through EXTI lines 17, 19 and 20 and LSE CSS interrupt through EXTI line 19.
  */
void RTC_IRQHandler(void)
{
  /* USER CODE BEGIN RTC_IRQn 0 */

  /* USER CODE END RTC_IRQn 0 */
  HAL_RTCEx_WakeUpTimerIRQHandler(&hrtc);
  /* USER CODE BEGIN RTC_IRQn 1 */

  /* USER CODE END RTC_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel 1 interrupt.
  */
void DMA1_Channel1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel1_IRQn 0 */
	/* Check whether DMA transfer complete caused the DMA interruption */
		  if(LL_DMA_IsActiveFlag_TC1(DMA1) == 1)
		  {
		    /* Clear flag DMA global interrupt */
		    /* (global interrupt flag: half transfer and transfer complete flags) */
		    LL_DMA_ClearFlag_GI1(DMA1);

		    /* Call interruption treatment function */
		    AdcDmaTransferComplete_Callback();
		  }

		  /* Check whether DMA transfer error caused the DMA interruption */
		  if(LL_DMA_IsActiveFlag_TE1(DMA1) == 1)
		  {
		    /* Clear flag DMA transfer error */
		    LL_DMA_ClearFlag_TE1(DMA1);

		    /* Call interruption treatment function */
		    AdcDmaTransferError_Callback();
		  }
  /* USER CODE END DMA1_Channel1_IRQn 0 */

  /* USER CODE BEGIN DMA1_Channel1_IRQn 1 */

  /* USER CODE END DMA1_Channel1_IRQn 1 */
}

/**
  * @brief This function handles ADC, COMP1 and COMP2 interrupts (COMP interrupts through EXTI lines 21 and 22).
  */
void ADC1_COMP_IRQHandler(void)
{
  /* USER CODE BEGIN ADC1_COMP_IRQn 0 */
	/* Check whether ADC group regular end of sequence conversions caused       */
		  /* the ADC interruption.                                                    */
		  if(LL_ADC_IsActiveFlag_EOS(ADC1) != 0)
		  {
		    /* Clear flag ADC group regular end of sequence conversions */
		    LL_ADC_ClearFlag_EOS(ADC1);

		    /* Call interruption treatment function */
		    AdcGrpRegularSequenceConvComplete_Callback();
		  }

		  /* Check whether ADC group regular overrun caused the ADC interruption */
		  if(LL_ADC_IsActiveFlag_OVR(ADC1) != 0)
		  {
		    /* Clear flag ADC group regular overrun */
		    LL_ADC_ClearFlag_OVR(ADC1);

		    /* Call interruption treatment function */
		    AdcGrpRegularOverrunError_Callback();
		  }
  /* USER CODE END ADC1_COMP_IRQn 0 */

  /* USER CODE BEGIN ADC1_COMP_IRQn 1 */

  /* USER CODE END ADC1_COMP_IRQn 1 */
}

/**
  * @brief This function handles LPTIM1 global interrupt / LPTIM1 wake-up interrupt through EXTI line 29.
  */
void LPTIM1_IRQHandler(void)
{
  /* USER CODE BEGIN LPTIM1_IRQn 0 */

  /* USER CODE END LPTIM1_IRQn 0 */
  HAL_LPTIM_IRQHandler(&hlptim1);
  /* USER CODE BEGIN LPTIM1_IRQn 1 */

  /* USER CODE END LPTIM1_IRQn 1 */
}

/**
  * @brief This function handles USART4 and USART5 interrupt.
  */
void USART4_5_IRQHandler(void)
{
  /* USER CODE BEGIN USART4_5_IRQn 0 */
	if (LL_USART_IsActiveFlag_RXNE(USART4)
				&& LL_USART_IsEnabledIT_RXNE(USART4)) {
			/* RXNE flag will be cleared by reading of RDR register (done in call) */
			/* Call function in charge of handling Character reception */
			USART_CharReception_Callback();
		}
		if (LL_USART_IsActiveFlag_RXNE(USART5)
				&& LL_USART_IsEnabledIT_RXNE(USART5)) {
			/* RXNE flag will be cleared by reading of RDR register (done in call) */
			/* Call function in charge of handling Character reception */
			USART5_CharReception_Callback();
		}
  /* USER CODE END USART4_5_IRQn 0 */

  /* USER CODE BEGIN USART4_5_IRQn 1 */
		/* Check RXNE flag value in ISR register */
			if (LL_USART_IsEnabledIT_TXE(USART5) && LL_USART_IsActiveFlag_TXE(USART5)) {
				/* TXE flag will be automatically cleared when writing new data in TDR register */

				/* Call function in charge of handling empty DR => will lead to transmission of next character */
				USART5_TXEmpty_Callback();
			}

			if (LL_USART_IsEnabledIT_TC(USART5) && LL_USART_IsActiveFlag_TC(USART5)) {
				/* Clear TC flag */
				LL_USART_ClearFlag_TC(USART5);
				/* Call function in charge of handling end of transmission of sent character
				 and prepare next charcater transmission */
				USART5_CharTransmitComplete_Callback();
			}

			if (LL_USART_IsEnabledIT_ERROR(USART5)
					&& LL_USART_IsActiveFlag_NE(USART5)) {
				/* Call Error function */

			}


			if (LL_USART_IsEnabledIT_TXE(USART4) && LL_USART_IsActiveFlag_TXE(USART4)) {
				/* TXE flag will be automatically cleared when writing new data in TDR register */

				/* Call function in charge of handling empty DR => will lead to transmission of next character */
				USART_TXEmpty_Callback();
			}

			if (LL_USART_IsEnabledIT_TC(USART4) && LL_USART_IsActiveFlag_TC(USART4)) {
				/* Clear TC flag */
				LL_USART_ClearFlag_TC(USART4);
				/* Call function in charge of handling end of transmission of sent character
				 and prepare next charcater transmission */
				USART_CharTransmitComplete_Callback();
			}

			if (LL_USART_IsEnabledIT_ERROR(USART4)
					&& LL_USART_IsActiveFlag_NE(USART4)) {
				/* Call Error function */

			}
  /* USER CODE END USART4_5_IRQn 1 */
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */

  /* USER CODE END TIM2_IRQn 0 */
  HAL_TIM_IRQHandler(&htim2);
  /* USER CODE BEGIN TIM2_IRQn 1 */

  /* USER CODE END TIM2_IRQn 1 */
}

/**
  * @brief This function handles I2C1 event global interrupt / I2C1 wake-up interrupt through EXTI line 23.
  */
void I2C1_IRQHandler(void)
{
  /* USER CODE BEGIN I2C1_IRQn 0 */

  /* USER CODE END I2C1_IRQn 0 */
  if (hi2c1.Instance->ISR & (I2C_FLAG_BERR | I2C_FLAG_ARLO | I2C_FLAG_OVR)) {
    HAL_I2C_ER_IRQHandler(&hi2c1);
  } else {
    HAL_I2C_EV_IRQHandler(&hi2c1);
  }
  /* USER CODE BEGIN I2C1_IRQn 1 */

  /* USER CODE END I2C1_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt / USART1 wake-up interrupt through EXTI line 25.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
	if (LL_USART_IsActiveFlag_RXNE(USART1)
					&& LL_USART_IsEnabledIT_RXNE(USART1)) {
				/* RXNE flag will be cleared by reading of RDR register (done in call) */
				/* Call function in charge of handling Character reception */
				USART1_CharReception_Callback();
			}
  /* USER CODE END USART1_IRQn 0 */
  /* USER CODE BEGIN USART1_IRQn 1 */
	if (LL_USART_IsEnabledIT_TXE(USART1) && LL_USART_IsActiveFlag_TXE(USART1)) {
			/* TXE flag will be automatically cleared when writing new data in TDR register */

			/* Call function in charge of handling empty DR => will lead to transmission of next character */
			USART1_TXEmpty_Callback();
		}

		if (LL_USART_IsEnabledIT_TC(USART1) && LL_USART_IsActiveFlag_TC(USART1)) {
			/* Clear TC flag */
			LL_USART_ClearFlag_TC(USART1);
			/* Call function in charge of handling end of transmission of sent character
			 and prepare next charcater transmission */
			USART1_CharTransmitComplete_Callback();
		}

		if (LL_USART_IsEnabledIT_ERROR(USART1)
				&& LL_USART_IsActiveFlag_NE(USART1)) {
			/* Call Error function */

		}
  /* USER CODE END USART1_IRQn 1 */
}

/**
  * @brief This function handles USART2 global interrupt / USART2 wake-up interrupt through EXTI line 26.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

/**
  * @brief This function handles RNG and LPUART1 Interrupts / LPUART1 wake-up interrupt through EXTI line 28.
  */
void RNG_LPUART1_IRQHandler(void)
{
  /* USER CODE BEGIN RNG_LPUART1_IRQn 0 */

  /* USER CODE END RNG_LPUART1_IRQn 0 */
  HAL_UART_IRQHandler(&hlpuart1);
  /* USER CODE BEGIN RNG_LPUART1_IRQn 1 */

  /* USER CODE END RNG_LPUART1_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
