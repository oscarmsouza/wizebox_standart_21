/*
 * saraG450.c
 *
 *  Created on: 30 de mar de 2020
 *      Author: oscar
 */

#include "saraG450.h"
#include "stm32l0xx_it.h"

#define SARA_BYTES_TO_RECEIVER	1000

__IO uint8_t ubSend_SARA = 0;
uint8_t aStringToSend_SARA[500];
uint8_t ubSizeToSend_SARA = 0; //sizeof(aStringToSend_SARA);
uint8_t aStringToReceiver_SARA[SARA_BYTES_TO_RECEIVER];
uint64_t ubSizeToReceiver_SARA = 0;
uint8_t flag_sara;
char get_default_page_info[155];
char latitude_SARA[8];
char longitude_SARA[8];
char sara_OPERATOR[5];

struct {
	int lat_sara, lng_sara, hour_sara, minute_sara, second_sara, batterry_level,
			signal_level, network_service, sounder_active, unread_mensage,
			call_progress, roaming_network, sms_full, ps_indicator, call_set_up,
			call_on, sim_detection;
} st_indicator_control_e;

//*****************     MAIN SARA    *********************

void fn_main_sara(void) {
	e_Current_sara_sequence = TURN_ON_MODE;
	uint8_t info_ok;
	sequencialMensage++;
	GPRS_Timer_EN = st_timer.GPRSenable;
	st_flag.en_HTTP = 1;
	st_flag.en_SMS = 0;

	if (st_flag.en_HTTP || st_flag.en_SMS) {

		while (GPRS_Timer_EN && e_Current_sara_sequence != TURN_OFF_MODE) {
			if (!GPRS_Timer_EN) {
				fn_fprint("\r\n SARA TIMER ENABLE OUT\r\n");
				e_Current_sara_sequence = TURN_OFF_MODE;
			}
			switch (e_Current_sara_sequence) {

			case RESET_MODE:
				fn_fprint("\r\n******RESET_MODE******\r\n");
				fn_power_off_sara();
				HAL_Delay(10000);
				e_Current_sara_sequence = TURN_ON_MODE;
				break;

			case TURN_ON_MODE:
				fn_fprint("\r\n******TURN_ON_MODE******\r\n");
				fn_power_on_sara();
				//HAL_Delay(5000);
				e_Current_sara_sequence = INICIALIZATION;
				break;

			case INICIALIZATION:
				fn_fprint("\r\n******INICIALIZATION******\r\n");
				if (fn_start_at_commands_sara()) {

					fn_at_command_saraG450("AT+UFACTORY=1,1");
					memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
					Start_Sara_Transfers("ATE0\r");
					HAL_Delay(100);
					fn_at_uart_reception_sara();
					HAL_Delay(100);
					memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

					e_Current_sara_sequence = INFO_MODE;
				} else
					e_Current_sara_sequence = TURN_OFF_MODE;
				break;

			case INFO_MODE:
				fn_fprint("\r\n******INFO_MODE******\r\n");
				fn_get_module_info_sara();
				e_Current_sara_sequence = QUALITY_SIGNAL_MODE;
				break;

			case QUALITY_SIGNAL_MODE:
				fn_fprint("\r\n******QUALITY_SIGNAL_MODE******\r\n");
				e_Current_sara_sequence = OPERATOR_INFO_MODE;

				while (!fn_check_signal_quality_status()) {
					HAL_Delay(500);
					fn_fprint("\r\nGPRS TimerOut remaind: ");
					fn_fprintnumber(GPRS_Timer_EN);
					fn_fprint("s\r\n");
					if (!GPRS_Timer_EN)
						break;

				}
				if (st_flag.en_HTTP) {
					e_Current_sara_sequence = OPERATOR_INFO_MODE;
				}
				if (st_flag.en_SMS) {
					e_Current_sara_sequence = SMS_MODE;
				}
				//fn_get_RSSI_QS(fn_check_signal_quality_status());
				break;

			case OPERATOR_INFO_MODE:
				fn_fprint("\r\n******OPERATOR_INFO_MODE******\r\n");
				e_Current_sara_sequence = NETWORK_OPERATION_MODE;

				fn_get_registration_status();

				fn_get_operator_information();

				break;

			case NETWORK_OPERATION_MODE:
				e_Current_sara_sequence = HTTP_MODE;

				fn_fprint("\r\n******NETWORK_OPERATION_MODE******\r\n");

				fn_get_defined_APN(0);

				fn_activate_PDP_context(0);

				//fn_get_activation_status(0);

				fn_get_dynamic_IP(0);

				break;

			case CELL_LOCATION_MODE:
				fn_fprint("\r\n******CELL_LOCATION_MODE******\r\n");
				//fn_mount_frame_sara(DAILY_UPDATE_FRAME);
				fn_cellLocate_localization_info();
				info_ok = fn_get_cellLocate_values();

				if (!info_ok) {
					fn_cellLocate_localization_info();
					info_ok = fn_get_cellLocate_values();
				}

				e_Current_sara_sequence = HTTP_MODE;

				break;

			case HTTP_MODE:
				e_Current_sara_sequence = QUALITY_SIGNAL_MODE;
				st_flag.en_HTTP = 0;
				st_flag.en_SMS = 1;
				fn_fprint("\r\n******HTTP_MODE******\r\n");

				fn_mount_frame_sara();

				if (!fn_reset_HTTP_profile(0)) {
					break;
				}

				if (!fn_set_HTTP_wizeBox_serve_name(0)) {
					break;
				}

				if (fn_GET_request_to_wizebox_HTTP_server(0) == 2) {
					st_flag.new_downlink =
							fn_read_data_folder_responseFilename_HTTP();

					fn_delete_data_folder_responseFilename_HTTP();

					e_Current_sara_sequence = TURN_OFF_MODE;

					break;
				}

				break;

			case SMS_MODE:
				st_flag.en_HTTP = 1;
				st_flag.en_SMS = 0;
				e_Current_sara_sequence = QUALITY_SIGNAL_MODE;
				fn_fprint("\r\n******SMS_MODE******\r\n");

				if (fn_send_sms_payload()) {
					if (st_data_sensor_e.header == 2) {
						GPRS_Timer_EN = st_timer.GPRSenable;
						HAL_Delay(20000);
						if (fn_check_new_sms() == 2) {
							st_flag.new_downlink = fn_check_sms_upload();
						}
					}
					fn_delete_all_sms();
					e_Current_sara_sequence = TURN_OFF_MODE;
				}
				break;

			case TURN_OFF_MODE:
				fn_fprint("\r\n******TURN_OFF_MODE******\r\n");
				fn_power_off_sara();

				break;
			default:
				break;
			}
		}
	}
	fn_fprint("\r\n******TURN_OFF_MODE******\r\n");
	fn_power_off_sara();

}

//*****************   GERAL  FUNCTIONS    *********************
void fn_power_on_sara(void) {
	fn_fprint("\r\nPOWER ON SARA MODULE\r\n");
	GPRS_RESET_OFF
	HAL_Delay(100);
	GPRS_ON
	HAL_Delay(1000);
	GPRS_PWR_ON
	HAL_Delay(10000);
	fn_fprint("SARA MUDULE RUN\r\n");
}

void fn_power_off_sara(void) {
	fn_fprint("\r\nSARA MODULE SWITCH OFF\r\n");
	Start_Sara_Transfers("AT+CPWROFF\r\n");
	HAL_Delay(500);
	GPRS_OFF
	HAL_Delay(100);
	GPRS_PWR_OFF
	HAL_Delay(100);
	fn_fprint("SARA MUDULE OFF\r\n");
}

uint8_t fn_start_at_commands_sara() {

	uint8_t timeToScape = 10;
	uint8_t retorno = 1;

	while (!fn_at_command_saraG450("AT&K0") && timeToScape != 0) {
		HAL_Delay(100);
		timeToScape--;
	}
	retorno = fn_at_command_saraG450("AT");
	while (!retorno && timeToScape != 0) {
		HAL_Delay(100);
		timeToScape--;
		fn_fprint("\r\nAT COMMAND DONT START, TRY AGAIN\r\n");
		retorno = fn_at_command_saraG450("AT");

	}
	retorno = fn_at_command_saraG450("AT+CPIN?");
	while (!retorno && timeToScape != 0) {
		HAL_Delay(100);
		timeToScape--;
		fn_fprint("\r\nCHIP OFF, TRY AGAIN\r\n");
		retorno = fn_at_command_saraG450("AT+CPIN?");
	}
	return retorno;
}

void fn_get_module_info_sara(void) {

	fn_at_command_saraG450("AT+CMEE=2");
//  Manufacturer identification +CGMI
	fn_at_command_saraG450("AT+CGMI");
// Model identification +CGMM
	fn_at_command_saraG450("AT+CGMM");
//  Firmware version identification +CGMR
	fn_at_command_saraG450("AT+CGMR");

	fn_at_command_saraG450("ATI9");

	fn_at_command_saraG450("AT+CLCK=\"SC\",2");

	fn_at_command_saraG450("AT+CPIN?");

	fn_at_command_saraG450("AT+CCLK?");
// IMEI identification +CGSN
	fn_get_imei_sara();
// Indicator control
//fn_at_command_saraG450("AT+CIND?", 1);
	fn_indicator_control_command();

	//fn_set_automatic_network_selection();

//fn_print_indicator_control();

}

void fn_mount_frame_sara() {
	//char sara_FIRWARE[8];
	fn_encoder_report_frame(sara_data);

	itoa(sequencialMensage, sara_SEQNUMBER, 10);
	itoa((int) fn_check_signal_quality_status(), sara_RSSI, 10);
	//itoa((int) TimeCounter, sara_TIMESTAMP, 10);
	itoa((int) KeepAlive_Timer, sara_KEEPALIVE, 10);
	//itoa((int) FIRMWARE_VERSION, sara_FIRWARE, 10);

	memset(get_default_page_info, 0, 155);
	strncpy(get_default_page_info,
			"AT+UHTTPC=0,1,\"/gprs/bidir?ack=true&device=", 43);
	strncat(get_default_page_info, sara_imei, 15);
	strncat(get_default_page_info, "&data=", 6);
	strncat(get_default_page_info, sara_data, 24);
	/*	strncat(get_default_page_info, "&lat=-", 6);
	 strncat(get_default_page_info, latitude_SARA, 8);
	 strncat(get_default_page_info, "&lng=-", 6);
	 strncat(get_default_page_info, longitude_SARA, 8);*/
	strncat(get_default_page_info, "&rssi=-", 7);
	strncat(get_default_page_info, sara_RSSI, 2);
	strncat(get_default_page_info, "&op=", 4);
	strcat(get_default_page_info, sara_OPERATOR);
//strncat(get_default_page_info, "&snr=20", 7);
	strncat(get_default_page_info, "&kAtime=", 15);
	strncat(get_default_page_info, sara_KEEPALIVE, 5);
	//strncat(get_default_page_info, "&timeStamp=", 11);
	//strncat(get_default_page_info, sara_TIMESTAMP, 5);
	//strncat(get_default_page_info, "&firmwareVersion=", 17);
	//strncat(get_default_page_info, FIRMWARE_VERSION, 8);
	strncat(get_default_page_info, "&seqNumber=", 11);
	strncat(get_default_page_info, sara_SEQNUMBER, 3);
	strncat(get_default_page_info, "&http=1", 7);
	strcat(get_default_page_info, "\",\"data.fss\"\r\n");

}

//*****************     UART    *********************

//*****************   SARA UART    *********************
void Start_Sara_Transfers(char* info) {

	memset(aStringToSend_SARA, 0, 500);
	ubSizeToSend_SARA = strlen(info);
	char valor[1];
	for (int var = 0; var < ubSizeToSend_SARA; ++var) {
		aStringToSend_SARA[var] = info[var];
		valor[0] = aStringToSend_SARA[var];
		HAL_UART_Transmit(&hlpuart1, (uint8_t*) valor, 1, 50);

	}
	/* Start transfer only if not already ongoing */
	if (ubSend_SARA == 0) {
		/* Start USART transmission : Will initiate TXE interrupt after TDR register is empty */
		LL_USART_TransmitData8(USART5, aStringToSend_SARA[ubSend_SARA++]);
		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USART5);
	}

}

void USART5_CharTransmitComplete_Callback(void) {
	if (ubSend_SARA == ubSizeToSend_SARA) {
		ubSend_SARA = 0;

		/* Disable TC interrupt */
		LL_USART_DisableIT_TC(USART5);

	}
}

void USART5_TXEmpty_Callback(void) {
	if (ubSend_SARA == (ubSizeToSend_SARA - 1)) {
		/* Disable TXE interrupt */
		LL_USART_DisableIT_TXE(USART5);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USART5);
	}

	/* Fill TDR with a new char */
	LL_USART_TransmitData8(USART5, aStringToSend_SARA[ubSend_SARA++]);
}

void USART5_CharReception_Callback(void) {
//__IO uint32_t received_char;

	/* Read Received character. RXNE flag is cleared by reading of RDR register */
//received_char = LL_USART_ReceiveData8(USART4);
	aStringToReceiver_SARA[ubSizeToReceiver_SARA++] = LL_USART_ReceiveData8(
	USART5);	//received_char;
	/* Check if received value is corresponding to specific one : \r */
	if ((LL_USART_ReceiveData8(USART5) == 13)) {
		flag_sara = 1;
		/* Turn LED2 On : Expected character has been received */
		/* Echo received character on TX */

		/* Clear Overrun flag, in case characters have already been sent to USART */
		LL_USART_ClearFlag_ORE(USART5);

	}

//LL_USART_TransmitData8(USARTx_INSTANCE, received_char);
}

uint8_t fn_at_uart_reception_sara() {
	uint8_t ret = 0;
	if (flag_sara) {
		//HAL_UART_Transmit(&hlpuart1, aStringToReceiver_SARA,ubSizeToReceiver_SARA, 100);
		ubSizeToReceiver_SARA = 0;
		flag_sara = 0;
		fn_fprint((char*) aStringToReceiver_SARA);
		ret = 1;
	}
	return ret;
}

//**************************BASIC COMMANDS******************************************************

uint8_t fn_at_command_saraG450(char* at_command) {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint8_t timer_out = 100;
	int size_at_command = strlen(at_command);
	size_at_command += 2;
	char new_command[size_at_command];
	strcpy(new_command, at_command);
	strcat(new_command, "\r\n");

	Start_Sara_Transfers(new_command);
	HAL_Delay(100);

	while (timer_out != 0 && atReturn == 0) {

		atReturn = fn_at_uart_reception_sara();

		if (timer_out != 0) {
			HAL_Delay(100);
			timer_out--;
		}

	}
	if (strstr((char*) aStringToReceiver_SARA, "OK")) {
		atReturn = 1;
	}
	if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
		atReturn = 0;
	}
	return atReturn;
}

uint8_t fn_at_command_word_responce_saraG450(char* at_command, uint32_t msDelay,
		char* responce) {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;

	int size_at_command = strlen(at_command);
	size_at_command++;
	char new_command[size_at_command];
	strcpy(new_command, at_command);
	strcat(new_command, "\r");

	Start_Sara_Transfers(new_command);
	HAL_Delay(500);
	HAL_Delay(msDelay);
	fn_at_uart_reception_sara();

	int size_responce = strlen(responce);
	char new_responce[size_responce];
	strcpy(new_responce, responce);

//atReturn = find_word((char*) aStringToReceiver_SARA, new_responce);

	return atReturn;
}

uint8_t fn_check_sarag450_status() {
	uint8_t check = 0, count = 10, check_sim_card = 0;
	sequencialMensage = 0;
	GPRS_RESET_OFF
	HAL_Delay(100);
	GPRS_ON
	HAL_Delay(2000);
	GPRS_PWR_ON
	HAL_Delay(5000);
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

	char new_command[6] = "AT&K0\r";
	while (count) {
		count--;
		Start_Sara_Transfers(new_command);
		HAL_Delay(100);
		if (flag_sara) {
			ubSizeToReceiver_SARA = 0;
			flag_sara = 0;
		}

		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
				count = 0;
			}
		}
	}
	//fn_fprint("\r\nSARA IMEI:");
	// IMEI identification +CGSN
	check = fn_at_command_saraG450("AT+CGSN");
	check_sim_card = fn_at_command_saraG450("AT+CPIN?");

	fn_get_imei_sara();
	GPRS_PWR_OFF
	HAL_Delay(100);
	GPRS_OFF
	e_Current_sara_sequence = TURN_OFF_MODE;
	if (check_sim_card == 1)
		check = 2;
	return check;
}

uint8_t fn_indicator_control_command() {
	//char sara_SNR[2] = "00";
	char indicator_status[12];
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t c = 0;
	//uint8_t dec = 0;
	//uint8_t uni = 0;

	fn_at_command_saraG450("AT+CIND?");
	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SARA[var] == 44) {
			indicator_status[c] = aStringToReceiver_SARA[var - 1];
			c++;
		}
	}
	if (c != 0) {
		st_indicator_control_e.batterry_level = (indicator_status[0] - 48);
		st_indicator_control_e.signal_level = (indicator_status[1] - 48);
		st_indicator_control_e.network_service = (indicator_status[2] - 48);
		st_indicator_control_e.sounder_active = (indicator_status[3] - 48);
		st_indicator_control_e.unread_mensage = (indicator_status[4] - 48);
		st_indicator_control_e.call_progress = (indicator_status[5] - 48);
		st_indicator_control_e.roaming_network = (indicator_status[6] - 48);
		st_indicator_control_e.sms_full = (indicator_status[7] - 48);
		st_indicator_control_e.ps_indicator = (indicator_status[8] - 48);
		st_indicator_control_e.call_set_up = (indicator_status[9] - 48);
		st_indicator_control_e.call_on = (indicator_status[10] - 48);
		st_indicator_control_e.sim_detection = (indicator_status[11] - 48);
	}

	//uint8_t dec = (st_indicator_control_e.signal_level * 5) / 10;
	//uint8_t uni = (st_indicator_control_e.signal_level * 5) - (dec * 10);

	//sara_SNR[0] = dec + 48;
	//sara_SNR[1] = uni + 48;

	return c;
}

void fn_print_indicator_control(void) {
	if (fn_indicator_control_command() != 0) {
		fn_fprint("\r\n***INDICATOR CONTROL***\r\n");
		fn_fprint("battery charge level: ");
		fn_fprintnumber(st_indicator_control_e.batterry_level);
		fn_fprint("\r\n");

		fn_fprint("signal level: ");
		if (st_indicator_control_e.signal_level == 0)
			fn_fprint("< -150 dBm - very bad");
		if (st_indicator_control_e.signal_level == 1)
			fn_fprint("< -93 dBm - bad");
		if (st_indicator_control_e.signal_level == 2)
			fn_fprint("< -81 dBm - regular");
		if (st_indicator_control_e.signal_level == 3)
			fn_fprint("< -69 dBm - ok");
		if (st_indicator_control_e.signal_level == 4)
			fn_fprint("< -57 dBm - good");
		if (st_indicator_control_e.signal_level == 5)
			fn_fprint(">= -57 dBm - excelent");
		fn_fprint("\r\n");

		fn_fprint("network service availability: ");
		if (st_indicator_control_e.network_service == 0)
			fn_fprint(" not registered to any network");
		if (st_indicator_control_e.network_service == 1)
			fn_fprint(" registered to the network");
		fn_fprint("\r\n");

		/*fn_fprint("sounder activity: ");
		 if(st_indicator_control_e.sounder_active==0)
		 fn_fprint("no sound");
		 if(st_indicator_control_e.sounder_active==1)
		 fn_fprint(" sound");
		 fn_fprint("\r\n");*/

		/*fn_fprint("unread message available in <mem1> storage: ");
		 if(st_indicator_control_e.unread_mensage==0)
		 fn_fprint(" no messages");
		 if(st_indicator_control_e.unread_mensage==1)
		 fn_fprint(" messages");
		 fn_fprint("\r\n");*/

		/*fn_fprint("call in progress: ");
		 if(st_indicator_control_e.call_progress==0)
		 fn_fprint("not call");
		 if(st_indicator_control_e.call_progress==1)
		 fn_fprint(" call");
		 fn_fprint("\r\n");*/

		fn_fprint("registration on a roaming network: ");
		if (st_indicator_control_e.roaming_network == 0)
			fn_fprint(" not roaming");
		if (st_indicator_control_e.roaming_network == 1)
			fn_fprint(" in roaming");
		fn_fprint("\r\n");

		/*		fn_fprint("SMS storage full");
		 if(st_indicator_control_e.sms_full==0)
		 fn_fprint(" SMS storage not full");
		 if(st_indicator_control_e.sms_full==1)
		 fn_fprint(" SMS storage full");
		 fn_fprint("\r\n");*/

		fn_fprint("PS indication status: ");
		if (st_indicator_control_e.ps_indicator == 0)
			fn_fprint(" no PS available in the network");
		if (st_indicator_control_e.ps_indicator == 1)
			fn_fprint(" PS available in the network but not registered");
		if (st_indicator_control_e.ps_indicator == 2)
			fn_fprint(" registered to PS");

		fn_fprint("\r\n");

		/*fn_fprint("callsetup:");
		 if(st_indicator_control_e.call_set_up==0)
		 fn_fprint("no call set-up");
		 if(st_indicator_control_e.call_set_up==1)
		 fn_fprint("incoming call not accepted or rejected");
		 if(st_indicator_control_e.call_set_up==2)
		 fn_fprint("outgoing call in dialling state");
		 if(st_indicator_control_e.call_set_up==3)
		 fn_fprint("outgoing call in remote party alerting state");
		 fn_fprint("\r\n");*/

		/*fn_fprint("call on hold:");
		 if(st_indicator_control_e.call_on==0)
		 fn_fprint(" no calls on hold");
		 if(st_indicator_control_e.call_on==1)
		 fn_fprint(" calls on hold");
		 fn_fprint("\r\n");*/

		fn_fprint("SIM detection: ");
		if (st_indicator_control_e.sim_detection == 0)
			fn_fprint("not detected");
		if (st_indicator_control_e.sim_detection == 1)
			fn_fprint(" detected");
		if (st_indicator_control_e.sim_detection == 2)
			fn_fprint(" not available service");
		fn_fprint("\r\n");

	} else
		fn_fprint("\r\nERROR");
}

uint8_t fn_check_attach_GPRS() {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint8_t check = 0;
	uint8_t timer = 255;
	Start_Sara_Transfers("AT+CGATT?\r");
	while (check == 0 && timer != 0) {
		HAL_Delay(100);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
			}
		}
		if (timer != 0)
			timer--;
	}
	if (timer == 0)
		fn_fprint("\r\nTIME OUT\r");
	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SARA[var] == 58) {
			atReturn = (aStringToReceiver_SARA[var++] - 48);
		}
	}
	return atReturn;
}

uint8_t fn_set_attach_GPRS() {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint8_t check = 0;
	uint8_t timer = 255;
	Start_Sara_Transfers("AT+CGATT=1\r");
	while (check == 0 && timer != 0) {
		HAL_Delay(100);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
			}
		}
		if (timer != 0)
			timer--;
	}
	if (timer == 0)
		fn_fprint("\r\nTIME OUT\r");
	return atReturn;
}

uint8_t fn_set_GPRS_network_registration_enable() {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

	uint8_t check = 0;
	uint8_t timer = 255;
	Start_Sara_Transfers("AT+CGREG=1\r");
	while (check == 0 && timer != 0) {
		HAL_Delay(100);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
			}
		}
		if (timer != 0)
			timer--;
	}
	if (timer == 0)
		fn_fprint("\r\nTIME OUT\r");
	return check;
}

uint8_t fn_set_Packet_switched_data_action() {
	uint8_t check = 0;
	uint8_t timer = 255;
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+UPSDA=1,0\r");
	while (check == 0 && timer != 0) {
		HAL_Delay(100);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
			}
		}
		if (timer != 0)
			timer--;
	}
	if (timer == 0)
		fn_fprint("\r\nTIME OUT\r");

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+UPSDA=1,1\r");
	while (check == 0 && timer != 0) {
		HAL_Delay(100);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
			}
		}
		if (timer != 0)
			timer--;
	}
	if (timer == 0)
		fn_fprint("\r\nTIME OUT\r");

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+UPSDA=1,3\r");
	while (check == 0 && timer != 0) {
		HAL_Delay(100);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
			}
		}
		if (timer != 0)
			timer--;
	}
	if (timer == 0)
		fn_fprint("\r\nTIME OUT\r");
	return check;
}

uint64_t fn_get_imei_sara() {
	uint64_t imei = 0;
	uint8_t counter_imei = 0;
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+CGSN\r");
	HAL_Delay(100);
	fn_at_uart_reception_sara();
	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SARA[var] > 47
				&& aStringToReceiver_SARA[var] < 58) {
			sara_imei[counter_imei] = aStringToReceiver_SARA[var];
			if (counter_imei == 15)
				break;
			counter_imei++;
		}
	}
	imei = atoll(sara_imei);
	return imei;
}

//*************************ADVANCE COMMANDS*******************************************************

//NETWORK
uint8_t fn_check_signal_quality_status() {

	uint8_t validacao = 0;
	validar: memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t d = 0;
	uint8_t u = 0;
	uint8_t retorno = 0;
	Start_Sara_Transfers("AT+CSQ\r\n");
	HAL_Delay(200);
	fn_at_uart_reception_sara();
	if (strstr((char*) aStringToReceiver_SARA, "OK")) {
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {

			if (aStringToReceiver_SARA[var] == 44) {
				u = (aStringToReceiver_SARA[var - 1] - 48);
				d = (aStringToReceiver_SARA[var - 2] - 48);
				retorno = (d * 10) + u;
				break;
				/*			if (aStringToReceiver_SARA[var + 1] == 57
				 || aStringToReceiver_SARA[var + 1] == 48) {



				 u = (aStringToReceiver_SARA[var - 1] - 48);
				 if (aStringToReceiver_SARA[var - 2] >= 48)
				 d = (aStringToReceiver_SARA[var - 2] - 48);
				 if (d > 0)
				 retorno = (d * 10) + u;
				 else
				 retorno = u;

				 itoa((int) retorno, sara_RSSI, 10);
				 break;
				 }*/
			}
		}
		itoa((int) retorno, sara_RSSI, 10);
	}

	if (retorno == 99) {
		retorno = 0;
	}

	if (retorno != 0 && validacao != 3) {
		validacao++;
		goto validar;
	}

	return retorno;
}

// ********************** NETWORK  *****************************

uint8_t fn_get_operator_information() {
	memset(sara_OPERATOR, 0, 5);
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint8_t timer_out = 5;

	Start_Sara_Transfers("AT+COPS?\r\n");
	HAL_Delay(500);

	while (timer_out) {

		fn_at_uart_reception_sara();

		HAL_Delay(1000);
		timer_out--;

		if (strstr((char*) aStringToReceiver_SARA, "OK")) {
			strncpy(sara_OPERATOR, "NONE", 4);
			atReturn = 1;
			if (strstr((char*) aStringToReceiver_SARA, "TIM")) {
				memset(sara_OPERATOR, 0, 5);
				atReturn = 2;
				strncpy(sara_OPERATOR, "TIM", 3);
				break;
			}
			if (strstr((char*) aStringToReceiver_SARA, "Oi")) {
				memset(sara_OPERATOR, 0, 5);
				atReturn = 3;
				strncpy(sara_OPERATOR, "OI", 2);
				break;
			}
			if (strstr((char*) aStringToReceiver_SARA, "Claro")) {
				memset(sara_OPERATOR, 0, 5);
				atReturn = 4;
				strncpy(sara_OPERATOR, "CLARO", 5);
				break;
			}
			if (strstr((char*) aStringToReceiver_SARA, "VIVO")) {
				memset(sara_OPERATOR, 0, 5);
				atReturn = 5;
				strncpy(sara_OPERATOR, "VIVO", 4);
				break;
			}
		}

		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			atReturn = 0;
			break;
		}
	}
	return atReturn;
}

uint8_t fn_set_automatic_network_selection() {

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint8_t timer_out = 25;

	Start_Sara_Transfers("AT+COPS=0\r\n");
	HAL_Delay(1000);
	while (timer_out != 0) {
		fn_at_uart_reception_sara();
		if (strstr((char*) aStringToReceiver_SARA, "OK")) {
			atReturn = 1;
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			atReturn = 0;
			break;
		}
		HAL_Delay(1000);
		timer_out--;
	}

	return atReturn;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_get_list_of_operators() {
	uint8_t check = 0;
	/*struct {
	 char OP1[5], OP2[5], OP3[5], OP4[5];
	 } st_operador_id;

	 memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

	 uint8_t timer = 200;
	 uint8_t op_code_counter = 0;

	 Start_Sara_Transfers("AT+COPS=?\r\n");

	 while (timer != 0 && check == 0) {
	 HAL_Delay(1000);
	 fn_at_uart_reception_sara();
	 for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
	 if (aStringToReceiver_SARA[var - 1] == 79
	 && aStringToReceiver_SARA[var] == 75) {
	 check = 1;
	 }
	 if (aStringToReceiver_SARA[var - 3] == 69
	 && aStringToReceiver_SARA[var - 2] == 82
	 && aStringToReceiver_SARA[var - 1] == 82
	 && aStringToReceiver_SARA[var] == 79) {
	 timer = 0;
	 }
	 }
	 if (timer != 0)
	 timer--;
	 }

	 if (check == 1) {
	 for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
	 if (aStringToReceiver_SARA[var] == 44
	 && aStringToReceiver_SARA[var - 1] == 41
	 && aStringToReceiver_SARA[var - 2] == 34) {
	 if (op_code_counter == 3) {
	 st_operador_id.OP1[0] = aStringToReceiver_SARA[var - 6];
	 st_operador_id.OP1[1] = aStringToReceiver_SARA[var - 5];
	 st_operador_id.OP1[2] = aStringToReceiver_SARA[var - 4];
	 st_operador_id.OP1[3] = aStringToReceiver_SARA[var - 3];
	 st_operador_id.OP1[4] = aStringToReceiver_SARA[var - 2];
	 op_code_counter++;
	 }
	 if (op_code_counter == 2) {
	 st_operador_id.OP2[0] = aStringToReceiver_SARA[var - 6];
	 st_operador_id.OP2[1] = aStringToReceiver_SARA[var - 5];
	 st_operador_id.OP2[2] = aStringToReceiver_SARA[var - 4];
	 st_operador_id.OP2[3] = aStringToReceiver_SARA[var - 3];
	 st_operador_id.OP2[4] = aStringToReceiver_SARA[var - 2];
	 op_code_counter++;
	 }
	 if (op_code_counter == 1) {
	 st_operador_id.OP3[0] = aStringToReceiver_SARA[var - 6];
	 st_operador_id.OP3[1] = aStringToReceiver_SARA[var - 5];
	 st_operador_id.OP3[2] = aStringToReceiver_SARA[var - 4];
	 st_operador_id.OP3[3] = aStringToReceiver_SARA[var - 3];
	 st_operador_id.OP3[4] = aStringToReceiver_SARA[var - 2];
	 op_code_counter++;
	 }
	 if (op_code_counter == 0) {
	 st_operador_id.OP4[0] = aStringToReceiver_SARA[var - 6];
	 st_operador_id.OP4[1] = aStringToReceiver_SARA[var - 5];
	 st_operador_id.OP4[2] = aStringToReceiver_SARA[var - 4];
	 st_operador_id.OP4[3] = aStringToReceiver_SARA[var - 3];
	 st_operador_id.OP4[4] = aStringToReceiver_SARA[var - 2];
	 op_code_counter++;
	 }

	 }
	 }
	 }*/
	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_get_registration_status() {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint8_t timer = 10;
	uint8_t n = 0, stat = 0;

	fn_at_command_saraG450("AT+CREG=2");
	HAL_Delay(100);
	fn_at_uart_reception_sara();
	HAL_Delay(100);

	while (atReturn == 0 && timer != 0) {
		memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
		fn_at_command_saraG450("AT+CREG?");
		if (timer != 0) {
			timer--;
		}
		HAL_Delay(200);
		fn_at_uart_reception_sara();
		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var] == 44) {
				n = aStringToReceiver_SARA[var - 1] - 48;
				stat = aStringToReceiver_SARA[var + 1] - 48;
				if (((n == 0) || (n == 2)) && ((stat == 1) || (stat == 5))) {
					atReturn = 1;
					break;
				}
			}
		}
	}

	if (timer == 0)
		fn_fprint("TIME OUT\r");

	return atReturn;
//Return network registration status -> accept values = (1) registered, home network or (5) registered, roaming
}

void fn_get_RSSI_QS(uint8_t signal) {
	unsigned int valor = -2.0295 * signal + 112.48;
	unsigned int unidade, dezena;
	dezena = valor / 10;
	unidade = valor - (dezena * 10);
	if (valor < 100) {
		sara_RSSI[0] = dezena + 48;
		sara_RSSI[1] = unidade + 48;
	} else {
		strcpy(sara_RSSI, "99");
	}
	/*	fn_fprint("\r\nRSSI= -");
	 fn_fprint(sara_RSSI);
	 fn_fprint("dBm\r\n");*/
}

//PSD

// ********************** PSD  *****************************

uint8_t fn_get_defined_APN(uint8_t profile) {
	uint8_t check = 0;
	check = fn_at_command_saraG450("AT+UPSD=0,1,\"wize.algar.br\"");
//HAL_UART_Transmit(&hlpuart1, (uint8_t*) aStringToReceiver_SARA, SARA_BYTES_TO_RECEIVER, 1000);
	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_get_dynamic_IP(uint8_t profile) {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t check = 0;
	uint8_t timer_out = 2;
	Start_Sara_Transfers("AT+UPSND=1,0\r\n");
	HAL_Delay(1000);
	while (timer_out != 0) {
		fn_at_uart_reception_sara();
		if (strstr((char*) aStringToReceiver_SARA, "OK")) {
			if (strstr((char*) aStringToReceiver_SARA, "\"\"")) {
				check = 0;
			} else {
				check = 1;
			}
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			check = 0;
			break;
		}

		if (timer_out != 0) {
			HAL_Delay(1000);
			timer_out--;
		}

	}

	return check;
//Return options -> (1)= active; (0)= no active
}

uint8_t fn_get_activation_status(uint8_t profile) {

	uint8_t check = 0;
	uint8_t timer_out = 20;
	Start_Sara_Transfers("AT+UPSND=1,0\r\n");
	HAL_Delay(1000);
	while (timer_out != 0) {
		fn_at_uart_reception_sara();
		if (strstr((char*) aStringToReceiver_SARA, "OK")) {
			check = 1;
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			check = 0;
			break;
		}

		if (timer_out != 0) {
			HAL_Delay(1000);
			timer_out--;
		}

	}
	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_get_ip_adres(uint8_t profile) {

	uint8_t check = 0;
	uint8_t timer_out = 20;
	Start_Sara_Transfers("AT+UPSND=0,0\r\n");
	HAL_Delay(1000);
	while (timer_out != 0) {
		fn_at_uart_reception_sara();
		if (strstr((char*) aStringToReceiver_SARA, "OK")) {
			check = 1;
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			check = 0;
			break;
		}

		if (timer_out != 0) {
			HAL_Delay(1000);
			timer_out--;
		}

	}
	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_activate_PDP_context(uint8_t profile) {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t check = 0;
	check = fn_at_command_saraG450("AT+UPSDA=1,3");
	return check;
//Return options -> (1)= ok; (0)= no ok
}

// ********************** HTTP  *****************************

uint8_t fn_reset_HTTP_profile(uint8_t profile) {
	HAL_Delay(500);
	uint8_t check = 0;

	check = fn_at_command_saraG450("AT+UHTTP=0");

	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_set_HTTP_wizeBox_serve_name(uint8_t profile) {
	HAL_Delay(500);

	uint8_t check = 0;

	check = fn_at_command_saraG450(
			"AT+UHTTP=0,1,\"wizebox-api-prod.herokuapp.com\"");

	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_GET_request_to_wizebox_HTTP_server(uint8_t profile) {

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	uint16_t timer_out = 500;

	Start_Sara_Transfers(get_default_page_info);
	HAL_Delay(600);
	if (strstr((char*) aStringToReceiver_SARA, "OK")) {
		atReturn = 1;
	}
	while (timer_out != 0) {
		fn_at_uart_reception_sara();
		if (strstr((char*) aStringToReceiver_SARA, "0,1,1")) {
			atReturn = 2;
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			atReturn = 0;
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "0,1,0")) {
			atReturn = 0;
			break;
		}
		HAL_Delay(100);
		timer_out--;
	}

	return atReturn;

//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_read_data_folder_responseFilename_HTTP() {

	uint8_t check = 0;
	uint8_t timer_out = 255;
	char * pch;
	char buffer[19];

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+URDFILE=\"data.fss\"\r\n");
	HAL_Delay(1000);
	while (timer_out != 0 && check == 0) {

		check = fn_at_uart_reception_sara();

		if (timer_out != 0) {
			HAL_Delay(100);
			timer_out--;
		}

	}
	if (strstr((char*) aStringToReceiver_SARA, "OK")) {
		fn_fprint("OK\r\n");
	}
	if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
		check = 0;
		fn_fprint("ERROR\r\n");
	}

//check = fn_at_command_saraG450("AT+URDFILE=\"data.fss\"\r", 2);

	/*	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {

	 if (aStringToReceiver_SARA[var] == 52
	 && aStringToReceiver_SARA[var + 1] == 48
	 && aStringToReceiver_SARA[var + 2] == 52) {
	 fn_fprint("\r\nPAGE ERROR \r\n\r\n");
	 check = 0;
	 }
	 }*/
	if (check) {
		pch = strstr((char*) aStringToReceiver_SARA, "TX=");
		check = 1;
		strncpy(buffer, pch, 19);
		for (int c = 0; c < 19; c++) {
			if (c < 16)
				buffer[c] = buffer[c + 3];
			else
				buffer[c] = 0;

		}
		strncpy(downlink_data, buffer, 16);

	}

	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_delete_data_folder_responseFilename_HTTP() {

	uint8_t check = 0;

	check = fn_at_command_saraG450("AT+UDELFILE=\"data.fss\"");

	return check;
//Return options -> (1)= ok; (0)= no ok
}

// ********************** SMS  *****************************

uint8_t fn_delete_all_sms() {
	uint8_t check = 0;
	fn_fprint("\rDELETE ALL SMS\r\n");
	check = fn_at_command_saraG450("AT+CMGD=0,4");
	return check;
}

uint8_t fn_set_preferred_sms_format_text() {
	uint8_t check = 0;
	check = fn_at_command_saraG450("AT+CMGF=1");
	return check;
//Return options -> (1)= ok; (0)= no ok
}

uint8_t fn_read_all_sms_mensages() {
	uint8_t check = 0;
	return check;
}

uint8_t fn_delete_all_read_sms() {
	uint8_t check = 0;
	return check;
}

uint8_t fn_set_sms_center() {
	uint8_t check = 0;
	return check;

}

uint8_t fn_send_sms_text(char* message) {
	uint8_t check = 0;
	return check;
}

uint8_t fn_send_sms() {

	uint8_t check = 0;
//uint8_t timer = 100;

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+CMGF=1\r");
	HAL_Delay(100);
	fn_at_uart_reception_sara();
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	HAL_Delay(1000);
	Start_Sara_Transfers(
			"AT+CMGS=\"27981093300\"\r\"Hello Programmer, its a send test tks!!\"\x1A");
	HAL_Delay(5000);
	fn_at_uart_reception_sara();

	/*while (check == 0 && timer != 0) {
	 HAL_Delay(1000);
	 fn_at_uart_reception_sara();
	 for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
	 if (aStringToReceiver_SARA[var - 1] == 79
	 && aStringToReceiver_SARA[var] == 75) {
	 check = 1;
	 }
	 }
	 if (timer != 0)
	 timer--;
	 }
	 if (timer == 0)
	 fn_fprint("\r\nTIME OUT\r");*/

	return check;
}

uint8_t fn_check_new_sms() {
	fn_fprint("CHECK SMS\r\n");
	uint8_t check = 0;
	unsigned int timer = 10000;
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+CMGF=1\r");
	HAL_Delay(200);
	fn_at_uart_reception_sara();
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+CMGL=\"ALL\"\r");
	HAL_Delay(1000);
	while (fn_at_uart_reception_sara() == 0 && timer != 0) {
		HAL_Delay(1);
		if (timer != 0) {
			timer--;

		}
	}
	fn_fprint("\r\n");

	if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
		fn_fprint("\r\nERRO SMS\r\n");

	} else if (strstr((char*) aStringToReceiver_SARA, "+CMGL:")) {
		fn_fprint("\r\nNEW SMS\r\n");
		check = 2;
	} else {
		fn_fprint("\r\nNO SMS\r\n");
		check = 1;
	}

	return check;
}

uint8_t fn_send_sms_payload() {

	fn_encoder_report_frame(sara_data);
	fn_get_payload_for_sms();

	uint8_t check = 5;
	//unsigned int timer = 60000;
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	//Start_Sara_Transfers("ATE0\r");
	//HAL_Delay(100);
	fn_at_uart_reception_sara();
	HAL_Delay(100);
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers("AT+CMGF=1\r");
	HAL_Delay(100);
	while (!fn_at_uart_reception_sara())
		HAL_Delay(500);
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

//strncat(sms_mensage, "\x1A", 1);

	//Start_Sara_Transfers("AT+CMGS=\"16996826437\"\r"); //wize
	Start_Sara_Transfers("AT+CMGS=\"016990789568\"\r"); //cietec
	//Start_Sara_Transfers("AT+CMGS=\"027981093300\"\r"); //programming

	HAL_Delay(1000);

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	Start_Sara_Transfers(SMS_buffer);
	fn_fprint(SMS_buffer);
	HAL_Delay(100);
	Start_Sara_Transfers("\x1A");
	HAL_Delay(200);

	unsigned int cont = 0;
	fn_fprint("\r\n");
	LED_ON
	while (check) {
		fn_at_uart_reception_sara();
		//strstr((char*) aStringToReceiver_SARA, "+CMGS:") ||
		if (strstr((char*) aStringToReceiver_SARA, "OK")) {
			check = 1;
			break;
		}
		if (strstr((char*) aStringToReceiver_SARA, "ERRO")) {
			check = 0;
			break;
		}
		if ((cont > 300)) {
			check = 0;
			break;
		}
		HAL_Delay(100);
		fn_fprint("\r  ");
		fn_fprintnumber(cont++);
		fn_fprint("  \r");
	}

	return check;
}

uint8_t fn_answer_sms_message() {
	//uint8_t option = 0;
	uint8_t check = 0;
	/*	unsigned int timer = 60000;
	 char at_command_complete[23] = { 0 };
	 char loc[14] = { 0 };
	 //char number_res[12] = { 0 };
	 char * pch;

	 if (strstr((char*) aStringToReceiver_SARA, "STATUS?"))
	 option = 1;
	 if (strstr((char*) aStringToReceiver_SARA, "PAYLOAD?"))
	 option = 2;
	 if (strstr((char*) aStringToReceiver_SARA, "DADOS?"))
	 option = 3;
	 if (strstr((char*) aStringToReceiver_SARA, "LORA="))
	 option = 4;
	 if (strstr((char*) aStringToReceiver_SARA, "TEMPO="))
	 option = 5;
	 if (strstr((char*) aStringToReceiver_SARA, "SIGFOX="))
	 option = 6;
	 if (strstr((char*) aStringToReceiver_SARA, "HTTP="))
	 option = 7;
	 if (strstr((char*) aStringToReceiver_SARA, "GPS="))
	 option = 8;
	 if (strstr((char*) aStringToReceiver_SARA, "SMS="))
	 option = 9;
	 if (strstr((char*) aStringToReceiver_SARA, "BLE="))
	 option = 10;

	 if (option != 0) {

	 switch (option) {
	 case 1:
	 fn_get_status_for_sms();
	 fn_fprint("STATUS REQUEST\r\n");
	 break;
	 case 2:
	 fn_get_payload_for_sms();
	 fn_fprint("PAYLOAD REQUEST\r\n");
	 break;
	 case 3:
	 fn_get_data_for_sms();
	 fn_fprint("DATA REQUEST\r\n");
	 break;
	 case 4:
	 fn_fprint("CONFIG LORA\r\n");
	 if (strstr((char*) aStringToReceiver_SARA, "ATIVAR")) {
	 st_flag.en_LoRa = 1;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "LORA ATIVADO");
	 }
	 if (strstr((char*) aStringToReceiver_SARA, "DESATIVAR")) {
	 st_flag.en_LoRa = 0;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "LORA DESATIVADO");
	 }
	 break;
	 case 5:
	 fn_fprint("CONFIG TIME\r\n");

	 for (int var = 0; var < SARA_BYTES_TO_RECEIVER; ++var) {
	 if (aStringToReceiver_SARA[var] >= 48
	 && aStringToReceiver_SARA[var] <= 57) {
	 sara_KEEPALIVE[0] = aStringToReceiver_SARA[var];
	 sara_KEEPALIVE[1] = aStringToReceiver_SARA[var + 1];
	 sara_KEEPALIVE[2] = aStringToReceiver_SARA[var + 2];
	 sara_KEEPALIVE[3] = aStringToReceiver_SARA[var + 3];
	 sara_KEEPALIVE[4] = aStringToReceiver_SARA[var + 4];

	 itoa((int) KeepAlive_Timer, sara_KEEPALIVE, 10);
	 fn_fprint("new time ");
	 fn_fprint(sara_KEEPALIVE);
	 fn_fprint("s\r\n");
	 break;
	 }
	 }
	 break;
	 case 6:
	 fn_fprint("CONFIG SIGFOX\r\n");
	 if (strstr((char*) aStringToReceiver_SARA, "ATIVAR")) {
	 st_flag.en_SiGFOX = 1;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "SIGFOX ATIVADO");
	 }
	 if (strstr((char*) aStringToReceiver_SARA, "DESATIVAR")) {
	 st_flag.en_SiGFOX = 0;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "SIGFOX DESATIVADO");
	 }
	 break;
	 case 7:
	 fn_fprint("CONFIG HTTP\r\n");
	 if (strstr((char*) aStringToReceiver_SARA, "ATIVAR")) {
	 st_flag.en_HTTP = 1;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "TRANSMISSAO VIA HTTP ATIVADO");
	 }
	 if (strstr((char*) aStringToReceiver_SARA, "DESATIVAR")) {
	 st_flag.en_HTTP = 0;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "TRANSMISSAO VIA HTTP DESATIVADO");
	 }
	 break;
	 case 8:
	 fn_fprint("CONFIG GPS\r\n");
	 if (strstr((char*) aStringToReceiver_SARA, "ATIVAR")) {
	 st_flag.en_GPS = 1;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "GPS ATIVADO");

	 }
	 if (strstr((char*) aStringToReceiver_SARA, "DESATIVAR")) {
	 st_flag.en_GPS = 0;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "GPS DESATIVADO");
	 }
	 break;
	 case 9:
	 fn_fprint("CONFIG SMS\r\n");
	 if (strstr((char*) aStringToReceiver_SARA, "ATIVAR")) {
	 st_flag.en_SMS = 1;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "SMS ATIVADO");

	 }
	 if (strstr((char*) aStringToReceiver_SARA, "DESATIVAR")) {
	 st_flag.en_SMS = 0;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "SMS DESATIVADO");
	 }
	 break;
	 case 10:
	 fn_fprint("CONFIG BLE\r\n");
	 if (strstr((char*) aStringToReceiver_SARA, "ATIVAR")) {
	 st_flag.en_BLE = 1;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "BLE ATIVADO");
	 }
	 if (strstr((char*) aStringToReceiver_SARA, "DESATIVAR")) {
	 st_flag.en_BLE = 0;
	 memset(SMS_buffer, 0, 1000);
	 strcpy(SMS_buffer, "CONFIGURACAO OK\n");
	 strcat(SMS_buffer, "BLE DESATIVADO");
	 }
	 break;

	 }

	 pch = strstr((char*) aStringToReceiver_SARA, "\"0");
	 strncpy(loc, pch, 14);
	 strncpy(at_command_complete, "AT+CMGS=", 8);
	 strncat(at_command_complete, loc, 14);
	 at_command_complete[22] = 13;

	 memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	 Start_Sara_Transfers("AT+CMGF=1\r");
	 HAL_Delay(100);
	 fn_at_uart_reception_sara();
	 memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

	 Start_Sara_Transfers(at_command_complete);
	 HAL_Delay(500);

	 memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

	 Start_Sara_Transfers(SMS_buffer);
	 HAL_Delay(200);
	 Start_Sara_Transfers("\x1A");
	 HAL_Delay(600);
	 fn_fprint("\r\n");
	 fn_fprint(SMS_buffer);
	 fn_fprint("\r\n");
	 unsigned int cont = 0;
	 while (fn_at_uart_reception_sara() == 0 && timer != 0) {
	 HAL_Delay(1);
	 if (timer != 0) {
	 timer--;

	 }
	 }
	 fn_fprint("\r\n");
	 LED_ON
	 while (!check) {
	 if (strstr((char*) aStringToReceiver_SARA, "+CMGS:"))
	 check = 1;
	 if (strstr((char*) aStringToReceiver_SARA, "ERRO"))
	 check = 5;
	 HAL_Delay(1);
	 fn_fprint("\r ");
	 fn_fprintnumber(cont++);
	 fn_fprint("  \r");
	 if (cont > 1800)
	 check = 5;
	 }
	 LED_OFF
	 fn_fprint("\r\n");
	 }*/
	return check;
}

uint8_t fn_check_sms_upload() {
	memset(downlink_data, 0, 16);
	uint8_t check = 0;
	char * pch;
	char buffer[19] = { 0 };

	if (strstr((char*) aStringToReceiver_SARA, "TX=")) {
		pch = strstr((char*) aStringToReceiver_SARA, "TX=");
		strncpy(buffer, pch, 19);
		check = 1;
		st_flag.new_downlink = 1;
		for (int c = 0; c < 19; c++) {
			if (c < 16)
				buffer[c] = buffer[c + 3];
			else
				buffer[c] = 0;

		}
		strncpy(downlink_data, buffer, 16);
	}
	return check;
}
//**************** CELL_LOC  *********************************

uint8_t fn_cellLocate_localization_info() {
	uint8_t check = 0;

	check = fn_at_command_saraG450("AT+ULOC=2,2,1,100,2000");
	HAL_Delay(5000);

	return check;
}

uint8_t fn_get_cellLocate_values() {
	uint8_t check = 0;
	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SARA[var] == 43
				&& aStringToReceiver_SARA[var + 1] == 85
				&& aStringToReceiver_SARA[var + 2] == 85) {
			latitude_SARA[0] = aStringToReceiver_SARA[var + 33];
			latitude_SARA[1] = aStringToReceiver_SARA[var + 34];
			latitude_SARA[2] = aStringToReceiver_SARA[var + 35];
			latitude_SARA[3] = aStringToReceiver_SARA[var + 36];
			latitude_SARA[4] = aStringToReceiver_SARA[var + 37];
			latitude_SARA[5] = aStringToReceiver_SARA[var + 38];
			latitude_SARA[6] = aStringToReceiver_SARA[var + 39];
			latitude_SARA[7] = aStringToReceiver_SARA[var + 40];

			longitude_SARA[0] = aStringToReceiver_SARA[var + 45];
			longitude_SARA[1] = aStringToReceiver_SARA[var + 46];
			longitude_SARA[2] = aStringToReceiver_SARA[var + 47];
			longitude_SARA[3] = aStringToReceiver_SARA[var + 48];
			longitude_SARA[4] = aStringToReceiver_SARA[var + 49];
			longitude_SARA[5] = aStringToReceiver_SARA[var + 50];
			longitude_SARA[6] = aStringToReceiver_SARA[var + 51];
			longitude_SARA[7] = aStringToReceiver_SARA[var + 52];

			st_indicator_control_e.lat_sara = ((latitude_SARA[0] - 48) * 1000000
					+ (latitude_SARA[1] - 48) * 100000
					+ (latitude_SARA[3] - 48) * 10000
					+ (latitude_SARA[4] - 48) * 1000
					+ (latitude_SARA[5] - 48) * 100
					+ (latitude_SARA[6] - 48) * 10 + (latitude_SARA[7] - 48));

			st_indicator_control_e.lng_sara = ((longitude_SARA[0] - 48)
					* 1000000 + (longitude_SARA[1] - 48) * 100000
					+ (longitude_SARA[3] - 48) * 10000
					+ (longitude_SARA[4] - 48) * 1000
					+ (longitude_SARA[5] - 48) * 100
					+ (longitude_SARA[6] - 48) * 10 + (longitude_SARA[7] - 48));

		}
	}

	if ((st_indicator_control_e.lng_sara != 0
			&& st_indicator_control_e.lat_sara != 0)
			&& (st_indicator_control_e.lng_sara != 1000000
					&& st_indicator_control_e.lat_sara != 5500000)) {
		check = 1;
		fn_fprint("\r\nlatutude: -");
		fn_fprint(latitude_SARA);
		fn_fprint(" longitude: -");
		fn_fprint(longitude_SARA);
		fn_fprint("\r\n");
	}

	return check;
}
