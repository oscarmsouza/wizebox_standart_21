/*
 * ninaB1.c
 *
 *  Created on: 6 de abr de 2020
 *      Author: oscar
 */

#include "ninaB1.h"
#include "stm32l0xx_it.h"
#include "saraG450.h"
char teste[5] = { 0 };

__IO uint8_t ubSend = 0;
uint8_t aStringToSend[500];
uint8_t ubSizeToSend = 0; //sizeof(aStringToSend);
uint8_t aStringToReceiver[BYTES_NINA_TO_RECEIVER];
uint8_t ubSizeToReceiver = 0;
char lsm_data_transfer[50], slave_ID[13];

//******************** MAIN BLE **************************

void fn_Change_Nina_Config_Type(e_nina_config_type nina_state) {
	//e_Previous_nina_config_type = e_Current_nina_config_type;
	//e_Current_nina_config_type = nina_state;
}

void fn_main_nina(void) {
	if (st_flag.BLE == 1 && st_flag.en_BLE == 1) {
		switch (e_Current_nina_config_type) {
		case OFF_MODE:
			st_flag.en_BLE = 0;
			break;
		case CONFIG_MODE:
			e_Current_nina_config_type = OFF_MODE;
			fn_fprint("BLE_INIT_MODE\r\n");
			fn_turn_on_nina();
			LED_CHANGE

			if (!fn_at_command_attention())
				fn_at_command_escape_at();

			fn_at_command_get_serial_number();
			blink(1);
			fn_at_command_get_model_id();
			blink(1);
			fn_at_command_get_manufacture_id();
			blink(1);
			fn_at_command_set_new_name("wBox_Config");
			blink(1);
			fn_at_command_set_connectable();
			blink(1);
			fn_at_command_set_discoverable();
			blink(1);
			fn_at_command_set_pairable();
			blink(1);
			fn_at_command_set_perimetral();
			blink(1);
			fn_at_command_storing();
			blink(1);
			fn_at_command_reboot_device();
			fn_fprint("WAITING FOR CONNECTION\r\n");
			blink(1);
			LED_ON
			BLE_timer_EN = st_timer.BLEenable + TimeCounter;
			uint8_t receiver = 0;
			while (!receiver && (TimeCounter != BLE_timer_EN)) {
				receiver = fn_wait_for_connection();
			}
			if (receiver == 1) {
				fn_at_command_data_mode();
				fn_fprint("CONNECTED\r\n");
				StartTransfers("OK\r");
				blink(2);
				LED_ON

				BLE_timer_EN = st_timer.BLEenable + TimeCounter;
				uint8_t _somaReceiver = 0;
				while ((TimeCounter != BLE_timer_EN)) {
					receiver = 0;
					receiver = fn_receiver_tx();
					if (receiver && !_somaReceiver) {
						BLE_timer_EN = st_timer.BLEenable + TimeCounter;
						_somaReceiver += receiver;
						//st_flag.en_BLE = 0;
					}
					if (receiver == 12) {
						fn_fprint("EXIT BLE CONFIGURATIONS\r\n");
						downlink_data_ok = 4;
						//st_flag.en_BLE = 0;
						break;
					}

				}
			} else {
				fn_fprint("\r\nBLE TimeOut");
				st_flag.en_BLE = 0;
			}

			TimerDelay = 0;
			fn_turn_off_nina();
			break;
		case IBEACON_MODE:
			fn_fprint("BLE IBEACON_MODE\r\n");
			fn_fprint("wBeacon..");
			fn_turn_on_nina();
			fn_fprint(".");
			HAL_Delay(100);
			fn_fprint(".");
			fn_at_command_factory_reset();
			fn_fprint(".");
			HAL_Delay(100);
			fn_fprint(".");
			fn_at_command_reboot_device();
			fn_fprint(".");
			HAL_Delay(200);
			fn_fprint(".");
			if (!fn_at_command_attention())
				fn_at_command_escape_at();
			fn_at_command_set_connectable();
			fn_fprint(".");
			fn_at_command_set_discoverable();
			fn_fprint(".");
			fn_at_command_set_pairable();
			fn_fprint(".");
			fn_at_command_set_perimetral();
			//fn_at_command_set_wbeacon_name();
			fn_fprint(".");
			HAL_Delay(100);
			fn_fprint(".");
			fn_at_command_set_beacon_payload();
			fn_fprint("\r\n");
		case EDDYSTONE_BEACON_MODE:

			LED_CHANGE
			fn_turn_on_nina();
			fn_fprint("BLE EDDYSTONE_BEACON_MODE\r\n");
			if (!fn_at_command_attention())
				fn_at_command_escape_at();
			fn_at_command_set_perimetral();
			fn_at_command_set_wbeacon_name();
			SIGFOX_OFF
			GPRS_OFF
			GPRS_PWR_OFF
			LED_OFF
			GPS_OFF
			while (1) {
				fn_get_volume_value();
				fn_get_temperature_value();
				fn_get_battery_value();
				fn_print_sensor_values();
				fn_at_command_set_eddyston_TLM_payload();
				HAL_Delay(60000);
			}
		case SCAN_BEACON_MODE:
			fn_turn_on_nina();
			fn_fprint("BLE SCAN_BEACON_MODE\r\n");
			fn_fprint("id: \r\n");
			fn_fprint(slave_ID);
			fn_fprint("\r\n");

			fn_at_command_factory_reset();
			HAL_Delay(100);
			fn_at_command_reboot_device();
			HAL_Delay(200);
			if (!fn_at_command_attention())
				fn_at_command_escape_at();
			fn_at_command_set_new_name("wBox");
			HAL_Delay(20);
			fn_at_command_set_connectable();
			HAL_Delay(20);
			fn_at_command_set_discoverable();
			HAL_Delay(20);
			fn_at_command_set_pairable();
			HAL_Delay(20);
			fn_at_command_set_central();
			HAL_Delay(20);
			fn_at_command_storing();
			fn_at_command_reboot_device();
			HAL_Delay(200);
			fn_at_commnad_get_configuration_status();
			fn_at_command_get_serial_number();
			fn_at_command_get_model_id();
			fn_at_command_get_manufacture_id();
			//fn_at_command_storing();
			//fn_at_command_reboot_device();
			if (!st_flag.wBeacon_slave) {
				st_flag.wBeacon_slave = fn_find_id_peer("CCF9");
			}

			fn_at_command_scan_peer();

			fn_turn_off_nina();
			break;

		case SERIAL_BLE_MODE:
			fn_turn_on_nina();
			fn_fprint("BLE SERIAL MODE\r\n");
			while (!fn_at_command_attention())
				fn_at_command_escape_at();
			fn_at_command_set_new_name("WizeBox_Serial");
			fn_at_command_set_connectable();
			fn_at_command_set_discoverable();
			fn_at_command_set_pairable();
			fn_at_command_set_perimetral();
			fn_at_command_storing();
			fn_at_command_reboot_device();
			fn_fprint("WAITING TO CONNECT\r\n");
			fn_at_command_data_mode();
			BLE_timer_EN = TimeCounter + st_timer.BLEenable;

			while (TimeCounter != BLE_timer_EN) {
				receiver = fn_receiver_tx();
				if (receiver == 1) {
					fn_decoder_downlink();
					fn_serial_data_responce();
					break;
				}
				if (receiver == 2) {
					break;
				}
			}
			fn_turn_off_nina();
			break;
		default:
			break;
		}
	}

}

uint8_t fn_ckeck_Nina_status() {
	uint8_t check = 0;
	fn_turn_on_nina();
	LED_CHANGE
	for (int var = 0; var < BYTES_NINA_TO_RECEIVER; ++var) {
		if (aStringToReceiver[var] == 43) {
			check = 1;
			break;
		}
	}
	LED_CHANGE
	if (!check)
		check = fn_at_command_get_serial_number();
	else
		fn_at_command_get_serial_number();
	LED_CHANGE
	fn_at_command_factory_reset();
	LED_CHANGE
	HAL_Delay(500);
	LED_CHANGE
	fn_at_command_reboot_device();
	LED_CHANGE
	HAL_Delay(200);
	LED_CHANGE
	fn_turn_off_nina();
	return check;
}

//-----------------POWER MODE OPERATION-------------------
void fn_turn_on_nina(void) {
	BLE_ON
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
}

void fn_turn_off_nina(void) {
	fn_fprint("nina off\r\n");
	HAL_Delay(1000);
	BLE_OFF

}

void fn_reset_nina(void) {
	fn_turn_off_nina();
	fn_turn_on_nina();
}

//*************************************************************

void fn_transfer_lsm_values_ble() {
	fn_get_lsm303ah();
	char axBFF[10] = { 0 };
	char ayBFF[10] = { 0 };
	char azBFF[10] = { 0 };
	char mxBFF[10] = { 0 };
	char myBFF[10] = { 0 };
	char mzBFF[10] = { 0 };

	memset(lsm_data_transfer, 0, 50);

	struct {
		int16_t a_x, a_y, a_z;
	} st_ac_buffer;

	struct {
		int16_t m_x, m_y, m_z;
	} st_mag_buffer;

	if (st_accelerometer.a_x < 0)
		st_ac_buffer.a_x = (st_accelerometer.a_x * -1);
	if (st_accelerometer.a_y < 0)
		st_ac_buffer.a_y = (st_accelerometer.a_y * -1);
	if (st_accelerometer.a_z < 0)
		st_ac_buffer.a_z = (st_accelerometer.a_z * -1);
	if (st_magnetometer.m_x < 0)
		st_mag_buffer.m_x = (st_magnetometer.m_x * -1);
	if (st_magnetometer.m_y < 0)
		st_mag_buffer.m_y = (st_magnetometer.m_y * -1);
	if (st_magnetometer.m_z < 0)
		st_mag_buffer.m_z = (st_magnetometer.m_z * -1);

	if (st_accelerometer.a_x >= 0)
		st_ac_buffer.a_x = (st_accelerometer.a_x * 1);
	if (st_accelerometer.a_y >= 0)
		st_ac_buffer.a_y = (st_accelerometer.a_y * 1);
	if (st_accelerometer.a_z >= 0)
		st_ac_buffer.a_z = (st_accelerometer.a_z * 1);
	if (st_magnetometer.m_x >= 0)
		st_mag_buffer.m_x = (st_magnetometer.m_x * 1);
	if (st_magnetometer.m_y >= 0)
		st_mag_buffer.m_y = (st_magnetometer.m_y * 1);
	if (st_magnetometer.m_z >= 0)
		st_mag_buffer.m_z = (st_magnetometer.m_z * 1);

	utoa(st_ac_buffer.a_x, axBFF, 10);
	utoa(st_ac_buffer.a_y, ayBFF, 10);
	utoa(st_ac_buffer.a_z, azBFF, 10);
	utoa(st_mag_buffer.m_x, mxBFF, 10);
	utoa(st_mag_buffer.m_y, myBFF, 10);
	utoa(st_mag_buffer.m_z, mzBFF, 10);

	if (st_accelerometer.a_x < 0) {
		strcat(lsm_data_transfer, "ax: -");
	} else {
		strcat(lsm_data_transfer, "ax: ");
	}
	strcat(lsm_data_transfer, axBFF);

	if (st_accelerometer.a_y < 0) {
		strcat(lsm_data_transfer, " ay: -");
	} else {
		strcat(lsm_data_transfer, " ay: ");
	}
	strcat(lsm_data_transfer, ayBFF);

	if (st_accelerometer.a_z < 0) {
		strcat(lsm_data_transfer, " az: -");
	} else {
		strcat(lsm_data_transfer, " az: ");
	}
	strcat(lsm_data_transfer, azBFF);

	if (st_magnetometer.m_x < 0) {
		strcat(lsm_data_transfer, " mx: -");
	} else {
		strcat(lsm_data_transfer, " mx: ");
	}
	strcat(lsm_data_transfer, mxBFF);

	if (st_magnetometer.m_y < 0) {
		strcat(lsm_data_transfer, " my: -");
	} else {
		strcat(lsm_data_transfer, " my: ");
	}
	strcat(lsm_data_transfer, myBFF);

	if (st_magnetometer.m_z < 0) {
		strcat(lsm_data_transfer, " mz: -");
	} else {
		strcat(lsm_data_transfer, " mz: ");
	}
	strcat(lsm_data_transfer, mzBFF);

	strcat(lsm_data_transfer, " \r");

	StartTransfers(lsm_data_transfer);
}

void fn_ninaB1() {
	fn_send_at_command_nina("AT", 1000);
	fn_send_at_command_nina("AT+UBTLE=1", 1000);
	fn_send_at_command_nina("AT&W", 1000);
	fn_send_at_command_nina("AT+UBTLN=\"WIZE TEST CENTRAL BLE\"", 1000);
	fn_send_at_command_nina("AT+UBTD", 5000);
	fn_send_at_command_nina("AT+UDCP=sps://CCF9578580A3p", 2000);
	HAL_Delay(1000);
	fn_send_at_command_nina("ATO1", 1000);
	HAL_Delay(1000);
	fn_send_at_command_nina("teste", 1000);

	BLE_OFF
	LED_OFF
}

uint8_t fn_set_config_central_nina() {
	//fn_set_at_mode_nina();
	//fn_send_at_command_nina("AT", 1000);
	//fn_send_at_command_nina("AT+UBTLE=1", 1000);
	//fn_send_at_command_nina("AT&W", 1000);
	//fn_send_at_command_nina("AT+CPWROFF", 1000);
	HAL_Delay(1000);
	//fn_send_at_command_nina("AT+UDSC=0,6", 1000);
	//HAL_Delay(500);

	return 1;
}

//##############  CONFIG DATA MODE  #####################
uint8_t fn_at_command_escape_at() {
	uint8_t atReturn = 0;
	LED_ON
	HAL_Delay(1500);
	StartTransfers("+++");
	HAL_Delay(1500);
	LED_OFF
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_data_mode() {

	uint8_t atReturn = 0;
	StartTransfers("ATO1\r");
	HAL_Delay(200);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}
//*************************************************************

/*


 int fn_discovery_peer_nina() {

 memset(responce_nina_buff, 0, BYTES_NINA_TO_RECEIVER);

 HAL_UART_Transmit_DMA(&huart4, (uint8_t*) "AT+UBTD\r\n", 9);
 while (UartReady != SET) {
 }
 UartReady = RESET;
 HAL_UART_Receive_DMA(&huart4, (uint8_t*) responce_nina_buff, BYTES_NINA_TO_RECEIVER);
 while (UartReady != SET) {

 }
 UartReady = RESET;

 memset(responce_nina_buff, 0, BYTES_NINA_TO_RECEIVER);
 int return_flag = 0;

 char BLEreceiver[BYTES_NINA_TO_RECEIVER] = { 0 };

 HAL_UART_Transmit_IT(&huart4, (uint8_t*) "AT+UBTD\r\n", 9);
 while (UartReady != SET) {
 }
 UartReady = RESET;

 HAL_Delay(5000);

 HAL_UART_Receive_IT(&huart4, (uint8_t*) &BLEreceiver,
 BYTES_NINA_TO_RECEIVER);
 LED_ON
 HAL_Delay(10);
 LED_OFF
 huart4.pRxBuffPtr = (uint8_t*) &BLEreceiver;
 huart4.RxXferCount = 0;

 int size_response_uart = 0;

 for (int var = 0; var < BYTES_NINA_TO_RECEIVER; var++) {
 if (BLEreceiver[var] != 0 && BLEreceiver[var] < 128) {
 ++size_response_uart;
 }
 }
 //char sendResponse[size_response_uart];
 int counter_size = 0;

 for (int var = 0; var < BYTES_NINA_TO_RECEIVER; var++) {
 if (BLEreceiver[var] != 0 && BLEreceiver[var] < 128) {
 //sendResponse[counter_size] = BLEreceiver[var];
 responce_nina_buff[counter_size] = BLEreceiver[var];
 ++counter_size;
 if (responce_nina_buff[counter_size - 1] == 79
 && responce_nina_buff[counter_size] == 75) {
 return_flag = 1;
 }
 }
 }

 fn_fprint(responce_nina_buff);

 return return_flag;

 }
 */

//##############  AT COMMANDS  #####################
uint8_t fn_at_command_attention() {
	uint8_t atReturn = 0;
	StartTransfers("AT\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_storing() {
	uint8_t atReturn = 0;
	StartTransfers("AT&W\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_factory_reset() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UFACTORY\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_reboot_device() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CPWROFF\r");
	HAL_Delay(2000);
	fn_at_uart_reception_nina();

	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	HAL_Delay(500);
	fn_at_uart_reception_nina();
	return atReturn;
}

uint8_t fn_at_command_set_perimetral() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTLE=2\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_central() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTLE=1\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_get_serial_number() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CGSN\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_get_model_id() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CGMM\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_get_manufacture_id() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CGMI\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_new_name(char*name) {

	int size_command = strlen(name);
	size_command += 10;
	char command[size_command];
	strcpy(command, "AT+UBTLN=");
	strcat(command, name);
	strcat(command, "\r");
	uint8_t atReturn = 0;
	StartTransfers(command);
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_server_config() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UDSC=0,6\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_connectable() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTCM=2\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_discoverable() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTDM=3\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_pairable() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTPM=2\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_wbeacon_name() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTSD=080977426561636F6E\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_eddyston_TLM_payload() {
	uint8_t atReturn = 0;

	char payload_ble_eddystone[44] = { 0 };
	char command_complete_ble_eddystone[54] = { 0 };
	fn_encoder_ble_values(payload_ble_eddystone);
	command_complete_ble_eddystone[0] = 'A';
	command_complete_ble_eddystone[1] = 'T';
	command_complete_ble_eddystone[2] = '+';
	command_complete_ble_eddystone[3] = 'U';
	command_complete_ble_eddystone[4] = 'B';
	command_complete_ble_eddystone[5] = 'T';
	command_complete_ble_eddystone[6] = 'A';
	command_complete_ble_eddystone[7] = 'D';
	command_complete_ble_eddystone[8] = '=';
	//strncpy(command_complete_ble_eddystone, "AT+UBTAD=", 9);
	strncat(command_complete_ble_eddystone, payload_ble_eddystone, 44);
	command_complete_ble_eddystone[53] = 13;

	StartTransfers(command_complete_ble_eddystone);
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_at_command_set_beacon_payload() {
	uint8_t atReturn = 0;
	char dados[54] = { 0 };
	char _payload[24] = { 0 };
	char _seqNumber[3] = { 0 };
	char command_complete_ble_eddystone[64] = { 0 };

	if (st_flag.tipper_detect == 1)
		st_data_sensor_e.header = 1;

	fn_encoder_report_frame(_payload);
	decHexBLE(sequencialMensage, _seqNumber, 2);

	strncpy(dados, "1AFF4C000215", 12); //MODIFICAR AQUI
	strncat(dados, _payload, 24);
	strncat(dados, sara_imei, 15);
	strncat(dados, _seqNumber, 2); //12+15+24+3 =54

	command_complete_ble_eddystone[0] = 'A';
	command_complete_ble_eddystone[1] = 'T';
	command_complete_ble_eddystone[2] = '+';
	command_complete_ble_eddystone[3] = 'U';
	command_complete_ble_eddystone[4] = 'B';
	command_complete_ble_eddystone[5] = 'T';
	command_complete_ble_eddystone[6] = 'A';
	command_complete_ble_eddystone[7] = 'D';
	command_complete_ble_eddystone[8] = '=';

	strncat(command_complete_ble_eddystone, dados, 54);
	command_complete_ble_eddystone[62] = 70;
	command_complete_ble_eddystone[63] = 13;

	StartTransfers(command_complete_ble_eddystone);
	fn_fprint(command_complete_ble_eddystone);

	HAL_Delay(500);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_scan_peer() {
	uint8_t atReturn = 0;
	uint8_t ble_timer_out = 10;
	unsigned int loc_info = 0, loc_id = 0;
	char ble_distance_buffer[2] = { 0 };
	char ble_info_get[80] = { 0 };
	char eddystone[18] = "0303AAFE1116AAFE20";
	ble_distance[0] = 0;
	ble_distance[1] = 0;
	//strcpy(slave_ID, "CCF957881DEEp");
	//while (!strstr((char*) aStringToReceiver, "0303AAFE1116AAFE20")) {
	while (!strstr((char*) aStringToReceiver, slave_ID)) {
		StartTransfers("AT+UBTD=1,1\r");
		HAL_Delay(1000);
		fn_at_uart_reception_nina();
		ble_timer_out--;
		if (ble_timer_out == 0)
			break;
		if (strstr((char*) aStringToReceiver, slave_ID)
				&& strstr((char*) aStringToReceiver, eddystone)) {
			atReturn = 1;
			break;
		}

	}
	if (atReturn) {
		loc_id = find_location_word((char*) aStringToReceiver, slave_ID);

		loc_info = find_location_word((char*) aStringToReceiver, eddystone);
	}

	if (loc_id < loc_info) {
		for (int var = loc_id; var < (loc_id + 80); ++var) {
			ble_info_get[var - loc_id] = aStringToReceiver[var];
		}
		if (strstr((char*) ble_info_get, eddystone)) {
			ble_distance_buffer[0] = aStringToReceiver[loc_info + 16];
			ble_distance_buffer[1] = aStringToReceiver[loc_info + 17];
			atReturn = 1;
		}
		if ((ble_distance_buffer[0] > 47 && ble_distance_buffer[0] < 58)
				|| (ble_distance_buffer[0] > 64 && ble_distance_buffer[0] < 71)) {
			if ((ble_distance_buffer[1] > 47 && ble_distance_buffer[1] < 58)
					|| (ble_distance_buffer[1] > 64
							&& ble_distance_buffer[1] < 71)) {
				ble_distance[0] = ble_distance_buffer[0];
				ble_distance[1] = ble_distance_buffer[1];

				fn_fprint("\r\nble_distance: ");
				fn_fprint(ble_distance_buffer);
				fn_fprint("[hex]\r\n");
			}
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_find_id_peer(char * init_id) {
	fn_fprint("scan...\r\n");

	uint8_t _size = strlen(init_id);
	uint8_t atReturn = 1;
	uint8_t ble_timer_out = 30;
	unsigned int loc_id = 0;
	char slaveID2[13] = { 0 };
	char slaveID1[13] = { 0 };
	uint8_t rss1 = 0, rss2 = 0;

	while (!strstr((char*) aStringToReceiver, init_id)) {//!strstr((char*) aStringToReceiver, eddystone)) {
		StartTransfers("AT+UBTD=1,1\r");
		HAL_Delay(1000);
		fn_at_uart_reception_nina();
		ble_timer_out--;
		if (!ble_timer_out) {
			atReturn = 0;
			break;
		}
	}
	if (atReturn) {
		loc_id = (find_location_word((char*) aStringToReceiver, init_id))
				- _size;

		for (int var = loc_id; var < (loc_id + 13); ++var) {
			slaveID1[var - loc_id] = aStringToReceiver[var];
		}
		rss1 =
				(aStringToReceiver[loc_id + 16] + aStringToReceiver[loc_id + 17]);
		fn_fprint("\r\n ble id peer: ");
		fn_fprint(slaveID1);
		fn_fprint("\r\n");
		fn_fprint("\r\n rssi: ");
		fn_fprintnumber(rss1);
		fn_fprint("\r\n");
	}

	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);

	while (!strstr((char*) aStringToReceiver, init_id)) {//!strstr((char*) aStringToReceiver, eddystone)) {
		StartTransfers("AT+UBTD=1,1\r");
		HAL_Delay(1000);
		fn_at_uart_reception_nina();
		ble_timer_out--;
		if (!ble_timer_out) {
			atReturn = 0;
			break;
		}
	}
	if (atReturn && !find_location_word((char*) aStringToReceiver, slaveID1)) {
		loc_id = (find_location_word((char*) aStringToReceiver, init_id))
				- _size;

		for (int var = loc_id; var < (loc_id + 13); ++var) {
			slaveID2[var - loc_id] = aStringToReceiver[var];
		}
		rss2 =
				(aStringToReceiver[loc_id + 16] + aStringToReceiver[loc_id + 17]);
		fn_fprint("\r\n ble id peer: ");
		fn_fprint(slaveID2);
		fn_fprint("\r\n");
		fn_fprint("\r\n rssi: ");
		fn_fprintnumber(rss2);
		fn_fprint("\r\n");
	}

	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);

	if (rss1 >= rss2) {
		strncpy(slave_ID, slaveID2, 13);
		fn_fprint("\r\n ble id peer: ");
		fn_fprint(slave_ID);
		fn_fprint("\r\n");
	} else {
		strncpy(slave_ID, slaveID1, 13);
		fn_fprint("\r\n ble id peer: ");
		fn_fprint(slave_ID);
		fn_fprint("\r\n");
	}

	return atReturn;
}

uint8_t fn_at_commnad_get_configuration_status() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTLE?\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

//*****************     UART    *********************

void StartTransfers(char* info) {

	memset(aStringToSend, 0, 500);
	ubSizeToSend = strlen(info);
	for (int var = 0; var < ubSizeToSend; ++var) {
		aStringToSend[var] = info[var];
	}
	/* Start transfer only if not already ongoing */
	if (ubSend == 0) {
		/* Start USART transmission : Will initiate TXE interrupt after TDR register is empty */
		LL_USART_TransmitData8(USART4, aStringToSend[ubSend++]);
		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USART4);
	}

}

void USART_CharTransmitComplete_Callback(void) {
	if (ubSend == ubSizeToSend) {
		ubSend = 0;

		/* Disable TC interrupt */
		LL_USART_DisableIT_TC(USART4);

	}
}

void USART_TXEmpty_Callback(void) {
	if (ubSend == (ubSizeToSend - 1)) {
		/* Disable TXE interrupt */
		LL_USART_DisableIT_TXE(USART4);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USART4);
	}

	/* Fill TDR with a new char */
	LL_USART_TransmitData8(USART4, aStringToSend[ubSend++]);
}

void USART_CharReception_Callback(void) {
	//__IO uint32_t received_char;

	/* Read Received character. RXNE flag is cleared by reading of RDR register */
	//received_char = LL_USART_ReceiveData8(USART4);
	aStringToReceiver[ubSizeToReceiver++] = LL_USART_ReceiveData8(
	USART4); //received_char;
	/* Check if received value is corresponding to specific one : \r */
	if ((LL_USART_ReceiveData8(USART4) == 13)) {
		flag_nina = 1;
		/* Turn LED2 On : Expected character has been received */
		/* Echo received character on TX */

		/* Clear Overrun flag, in case characters have already been sent to USART */
		LL_USART_ClearFlag_ORE(USART4);

	}

	//LL_USART_TransmitData8(USARTx_INSTANCE, received_char);
}

uint8_t fn_at_uart_reception_nina() {
	uint8_t ret = 0;
	if (flag_nina) {
		ubSizeToReceiver = 0;
		//HAL_UART_Transmit(&hlpuart1, aStringToReceiver,BYTES_NINA_TO_RECEIVER, 1000);
		flag_nina = 0;
		fn_fprint((char*) aStringToReceiver);
		ret = 1;
	}
	return ret;

}

//++++++++++++++++++++++ PERIMETRAL INFO PROCESS +++++++++++++++++++++

uint8_t fn_wait_for_connection() {
	uint8_t atReturn = 0;
	/*for (int i = 0; i < ubSizeToReceiver; ++i) {
	 if (aStringToReceiver[i - 10] == 43 && aStringToReceiver[i - 9] == 85
	 && aStringToReceiver[i - 8] == 85
	 && aStringToReceiver[i - 7] == 66
	 && aStringToReceiver[i - 6] == 84
	 && aStringToReceiver[i - 5] == 65
	 && aStringToReceiver[i - 4] == 67
	 && aStringToReceiver[i - 3] == 76
	 && aStringToReceiver[i - 2] == 67
	 && aStringToReceiver[i - 1] == 58) {*/
	if (strstr((char*) aStringToReceiver, "UUBTACLC")) {
		atReturn = 1;
		memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
		blink(2);
	}
	fn_at_uart_reception_nina();
	HAL_Delay(100);
	return atReturn;
}

uint8_t fn_receiver_tx() {
	uint8_t atReturn = 0;

	fn_at_uart_reception_nina();
	fn_fprint((char*) aStringToReceiver);

	if (strstr((char*) aStringToReceiver, "AT?")) {
		blink(1);
		StartTransfers("AT OK\r");
		fn_fprint("BLE RECEIVER - AT\r\n");
		HAL_Delay(100);
		atReturn = 1;
	}
	if (strstr((char*) aStringToReceiver, "LC")) {
		char latitude[7] = { 0 }, longitude[7] = { 0 };
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 67) {
				latitude[0] = aStringToReceiver[var];
				latitude[1] = aStringToReceiver[var + 1];
				latitude[2] = aStringToReceiver[var + 3];
				latitude[3] = aStringToReceiver[var + 4];
				latitude[4] = aStringToReceiver[var + 5];
				latitude[5] = aStringToReceiver[var + 6];
				latitude[6] = aStringToReceiver[var + 7];

				longitude[0] = aStringToReceiver[var + 9];
				longitude[1] = aStringToReceiver[var + 10];
				longitude[2] = aStringToReceiver[var + 12];
				longitude[3] = aStringToReceiver[var + 13];
				longitude[4] = aStringToReceiver[var + 14];
				longitude[5] = aStringToReceiver[var + 15];
				longitude[6] = aStringToReceiver[var + 16];
				st_flag.location_CELL = 1;
				break;
			}
		}

		st_data_sensor_e.latitude = atol(latitude);
		st_data_sensor_e.longitude = atol(longitude);
		blink(10);
		StartTransfers("LC OK\r");
		fn_fprint("BLE RECEIVER - LC ");
		fn_fprint((char*) aStringToReceiver);
		fn_fprint("\r\n");

		HAL_Delay(100);
		atReturn = 2;
	}
	if (strstr((char*) aStringToReceiver, "TV")) {
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 'V') {
				st_flag.volume_measurement_type = aStringToReceiver[var] - 48;
				break;
			}
		}
		StartTransfers("TV OK\r");
		HAL_Delay(100);
		atReturn = 3;
		fn_fprint("BLE RECEIVER TV ");
		blink(1);
		fn_fprint((char*) aStringToReceiver);
		fn_fprint("\r\n");
		//memset(aStringToReceiver,0,BYTES_NINA_TO_RECEIVER);
	}

	if (strstr((char*) aStringToReceiver, "AB")) {
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 'B') {
				st_flag.en_tipper = aStringToReceiver[var] - 48;
				break;
			}
		}
		StartTransfers("AB OK\r");
		HAL_Delay(100);
		atReturn = 4;
		fn_fprint("BLE RECEIVER AB ");
		blink(1);
		fn_fprint((char*) aStringToReceiver);
		fn_fprint("\r\n");
	}

	if (strstr((char*) aStringToReceiver, "AG")) {
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 'G') {
				st_flag.en_GPS = aStringToReceiver[var] - 48;
				break;
			}
		}
		StartTransfers("AG OK\r");
		HAL_Delay(100);
		atReturn = 5;
		fn_fprint("BLE RECEIVER AG ");
		blink(1);
		fn_fprint((char*) aStringToReceiver);
		fn_fprint("\r\n");
	}

	if (strstr((char*) aStringToReceiver, "AU?")) {
		//ANGULO DO SU st_data_sensor_e.us_angle
		char numbuff[] = "\0";
		utoa((10 * st_data_sensor_e.us_angle), numbuff, 10);
		char ble_tranfer[10] = "AU=";
		strncat(ble_tranfer, numbuff, 3);
		strncat(ble_tranfer, " OK\r", 4);
		StartTransfers(ble_tranfer);
		HAL_Delay(100);
		atReturn = 6;
		fn_fprint("BLE SEND AU ");
		blink(1);
		fn_fprint((char*) aStringToReceiver);
		fn_fprint("\r\n");
	}
	if (strstr((char*) aStringToReceiver, "PI?")) {
		//ANGULO DO SU st_data_sensor_e.us_angle
		if (!st_data_sensor_e.device_pos)
			StartTransfers("PI=0 OK \r");
		else
			StartTransfers("PI=1 OK \r");

		HAL_Delay(100);
		atReturn = 6;
		blink(1);
		fn_fprint("BLE SEND PI\r\n");
	}
	if (strstr((char*) aStringToReceiver, "CB")) {
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 'B') {
				e_Current_nina_config_type = aStringToReceiver[var] - 48;
				break;
			}
		}
		StartTransfers("CB OK\r");
		HAL_Delay(100);
		blink(1);
		atReturn = 8;
		fn_fprint("BLE RECEIVER CB ");
		fn_fprint((char*) aStringToReceiver);
		fn_fprint("\r\n");
		if (e_Current_nina_config_type == 0) {
			st_flag.en_BLE = 0;
		}
	}
	if (strstr((char*) aStringToReceiver, "TT")) {
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var] == 'T'
					&& aStringToReceiver[var + 1] == 'T') {
				switch (aStringToReceiver[var + 2]) {
				case '0':

					break;
				case '1':
					KeepAlive_Timer = 600;
					break;
				case '2':
					KeepAlive_Timer = 900;
					break;
				case '3':
					KeepAlive_Timer = 1200;
					break;
				case '4':
					KeepAlive_Timer = 1800;
					break;
				case '5':
					KeepAlive_Timer = 3600;
					break;
				case '6':
					KeepAlive_Timer = 7200;
					break;
				case '7':
					KeepAlive_Timer = 14400;
					break;
				case '8':
					KeepAlive_Timer = 21600;
					break;
				case '9':
					KeepAlive_Timer = 28800;
					break;
				case 'a':
				case 'A':
					KeepAlive_Timer = 36000;
					break;
				case 'b':
				case 'B':
					KeepAlive_Timer = 43200;
					break;
				case 'c':
				case 'C':
					KeepAlive_Timer = 86400;
					break;
				case 'd':
				case 'D':

					break;
				case 'e':
				case 'E':

					break;
				case 'f':
				case 'F':

					break;
				default:
					break;

				}

				break;
			}

		}
		StartTransfers("TT OK\r");
		HAL_Delay(100);
		atReturn = 9;
		fn_fprint("BLE RECEIVER TT ");
		fn_fprint((char*) aStringToReceiver);
		blink(1);
		fn_fprint("\r\n");
	}
	if (strstr((char*) aStringToReceiver, "CT")) {
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 'T') {
				//st_flag.en_SiGFOX = aStringToReceiver[var] - 48;
				st_flag.en_HTTP = aStringToReceiver[var + 1] - 48;
				st_flag.en_SMS = aStringToReceiver[var + 2] - 48;
				//st_flag.en_LoRa = aStringToReceiver[var + 3] - 48;
			}
		}
		StartTransfers("CT OK\r");
		HAL_Delay(100);
		atReturn = 10;
		fn_fprint("BLE RECEIVER CT ");
		fn_fprint((char*) aStringToReceiver);
		blink(1);
		fn_fprint("\r\n");
	}
	if (strstr((char*) aStringToReceiver, "HS")) {
		uint16_t timeseconds = 0;
		for (int var = 0; var < 2042; ++var) {
			if (aStringToReceiver[var - 1] == 'S') {
				timeseconds = (aStringToReceiver[var] * 10000)
						+ (aStringToReceiver[var + 1] * 1000)
						+ (aStringToReceiver[var + 2] * 100)
						+ (aStringToReceiver[var + 3] * 10)
						+ aStringToReceiver[var + 4];

				break;
			}
		}
		TimeCounter = timeseconds;
		StartTransfers("\rHS OK\r");
		HAL_Delay(100);
		atReturn = 11;
		char numbuff[] = "\0";
		fn_fprint("\r\nTIMER CONTER: ");
		utoa(timeseconds, numbuff, 10);
		HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff,
				(unsigned) strlen(numbuff), 100);
		HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	}
	if (strstr((char*) aStringToReceiver, "EX")) {
		StartTransfers("EX OK\r");
		HAL_Delay(100);
		atReturn = 12;
		fn_fprint("BLE RECEIVER EX\r\n");
		blink(2);

	}

	/*	} else if (strstr((char*) aStringToReceiver, "EXIT")) {
	 StartTransfers("exit");
	 atReturn = 2;
	 } else if (strstr((char*) aStringToReceiver, "GPS")) {
	 BLE_timer_EN = TimeCounter;
	 if (strstr((char*) aStringToReceiver, "ON")) {
	 st_flag.en_GPS = 1;
	 StartTransfers("gps on");
	 } else if (strstr((char*) aStringToReceiver, "OFF")) {
	 st_flag.en_GPS = 0;
	 StartTransfers("gps off");
	 }
	 } else if (strstr((char*) aStringToReceiver, "LORA")) {
	 BLE_timer_EN = TimeCounter;
	 if (strstr((char*) aStringToReceiver, "ON")) {
	 st_flag.en_LoRa = 1;
	 StartTransfers("lora on");
	 } else if (strstr((char*) aStringToReceiver, "OFF")) {
	 st_flag.en_LoRa = 0;
	 StartTransfers("lora off");
	 }
	 } else if (strstr((char*) aStringToReceiver, "HTTP")) {
	 BLE_timer_EN = TimeCounter;
	 if (strstr((char*) aStringToReceiver, "ON")) {
	 st_flag.en_HTTP = 1;
	 StartTransfers("http on");
	 } else if (strstr((char*) aStringToReceiver, "OFF")) {
	 st_flag.en_HTTP = 0;
	 StartTransfers("http off");
	 }
	 } else if (strstr((char*) aStringToReceiver, "SMS")) {
	 BLE_timer_EN = TimeCounter;
	 if (strstr((char*) aStringToReceiver, "ON")) {
	 st_flag.en_SMS = 1;
	 StartTransfers("sms on");
	 } else if (strstr((char*) aStringToReceiver, "OFF")) {
	 st_flag.en_SMS = 0;
	 StartTransfers("sms off");
	 }
	 } else if (strstr((char*) aStringToReceiver, "BLE")) {
	 BLE_timer_EN = TimeCounter;
	 if (strstr((char*) aStringToReceiver, "ON")) {
	 st_flag.en_BLE = 1;
	 StartTransfers("ble on");
	 } else if (strstr((char*) aStringToReceiver, "OFF")) {
	 st_flag.en_BLE = 0;
	 StartTransfers("ble off");
	 }
	 if (strstr((char*) aStringToReceiver, "SCAN")) {
	 e_Current_nina_config_type = SCAN_BEACON_MODE;
	 StartTransfers("scan ok");
	 }
	 if (strstr((char*) aStringToReceiver, "SERIAL")) {
	 e_Current_nina_config_type = SERIAL_BLE_MODE;
	 StartTransfers("serial ok");
	 }
	 if (strstr((char*) aStringToReceiver, "EDDYSTONE")) {
	 e_Current_nina_config_type = EDDYSTONE_BEACON_MODE;
	 StartTransfers("eddystone ok");
	 }
	 }else if (strstr((char*) aStringToReceiver, "KEEPALIVE")) {
	 BLE_timer_EN = TimeCounter;
	 if (strstr((char*) aStringToReceiver, "900")) {
	 KeepAlive_Timer = 900;
	 StartTransfers("time 900");
	 } else if (strstr((char*) aStringToReceiver, "600")) {
	 KeepAlive_Timer = 600;
	 StartTransfers("time 600");
	 }else if (strstr((char*) aStringToReceiver, "1800")) {
	 KeepAlive_Timer = 1800;
	 StartTransfers("time 1800");
	 }else if (strstr((char*) aStringToReceiver, "3600")) {
	 KeepAlive_Timer = 3600;
	 StartTransfers("time 3600");
	 }else if (strstr((char*) aStringToReceiver, "7200")) {
	 KeepAlive_Timer = 7200;
	 StartTransfers("time 7200");
	 }else if (strstr((char*) aStringToReceiver, "36000")) {
	 KeepAlive_Timer = 36000;
	 StartTransfers("time 36000");
	 }*/

	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	flag_nina = 1;
	HAL_Delay(100);

	return atReturn;
}

uint8_t fn_answer_at_data_mode_nina() {
	uint8_t answer = 0;
	/*if (flag_nina) {
	 strcpy(at_data_receiver_ble_to_nina, (char*) aStringToReceiver);

	 if (strstr((char*) aStringToReceiver, BLE_CFG_PEER) && answer == 0) {

	 HAL_Delay(1000);
	 find_between("AT$DP=", "\r", (char*) aStringToReceiver,
	 peer_adress);
	 if (peer_adress[0] == 0 && peer_adress[1] == 13) {
	 flag_default_peer = RESET;
	 } else {
	 flag_default_peer = SET;
	 }
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 } else if (strstr((char*) aStringToReceiver, BLE_ALL) && answer == 0) {

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, st_sensor_char_values.battery);
	 strcat(at_data_receiver_ble_to_nina, " , ");

	 strcat(at_data_receiver_ble_to_nina,
	 st_sensor_char_values.temperature);
	 strcat(at_data_receiver_ble_to_nina, " , ");

	 strcat(at_data_receiver_ble_to_nina,
	 st_sensor_char_values.distance);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (strstr((char*) aStringToReceiver, BLE_VOLT) && answer == 0) {

	 HAL_Delay(1000);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, st_sensor_char_values.battery);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (strstr((char*) aStringToReceiver, BLE_TEMP) && answer == 0) {

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina,
	 st_sensor_char_values.temperature);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (strstr((char*) aStringToReceiver, BLE_DIST) && answer == 0) {

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina,
	 st_sensor_char_values.distance);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (strstr((char*) aStringToReceiver, BLE_CFG_P)
	 && answer == 0) {

	 HAL_Delay(1000);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
	 answer = 1;

	 } else if (strstr((char*) aStringToReceiver, BLE_CFG_C)
	 && answer == 0) {

	 HAL_Delay(1000);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 fn_Change_Nina_Config_Type(CENTRAL_MODE);
	 answer = 1;

	 } else if (strstr((char*) aStringToReceiver, BLE_CFG_I_BCN)
	 && answer == 0) {

	 HAL_Delay(1000);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 fn_Change_Nina_Config_Type(IBEACON_MODE);
	 answer = 1;

	 } else if (strstr((char*) aStringToReceiver, BLE_CFG_E_BCN)
	 && answer == 0) {

	 HAL_Delay(1000);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 fn_Change_Nina_Config_Type(EDDYSTONE_BEACON_MODE);
	 answer = 1;

	 } else if (strstr((char*) aStringToReceiver, BLE_INFO) && answer == 0) {
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (strstr((char*) aStringToReceiver, BLE_CFG) && answer == 0) {
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (strstr((char*) aStringToReceiver, BLE_RST) && answer == 0) {
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, "RESETING");
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 fn_Change_Nina_Config_Type(RESET_MODE);
	 answer = 1;

	 } else if (strstr((char*) aStringToReceiver, BLE_RST_FACTORY)
	 && answer == 0) {

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, "FACTORY RESET");
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);
	 fn_Change_Nina_Config_Type(FACTORY_MODE);
	 answer = 1;

	 } else if (strstr((char*) aStringToReceiver, BLE_AT) && answer == 0) {
	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_OK);

	 } else if (answer == 0) {

	 strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
	 strcat(at_data_receiver_ble_to_nina, BLE_ERROR);

	 }
	 HAL_Delay(100);
	 StartTransfers(at_data_receiver_ble_to_nina);
	 ubSizeToReceiver = 0;
	 flag_nina = 0;
	 memset(at_data_receiver_ble_to_nina, 0, DATA_SIZE_2);
	 memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	 }*/
	return answer;
}

//++++++++++++++++++++++ CENTRAL INFO PROCESS +++++++++++++++++++++

uint8_t fn_discovery_devices_nina() {
	uint8_t atReturn = 0;

	StartTransfers("AT+UBTD=4,1,2000\r");

	HAL_Delay(3000);
	fn_at_uart_reception_nina();
	HAL_Delay(100);
	//uint8_t counter = countOccurrences((char*) aStringToReceiver, "+UBTD:");
	if (!flag_default_peer) {
		memset(peer_adress, 0, 18);
		if (strstr((char*) aStringToReceiver, "	wBEacon")) {
			HAL_Delay(200);
			find_between("+UBTD:", "wBEacon", (char*) aStringToReceiver,
					peer_adress);
			if (peer_adress[12] == 112) {
				HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\npeer_adress: ", 15,
						100);
				HAL_Delay(200);
				HAL_UART_Transmit(&hlpuart1, (uint8_t*) peer_adress, 13, 500);
				HAL_Delay(200);
				HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 50);
				atReturn = 1;
			}
		}
	} else if (flag_default_peer) {
		if (strstr((char*) aStringToReceiver, peer_adress)) {
			HAL_Delay(200);
			peer_adress[12] = 112;

			HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\npeer_adress: ", 15,
					100);
			HAL_Delay(200);
			HAL_UART_Transmit(&hlpuart1, (uint8_t*) peer_adress, 13, 500);
			HAL_Delay(200);
			HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 50);
			atReturn = 1;

		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);

	return atReturn;
}

uint8_t fn_at_connect_peer() {

	char command[27] = "AT+UDCP=sps://xxxxxxxxxxxx";
	for (int var = 0; var < 13; ++var) {
		command[var + 14] = peer_adress[var];
	}
	command[26] = 13;
	uint8_t atReturn = 0;
	StartTransfers(command);
	HAL_Delay(10000);
	fn_at_uart_reception_nina();
	HAL_Delay(100);
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_disconect_peer() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UDCPC=1\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

//********************** DATA AT COMMANDS ************************
void fn_serial_data_responce(void) {
	//downlink_data_ok = 0;

}

uint8_t fn_data_at_command() {
	uint8_t atReturn = 0;
	StartTransfers("AT\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_data_get_at_temperature() {
	uint8_t atReturn = 0;
	StartTransfers("AT$T?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_data_get_at_battery() {
	uint8_t atReturn = 0;
	StartTransfers("AT$V?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_get_at_distance() {
	uint8_t atReturn = 0;
	StartTransfers("AT$D?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_data_get_at_all_sensors() {
	uint8_t atReturn = 0;
	StartTransfers("AT$A?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_get_at_config() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_central_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=1\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_perimetral_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=2\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_ibeacon_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=3\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_eddystone_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=4\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

void fn_send_serial_ble(char* data) {
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	StartTransfers(data);
	HAL_Delay(50);
}
