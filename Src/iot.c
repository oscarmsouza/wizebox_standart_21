/*
 * sigfox.c
 *
 *  Created on: 9 de mar de 2020
 *      Author: oscar
 */
#include <iot.h>
#include "stm32l0xx_it.h"

//********************* WISOL AT COMMAND ********************************

uint8_t fn_at_command_sigfox(char * at_command) {
	uint8_t atReturn = 0;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	int size_at_command = strlen(at_command);
	size_at_command += 2;
	char new_command[size_at_command];
	strcpy(new_command, at_command);
	strcat(new_command, "\r\n");

	Start_SIGFOX_Transfers(new_command);
	HAL_Delay(100);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var - 1] == 79
				&& aStringToReceiver_SIGFOX[var] == 75) {
			atReturn = 1;
		}
	}

	return atReturn;
}

//********************* GET WISOL INFO ********************************
uint8_t fn_check_sigfox_status() {
	uint8_t check = 0;

	SIGFOX_ON
	HAL_Delay(200);
	check = fn_at_sigfox();
	HAL_Delay(200);
	SIGFOX_OFF

	return check;

}

void fn_info_sigfox() {
	SIGFOX_ON
	uint8_t check = 0, time = 2;
	HAL_Delay(200);
	check = fn_at_sigfox();
	while (!check && time != 0) {
		HAL_Delay(100);
		time--;
		check = fn_at_sigfox();
	}
	fn_fprint("SIGFOX ID: ");
	if (!fn_get_id_sigfox()) {
		fn_at_sigfox();
		fn_get_id_sigfox();
	}
	HAL_Delay(100);
	fn_fprint("SIGFOX PAC: ");
	if (!fn_get_pac_sigfox()) {
		fn_at_sigfox();
		fn_get_pac_sigfox();
	}
	HAL_Delay(100);
	fn_fprint("SIGFOX TEMPERATURE: ");
	fn_get_temperature_sigfox();
	HAL_Delay(100);
	fn_fprint("SIGFOX VOLTS: ");
	fn_get_volt_sigfox();
	HAL_Delay(100);
	SIGFOX_OFF
}

uint8_t fn_get_id_sigfox() {
	uint8_t responce = 0;
	//char command[9] = "AT$I=10\r\n";

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$I=10\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.id[r_p] = aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_get_pac_sigfox() {
	uint8_t responce = 0;
	//char command[9] = "AT$I=11\r\n";

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$I=11\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;
	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.pac[r_p] = aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}

	return responce;
}

uint8_t fn_start_iot() {
	SIGFOX_ON
	LED_ON
	HAL_Delay(200);

	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	//char command[4] = "AT\r";
	//fn_fprint("\r\nAT ");
	Start_SIGFOX_Transfers("AT\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();


	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] == 'P'
				&& aStringToReceiver_SIGFOX[var + 1] == 'a'
				&& aStringToReceiver_SIGFOX[var + 2] == 'y'
				&& aStringToReceiver_SIGFOX[var + 3] == 'l'
				&& aStringToReceiver_SIGFOX[var + 4] == 'o'
				&& aStringToReceiver_SIGFOX[var + 5] == 'a'
				&& aStringToReceiver_SIGFOX[var + 6] == 'd'
				&& aStringToReceiver_SIGFOX[var + 7] == 'I'
				&& aStringToReceiver_SIGFOX[var + 8] == 'n'
				&& aStringToReceiver_SIGFOX[var + 9] == '='
				&& aStringToReceiver_SIGFOX[var + 10] == 'A'
				&& aStringToReceiver_SIGFOX[var + 11] == 'T')
				|| (aStringToReceiver_SIGFOX[var] == 67
						&& aStringToReceiver_SIGFOX[var + 1] == 77
						&& aStringToReceiver_SIGFOX[var + 2] == 87
						&& aStringToReceiver_SIGFOX[var + 3] == 88
						&& aStringToReceiver_SIGFOX[var + 4] == 49)) {
			responce = 1;

			break;
		} else if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			st_sigfox_parameters.at[0] = aStringToReceiver_SIGFOX[var];
			st_sigfox_parameters.at[1] = aStringToReceiver_SIGFOX[var + 1];

			responce = 2;
		}
	}
	return responce;
	HAL_Delay(200);

}

uint8_t fn_at_sigfox() {
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	//char command[4] = "AT\r";
	fn_fprint("\r\nAT ");
	Start_SIGFOX_Transfers("AT\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_get_volt_sigfox() {
	uint8_t responce = 0;
	//char command[7] = "AT$V?\r\n";
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$V?\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.volts[r_p] = aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}

	int_volt_sigfox = atoi(st_sigfox_parameters.volts);
	int_volt_sigfox /= 10;
	return responce;
}

uint8_t fn_get_temperature_sigfox() {
	uint8_t responce = 0;
	//char command[8] = "AT$T?\r\n";
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$T?\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.temperature[r_p] =
					aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}

	int_temp_sigfox = atoi(st_sigfox_parameters.temperature);
	int_temp_sigfox /= 10;
	if (st_sigfox_parameters.temperature)
		responce = 1;
	return responce;

}

void fn_status_sigfox() {

}

void fn_reset_sigfox() {
	SIGFOX_RESET_ON
	HAL_Delay(50);
	SIGFOX_RESET_OFF
	//HAL_GPIO_DeInit(EN_SFOX_GPIO_Port, EN_SFOX_Pin);
	//fn_fprint("\r\nsigfox reset\r\n");
}

//******************** SEND SIGFOX PAYLOADS **************************

void fn_send_report_frame_sigfox() {
	fn_fprint("\r\nSIGFOX REPORT FRAME\r\n");
	uint8_t check = 0, time = 10;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}
	fn_send_config_sigfox();
	memset(downlink_data, 0, 16);
	char sigfox_complete_report_frame[32] = { 0 };
	char sigfox_frame_report_values[24] = { 0 };

	fn_encoder_report_frame(sigfox_frame_report_values);

	sigfox_complete_report_frame[0] = 65;
	sigfox_complete_report_frame[1] = 84;
	sigfox_complete_report_frame[2] = 36;
	sigfox_complete_report_frame[3] = 83;
	sigfox_complete_report_frame[4] = 70;
	sigfox_complete_report_frame[5] = 61;
	for (int var = 0; var < 24; ++var) {
		sigfox_complete_report_frame[var + 6] = sigfox_frame_report_values[var];
	}
	sigfox_complete_report_frame[30] = 13;
	sigfox_complete_report_frame[31] = 10;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(sigfox_complete_report_frame);
	HAL_Delay(500);
	fn_at_uart_reception_sigfox();
	fn_fprint("frame report:  ");
	fn_fprint(sigfox_frame_report_values);
	fn_fprint("\r\n");
	HAL_Delay(1000);
	SIGFOX_OFF
}

void fn_send_start_frame_sigfox() {
	uint8_t check = 0, time = 10;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}
	/*	fn_get_id_sigfox();
	 fn_get_pac_sigfox();
	 fn_get_temperature_sigfox();
	 fn_get_volt_sigfox();*/
	fn_send_config_sigfox();
	char start_machine_frame[30] = { 0 };
	char buffer_start[7] = { 0 };
	int temp_buff = (int_temp_sigfox);
	int volt_buff = (int_volt_sigfox - 120);

	start_machine_frame[0] = 65;
	start_machine_frame[1] = 84;
	start_machine_frame[2] = 36;
	start_machine_frame[3] = 83;
	start_machine_frame[4] = 70;
	start_machine_frame[5] = 61;

	start_machine_frame[6] = 48;
	start_machine_frame[7] = 50;

	decHex(volt_buff, buffer_start);
	check_size_info(2, buffer_start);
	strcat(start_machine_frame, buffer_start);

	decHex(temp_buff, buffer_start);
	check_size_info(2, buffer_start);
	strcat(start_machine_frame, buffer_start);

	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");

	start_machine_frame[28] = 13;
	start_machine_frame[29] = 10;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(start_machine_frame);
	HAL_Delay(500);
	fn_at_uart_reception_sigfox();

	fn_fprint("frame start:  ");
	fn_fprint(start_machine_frame);
	fn_fprint("\r\n");
	HAL_Delay(1000);
	SIGFOX_OFF
}

uint8_t fn_send_daily_frame_sigfox() {

	fn_fprint("\r\nSIGFOX DAILY FRAME\r\n");
	uint8_t check = 0, time = 40;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!fn_at_sigfox() && time != 0) {
		HAL_Delay(1000);
		time--;

	}
	fn_send_config_sigfox();
	memset(downlink_data, 0, 16);
	char sigfox_complete_report_frame[34] = { 0 };
	char sigfox_frame_report_values[24] = { 0 };
	char sigfox_downlink_frame_complete[30] = { 0 };

	fn_encoder_report_frame(sigfox_frame_report_values);

	sigfox_complete_report_frame[0] = 65;
	sigfox_complete_report_frame[1] = 84;
	sigfox_complete_report_frame[2] = 36;
	sigfox_complete_report_frame[3] = 83;
	sigfox_complete_report_frame[4] = 70;
	sigfox_complete_report_frame[5] = 61;
	for (int var = 0; var < 24; ++var) {
		sigfox_complete_report_frame[var + 6] = sigfox_frame_report_values[var];
	}
	sigfox_complete_report_frame[30] = 44;
	sigfox_complete_report_frame[31] = 49;
	sigfox_complete_report_frame[32] = 13;
	sigfox_complete_report_frame[33] = 10;

	for (int var = 0; var < 3; ++var) {

		memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

		Start_SIGFOX_Transfers(sigfox_complete_report_frame);
		fn_fprint("frame report:  ");
		fn_fprint(sigfox_complete_report_frame);
		time = 40;
		while (fn_at_uart_reception_sigfox() == 0 && time != 0) {
			HAL_Delay(1000);
			time--;
		}

		char * pch;
		char buffer[19] = { 0 };

		if (strstr((char*) aStringToReceiver_SIGFOX, "RX=")) {

			strcpy(sigfox_downlink_frame_complete,
					(char*) aStringToReceiver_SIGFOX);
			RemoveSpaces(sigfox_downlink_frame_complete);

			pch = strstr(sigfox_downlink_frame_complete, "RX=");
			strncpy(buffer, pch, 19);
			check = 1;
			var = 3;
			for (int c = 0; c < 19; c++) {
				if (c < 16)
					buffer[c] = buffer[c + 3];
				else
					buffer[c] = 0;

			}

			strncpy(downlink_data, buffer, 16);

			fn_fprint("\r\ndownlink data= ");
			fn_fprint(downlink_data);
			fn_fprint("\r\n");
		}
		HAL_Delay(1000);
		memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	}
	SIGFOX_OFF
	return check;
}

//********************* SEND WISOL CONFIGURATIONS ********************************

uint8_t fn_send_config_sigfox() {
	uint8_t responce = 1;

	/*char ok[4];
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$P=0\r\n", 8, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$DR=905200000\r\n", 17, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$IF=902200000\r\n", 17, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$WR\r\n", 7, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$RC\r\n", 7, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);

	 #ifdef DONGLE_KEY
	 //public key or private key
	 HAL_UART_Transmit(&huart5,(uint8_t*)"ATS410=1\r\n",11,100);//ATS410=1 private key ; ATS410=0 public key
	 //HAL_UART_Receive(&huart5,(uint8_t*)ok,4,10);
	 HAL_Delay(500);
	 #endif
	 */
	/*
	 #ifdef DONGLE_KEY
	 //public key or private key
	 HAL_UART_Transmit(&huart5, (uint8_t*) "ATS410=1\r\n", 11, 100);//ATS410=1 private key ; ATS410=0 public key
	 //HAL_UART_Receive_IT(&huart5,(uint8_t*)ok,4,10);
	 HAL_Delay(500);
	 #endif
	 */
	fn_set_reset_module_sigfox();
	fn_set_RX_frequency_sigfox();
	fn_set_TX_frequency_sigfox();
	fn_set_save_configuration_sigfox();
	fn_set_reset_FCC_sigfox();
	fn_set_dongle_module_sigfox();
	return responce;
}

uint8_t fn_set_dongle_module_sigfox() {

	uint8_t responce = 0;

#ifdef DONGLE_KEY
	fn_fprint("dongle ON -  ");
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("ATS410=1\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
#else
	fn_fprint("dongle OFF - ");
	Start_SIGFOX_Transfers("ATS410=0\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();
#endif
	return responce;
}

uint8_t fn_set_reset_module_sigfox() {
	fn_fprint("reset module sigfox - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$P=0\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_RX_frequency_sigfox() {
	fn_fprint("set RX frequence - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$DR=905200000\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_TX_frequency_sigfox() {
	fn_fprint("set TX frequence - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$IF=902200000\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_save_configuration_sigfox() {
	fn_fprint("save config - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$WR\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_reset_FCC_sigfox() {
	fn_fprint("reset FCC - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$RC\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

//*****************     UART    *********************

void Start_SIGFOX_Transfers(char* sfox_info) {

	memset(aStringToSend_SIGFOX, 0, 100);
	ubSizeToSend_SIGFOX = strlen(sfox_info);
	for (int var = 0; var < ubSizeToSend_SIGFOX; ++var) {
		aStringToSend_SIGFOX[var] = sfox_info[var];
	}
	/* Start transfer only if not already ongoing */
	if (ubSend_SIGFOX == 0) {
		/* Start USART transmission : Will initiate TXE interrupt after TDR register is empty */
		LL_USART_TransmitData8(USART1, aStringToSend_SIGFOX[ubSend_SIGFOX++]);
		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USART1);
	}

}

void USART1_CharTransmitComplete_Callback(void) {
	if (ubSend_SIGFOX == ubSizeToSend_SIGFOX) {
		ubSend_SIGFOX = 0;

		/* Disable TC interrupt */
		LL_USART_DisableIT_TC(USART1);

	}
}

void USART1_TXEmpty_Callback(void) {
	if (ubSend_SIGFOX == (ubSizeToSend_SIGFOX - 1)) {
		/* Disable TXE interrupt */
		LL_USART_DisableIT_TXE(USART1);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USART1);
	}

	/* Fill TDR with a new char */
	LL_USART_TransmitData8(USART1, aStringToSend_SIGFOX[ubSend_SIGFOX++]);
}

void USART1_CharReception_Callback(void) {
	//__IO uint32_t received_char;

	/* Read Received character. RXNE flag is cleared by reading of RDR register */
	//received_char = LL_USART_ReceiveData8(USART4);
	aStringToReceiver_SIGFOX[ubSizeToReceiver_SIGFOX++] = LL_USART_ReceiveData8(
	USART1);	//received_char;
	/* Check if received value is corresponding to specific one : \r */
	if ((LL_USART_ReceiveData8(USART1) == 13)) {
		flag_sigfox = 1;
		/* Turn LED2 On : Expected character has been received */
		/* Echo received character on TX */

		/* Clear Overrun flag, in case characters have already been sent to USART */
		LL_USART_ClearFlag_ORE(USART1);

	}

	//LL_USART_TransmitData8(USARTx_INSTANCE, received_char);
}

uint8_t fn_at_uart_reception_sigfox() {

	uint8_t ret = 0;
	if (flag_sigfox) {
		ubSizeToReceiver_SIGFOX = 0;
		flag_sigfox = 0;
		//fn_fprint((char*) aStringToReceiver_SIGFOX);
		ret = 1;
	}
	return ret;

}

//********************************************************************************
//******************** SEND LORA PAYLOADS **************************

uint8_t fn_send_frame_lora() {
	fn_fprint("\r\n LORA FRAME\r\n");

	SIGFOX_ON
	HAL_Delay(500);
	uint8_t check = 0, time = 10;

	HAL_Delay(100);
	while (!fn_at_sigfox() && time != 0) {
		HAL_Delay(1000);
		time--;
	}

	memset(downlink_data, 0, 16);
	char sigfox_complete_report_frame[32] = { 0 };
	char sigfox_frame_report_values[24] = { 0 };

	fn_encoder_report_frame(sigfox_frame_report_values);

	sigfox_complete_report_frame[0] = 65;
	sigfox_complete_report_frame[1] = 84;
	sigfox_complete_report_frame[2] = 36;
	sigfox_complete_report_frame[3] = 83;
	sigfox_complete_report_frame[4] = 70;
	sigfox_complete_report_frame[5] = 61;
	for (int var = 0; var < 24; ++var) {
		sigfox_complete_report_frame[var + 6] = sigfox_frame_report_values[var];
	}
	sigfox_complete_report_frame[30] = 13;
	sigfox_complete_report_frame[31] = 10;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(sigfox_complete_report_frame);
	if(st_data_sensor_e.header ==2)
		HAL_Delay(35000);
	fn_at_uart_reception_sigfox();

	char * pch;
	char buffer[19] = { 0 };
	//char sigfox_downlink_frame_complete[28] = { 0 };

	if (strstr((char*) aStringToReceiver_SIGFOX, "RX=")) {

		pch = strstr((char*)aStringToReceiver_SIGFOX, "RX=");
		strncpy(buffer, pch, 19);
		check = 1;

		for (int c = 0; c < 19; c++) {
			if (c < 16)
				buffer[c] = buffer[c + 3];
			else
				buffer[c] = 0;

		}

		strncpy(downlink_data, buffer, 16);

		fn_fprint("\r\ndownlink data= ");
		fn_fprint(downlink_data);
		fn_fprint("\r\n");
	}

	SIGFOX_OFF

	return check;

}
