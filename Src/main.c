/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 * @by oscar machado de souza
 * @init - 09/03/2020
 * @project - wizebox
 * @company - Wize Company LTDA
 * @Hardware version - 3.1 (by Bruno Fernandes)
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
__IO ITStatus UartReady; // = RESET;
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* Definition of ADCx conversions data table size */
/* Size of array set to ADC sequencer number of ranks converted,            */
/* to have a rank in each array address.*/

//#define TEST
//#define greenPCB
//#define SLAVE_PROGRAM
//#define MASTER_PROGRAM
#define ADC_CONVERTED_DATA_BUFFER_SIZE   ((uint32_t)   4)
/* Definitions of environment analog values */
/* Value of analog reference voltage (Vref+), connected to analog voltage   */
/* supply Vdda (unit: mV).                                                  */
#define VDDA_APPLI                       ((uint32_t)3300)
#define ADC_CALIBRATION_TIMEOUT_MS       ((uint32_t)   1)
#define ADC_ENABLE_TIMEOUT_MS            ((uint32_t)   1)
#define ADC_DISABLE_TIMEOUT_MS           ((uint32_t)   1)
#define ADC_STOP_CONVERSION_TIMEOUT_MS   ((uint32_t)   1)
#define ADC_CONVERSION_TIMEOUT_MS        ((uint32_t) 300)

/* Delay between ADC end of calibration and ADC enable.                     */
/* Delay estimation in CPU cycles: Case of ADC enable done                  */
/* immediately after ADC calibration, ADC clock setting slow                */
/* (LL_ADC_CLOCK_ASYNC_DIV32). Use a higher delay if ratio                  */
/* (CPU clock / ADC clock) is above 32.                                     */
#define ADC_DELAY_CALIB_ENABLE_CPU_CYCLES  (LL_ADC_DELAY_CALIB_ENABLE_ADC_CYCLES * 32)

/* Prescaler declaration */
uint32_t uwPrescalerValue = 0;

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

I2C_HandleTypeDef hi2c1;

LPTIM_HandleTypeDef hlptim1;

UART_HandleTypeDef hlpuart1;
UART_HandleTypeDef huart2;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim2;

WWDG_HandleTypeDef hwwdg;

/* USER CODE BEGIN PV */
/* Variables for ADC conversion data */
__IO uint16_t aADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE]; /* ADC group regular conversion data */

/* Variable to report status of DMA transfer of ADC group regular conversions */
/*  0: DMA transfer is not completed                                          */
/*  1: DMA transfer is completed                                              */
/*  2: DMA transfer has not been started yet (initial state)                  */
__IO uint8_t ubDmaTransferStatus = 2; /* Variable set into DMA interruption callback */

/* Variable to report status of ADC group regular sequence conversions:       */
/*  0: ADC group regular sequence conversions are not completed               */
/*  1: ADC group regular sequence conversions are completed                   */
__IO uint8_t ubAdcGrpRegularSequenceConvStatus = 0; /* Variable set into ADC interruption callback */

/* Variable to report number of ADC group regular sequence completed          */
static uint32_t ubAdcGrpRegularSequenceConvCount = 0; /* Variable set into ADC interruption callback */

/* Variables for ADC conversion data computation to physical values */
__IO uint16_t uhADCxConvertedData_VoltageGPIO_mVolt = 0; /* Value of voltage on GPIO pin (on which is mapped ADC channel) calculated from ADC conversion data (unit: mV) */
__IO uint16_t uhADCxConvertedData_VrefInt_mVolt = 0; /* Value of internal voltage reference VrefInt calculated from ADC conversion data (unit: mV) */
__IO int16_t hADCxConvertedData_Temperature_DegreeCelsius = 0; /* Value of temperature calculated from ADC conversion data (unit: degree Celcius) */
__IO uint16_t uhADCxConvertedData_VrefAnalog_mVolt = 0; /* Value of analog reference voltage (Vref+), connected to analog voltage supply Vdda, calculated from ADC conversion data (unit: mV) */

uint32_t timer = 0;

uint16_t _pulseTimer, _pulseCounter;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_I2C1_Init(void);
static void MX_LPUART1_UART_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART4_UART_Init(void);
static void MX_USART5_UART_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM2_Init(void);
static void MX_LPTIM1_Init(void);
static void MX_WWDG_Init(void);
/* USER CODE BEGIN PFP */
float get_temp(uint32_t variable);

//void _stm32l_disableGpios( void );
static void MX_USART1_UART_DeInit(void);
static void MX_USART2_UART_DeInit(void);
static void MX_USART4_UART_DeInit(void);
static void MX_USART5_UART_DeInit(void);
//void stm32l_DeInit_Peripherals(void);

uint8_t fn_get_installation_position(void);
void LedUserInstallationInterface(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void inicial_parameters(void) {
	e_Current_nina_config_type = CONFIG_MODE;

	KeepAlive_Timer = 3600;

	st_timer.BLEenable = 100;
	st_timer.GPRSenable = 200;
	st_timer.GPSenable = 300;
	st_timer.TRIPPERenable = 0;

	TimeCounter = 0;
	TimerDelay = 0;

	st_flag.GPRS = 1;
	st_flag.erro = 0;
	st_flag.en_BLE = 1;
	st_flag.en_GPS = 1;
	st_flag.new_downlink = 0;
	st_flag.en_tipper = 1;
	st_flag.tipper_detect = 0;
	st_flag.motion_detect = 0;
	st_flag.wBeacon_slave = 0;

}

void check_position_device_install(void) {
	fn_fprint("**DEVICE POSITION** \r\n");
	st_data_sensor_previwes_e.device_pos = st_data_sensor_e.device_pos;
	st_data_sensor_e.device_pos = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_15);

	if (st_data_sensor_e.device_pos)
		fn_fprint("INSTALATION HIGH - HORIZONTAL\r\n");
	else
		fn_fprint("INSTALATION LOW - VERTICAL\r\n");
}

void check_position_us_install(void) {

	fn_fprint(" ULTRASSON POSITION INSTALL\r\n");
	uint16_t nCount = 5;
	msCount = 0;

	while ((msCount < 300000) && (nCount)) {
		st_data_sensor_previwes_e.us_angle = st_data_sensor_e.us_angle;
		fn_get_us_angle_value();
		if (st_data_sensor_e.us_angle == st_data_sensor_previwes_e.us_angle) {
			nCount--;
			HAL_Delay(100);
			fn_fprint("\r\n");
			fn_fprintnumber(nCount);
			fn_fprint(" NEW POSITION DETECT: ");
			fn_fprintnumber(st_data_sensor_e.us_angle);
			fn_fprint(" OLD POSITION DETECT: ");
			fn_fprintnumber(st_data_sensor_previwes_e.us_angle);
			fn_fprint(" - SAME\r\n");
		} else {
			fn_fprint("\r\n");
			fn_fprint(" NEW POSITION DETECT: ");
			fn_fprintnumber(st_data_sensor_e.us_angle);
			fn_fprint(" OLD POSITION DETECT: ");
			fn_fprintnumber(st_data_sensor_previwes_e.us_angle);
			fn_fprint(" - DIFFERENT\r\n");
			nCount = 5; // reinicia a contagem
			fn_fprint(" Contagem reiniciada \r\n");
			if (msCount > 270000) {
				msCount += 30000; // n�o d� mais tempo de coletar 30 medidas de �ngulos
			}
		}
	}

	if ((msCount > 300000)) {
		// timeout. Reinicia o dispositivo porque n�o reconheceu a posi��o
		fn_fprint("POSITION ERROR. REBOOT...\r\n");
		blinkfast(50);
		HAL_WWDG_Init(&hwwdg);
	} else {
		fn_fprint("ANGLE ULTRASSONIC SENSOR: ");
		fn_fprintnumber(st_data_sensor_e.us_angle);
	}

}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */

int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */


	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	MX_I2C1_Init();
	MX_LPUART1_UART_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
	MX_USART4_UART_Init();
	MX_USART5_UART_Init();
	MX_RTC_Init();
	MX_TIM2_Init();

	MX_LPTIM1_Init();
	MX_WWDG_Init();
	/* USER CODE BEGIN 2 */

	fn_fprint("\r\nSTART PROGRAM - V 24.02.21.01\r\n");

	strcpy(FIRMWARE_VERSION, "24022101");
	LED_ON
	fn_fprint("\r\n*****START INIT****\r\n");
	inicial_parameters();
	fn_check_communication_status_program();
	fn_check_sensors_status_program();
	check_position_device_install();
	//check_position_us_install();
	fn_fprint("\r\n\r\n\r\n****END INIT****\r\n");

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
		fn_main_nina();
		fn_get_sensors_values();
		fn_print_sensor_values();
		fn_send_payload();

		LPM_Enter_STOP_Mode();
		LPM_Exit_STOP_Mode();

	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI
			| RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1
			| RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_LPUART1 | RCC_PERIPHCLK_I2C1
			| RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_LPTIM1;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_HSI;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_HSI;
	PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_HSI;
	PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_HSI;

	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief ADC Initialization Function
 * @param None
 * @retval None
 */
static void MX_ADC_Init(void) {

	/* USER CODE BEGIN ADC_Init 0 */

	/* USER CODE END ADC_Init 0 */

	LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = { 0 };
	LL_ADC_InitTypeDef ADC_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
	/**ADC GPIO Configuration
	 PA4   ------> ADC_IN4
	 PA7   ------> ADC_IN7
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* ADC DMA Init */

	/* ADC Init */
	LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_1, LL_DMA_REQUEST_0);

	LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

	LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_PRIORITY_HIGH);

	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_CIRCULAR);

	LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PERIPH_NOINCREMENT);

	LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MEMORY_INCREMENT);

	LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PDATAALIGN_HALFWORD);

	LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MDATAALIGN_HALFWORD);

	/* ADC interrupt Init */
	NVIC_SetPriority(ADC1_COMP_IRQn, 0);
	NVIC_EnableIRQ(ADC1_COMP_IRQn);

	/* USER CODE BEGIN ADC_Init 1 */
	/*## Configuration of NVIC #################################################*/
	/* Configure NVIC to enable DMA interruptions */
	NVIC_SetPriority(DMA1_Channel1_IRQn, 1); /* DMA IRQ lower priority than ADC IRQ */
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);

	/*## Configuration of DMA ##################################################*/
	/* Enable the peripheral clock of DMA */
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

	/* Configure the DMA transfer */
	/*  - DMA transfer in circular mode to match with ADC configuration:        */
	/*    DMA unlimited requests.                                               */
	/*  - DMA transfer from ADC without address increment.                      */
	/*  - DMA transfer to memory with address increment.                        */
	/*  - DMA transfer from ADC by half-word to match with ADC configuration:   */
	/*    ADC resolution 12 bits.                                               */
	/*  - DMA transfer to memory by half-word to match with ADC conversion data */
	/*    buffer variable type: half-word.                                      */
	LL_DMA_ConfigTransfer(DMA1,
	LL_DMA_CHANNEL_1,
	LL_DMA_DIRECTION_PERIPH_TO_MEMORY |
	LL_DMA_MODE_CIRCULAR |
	LL_DMA_PERIPH_NOINCREMENT |
	LL_DMA_MEMORY_INCREMENT |
	LL_DMA_PDATAALIGN_HALFWORD |
	LL_DMA_MDATAALIGN_HALFWORD |
	LL_DMA_PRIORITY_HIGH);

	/* Select ADC as DMA transfer request */
	LL_DMA_SetPeriphRequest(DMA1,
	LL_DMA_CHANNEL_1,
	LL_DMA_REQUEST_0);

	/* Set DMA transfer addresses of source and destination */
	LL_DMA_ConfigAddresses(DMA1,
	LL_DMA_CHANNEL_1, LL_ADC_DMA_GetRegAddr(ADC1, LL_ADC_DMA_REG_REGULAR_DATA),
			(uint32_t) &aADCxConvertedData,
			LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

	/* Set DMA transfer size */
	LL_DMA_SetDataLength(DMA1,
	LL_DMA_CHANNEL_1,
	ADC_CONVERTED_DATA_BUFFER_SIZE);

	/* Enable DMA transfer interruption: transfer complete */
	LL_DMA_EnableIT_TC(DMA1,
	LL_DMA_CHANNEL_1);

	/* Enable DMA transfer interruption: transfer error */
	LL_DMA_EnableIT_TE(DMA1,
	LL_DMA_CHANNEL_1);

	/*## Activation of DMA #####################################################*/
	/* Enable the DMA transfer */
	LL_DMA_EnableChannel(DMA1,
	LL_DMA_CHANNEL_1);
	/* USER CODE END ADC_Init 1 */
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_4);
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_7);
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_TEMPSENSOR);
	LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
	LL_ADC_PATH_INTERNAL_TEMPSENSOR);
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_VREFINT);
	LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
	LL_ADC_PATH_INTERNAL_VREFINT);
	/** Common config
	 */
	ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_SOFTWARE;
	ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
	ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
	ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_LIMITED;
	ADC_REG_InitStruct.Overrun = LL_ADC_REG_OVR_DATA_OVERWRITTEN;
	LL_ADC_REG_Init(ADC1, &ADC_REG_InitStruct);
	LL_ADC_SetSamplingTimeCommonChannels(ADC1, LL_ADC_SAMPLINGTIME_160CYCLES_5);
	LL_ADC_SetOverSamplingScope(ADC1, LL_ADC_OVS_DISABLE);
	LL_ADC_REG_SetSequencerScanDirection(ADC1, LL_ADC_REG_SEQ_SCAN_DIR_FORWARD);
	LL_ADC_SetCommonFrequencyMode(__LL_ADC_COMMON_INSTANCE(ADC1),
	LL_ADC_CLOCK_FREQ_MODE_HIGH);
	LL_ADC_DisableIT_EOC(ADC1);
	LL_ADC_DisableIT_EOS(ADC1);

	/* Enable ADC internal voltage regulator */
	LL_ADC_EnableInternalRegulator(ADC1);
	/* Delay for ADC internal voltage regulator stabilization. */
	/* Compute number of CPU cycles to wait for, from delay in us. */
	/* Note: Variable divided by 2 to compensate partially */
	/* CPU processing cycles (depends on compilation optimization). */
	/* Note: If system core clock frequency is below 200kHz, wait time */
	/* is only a few CPU processing cycles. */
	uint32_t wait_loop_index;
	wait_loop_index = ((LL_ADC_DELAY_INTERNAL_REGUL_STAB_US
			* (SystemCoreClock / (100000 * 2))) / 10);
	while (wait_loop_index != 0) {
		wait_loop_index--;
	}
	ADC_InitStruct.Clock = LL_ADC_CLOCK_SYNC_PCLK_DIV2;
	ADC_InitStruct.Resolution = LL_ADC_RESOLUTION_12B;
	ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
	ADC_InitStruct.LowPowerMode = LL_ADC_LP_MODE_NONE;
	LL_ADC_Init(ADC1, &ADC_InitStruct);
	/* USER CODE BEGIN ADC_Init 2 */

	/*## Configuration of GPIO used by ADC channels ############################*/

	/* Note: On this STM32 device, ADC1 channel 4 is mapped on GPIO pin PA.04 */

	/* Enable GPIO Clock */
	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);

	/* Configure GPIO in analog mode to be used as ADC input */
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_4, LL_GPIO_MODE_ANALOG);

	/* Configure GPIO in analog mode to be used as ADC input */
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_ANALOG);

	/*## Configuration of NVIC #################################################*/
	/* Configure NVIC to enable ADC1 interruptions */
	NVIC_SetPriority(ADC1_COMP_IRQn, 0); /* ADC IRQ greater priority than DMA IRQ */
	NVIC_EnableIRQ(ADC1_COMP_IRQn);

	/*## Configuration of ADC ##################################################*/

	/*## Configuration of ADC hierarchical scope: common to several ADC ########*/

	/* Enable ADC clock (core clock) */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       All ADC instances of the ADC common group must be disabled.        */
	/* Note: In this example, all these checks are not necessary but are        */
	/*       implemented anyway to show the best practice usages                */
	/*       corresponding to reference manual procedure.                       */
	/*       Software can be optimized by removing some of these checks, if     */
	/*       they are not relevant considering previous settings and actions    */
	/*       in user application.                                               */
	if (__LL_ADC_IS_ENABLED_ALL_COMMON_INSTANCE() == 0) {
		/* Note: Call of the functions below are commented because they are       */
		/*       useless in this example:                                         */
		/*       setting corresponding to default configuration from reset state. */

		/* Set ADC clock (conversion clock) common to several ADC instances */
		/* Note: On this STM32 serie, ADC common clock asynchonous prescaler      */
		/*       is applied to each ADC instance if ADC instance clock is         */
		/*       set to clock source asynchronous                                 */
		/*       (refer to function "LL_ADC_SetClock()" below).                   */
		// LL_ADC_SetCommonClock(__LL_ADC_COMMON_INSTANCE(ADC1), LL_ADC_CLOCK_ASYNC_DIV1);
		/* Set ADC measurement path to internal channels */
		LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
				(LL_ADC_PATH_INTERNAL_VREFINT | LL_ADC_PATH_INTERNAL_TEMPSENSOR));

		/* Delay for ADC temperature sensor stabilization time.                   */
		/* Compute number of CPU cycles to wait for, from delay in us.            */
		/* Note: Variable divided by 2 to compensate partially                    */
		/*       CPU processing cycles (depends on compilation optimization).     */
		/* Note: If system core clock frequency is below 200kHz, wait time        */
		/*       is only a few CPU processing cycles.                             */
		/* Note: This delay is implemented here for the purpose in this example.  */
		/*       It can be optimized if merged with other delays                  */
		/*       during ADC activation or if other actions are performed          */
		/*       in the meantime.                                                 */
		wait_loop_index = ((LL_ADC_DELAY_TEMPSENSOR_STAB_US
				* (SystemCoreClock / (100000 * 2))) / 10);
		while (wait_loop_index != 0) {
			wait_loop_index--;
		}

		/*## Configuration of ADC hierarchical scope: multimode ####################*/

		/* Note: Feature not available on this STM32 serie */

	}

	/*## Configuration of ADC hierarchical scope: ADC instance #################*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled.                                              */
	if (LL_ADC_IsEnabled(ADC1) == 0) {
		/* Note: Call of the functions below are commented because they are       */
		/*       useless in this example:                                         */
		/*       setting corresponding to default configuration from reset state. */

		/* Set ADC clock (conversion clock) */
		LL_ADC_SetClock(ADC1, LL_ADC_CLOCK_SYNC_PCLK_DIV2);

		/* Set ADC data resolution */
		// LL_ADC_SetResolution(ADC1, LL_ADC_RESOLUTION_12B);
		/* Set ADC conversion data alignment */
		// LL_ADC_SetResolution(ADC1, LL_ADC_DATA_ALIGN_RIGHT);
		/* Set ADC low power mode */
		// LL_ADC_SetLowPowerMode(ADC1, LL_ADC_LP_MODE_NONE);
		/* Set ADC channels sampling time */
		/* Note: On this STM32 serie, sampling time is common to all channels     */
		/*       of the entire ADC instance.                                      */
		/*       Therefore, sampling time is configured here under ADC instance   */
		/*       scope (not under channel scope as on some other STM32 devices    */
		/*       on which sampling time is channel wise).                         */
		/* Note: Considering interruption occurring after each ADC group          */
		/*       regular sequence conversions                                     */
		/*       (IT from DMA transfer complete),                                 */
		/*       select sampling time and ADC clock with sufficient               */
		/*       duration to not create an overhead situation in IRQHandler.      */
		LL_ADC_SetSamplingTimeCommonChannels(ADC1,
		LL_ADC_SAMPLINGTIME_160CYCLES_5);

	}

	/*## Configuration of ADC hierarchical scope: ADC group regular ############*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled or enabled without conversion on going        */
	/*       on group regular.                                                  */
	if ((LL_ADC_IsEnabled(ADC1) == 0)
			|| (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
		/* Set ADC group regular trigger source */
		LL_ADC_REG_SetTriggerSource(ADC1, LL_ADC_REG_TRIG_SOFTWARE);

		/* Set ADC group regular trigger polarity */
		// LL_ADC_REG_SetTriggerEdge(ADC1, LL_ADC_REG_TRIG_EXT_RISING);
		/* Set ADC group regular continuous mode */
		LL_ADC_REG_SetContinuousMode(ADC1, LL_ADC_REG_CONV_SINGLE);

		/* Set ADC group regular conversion data transfer */
		LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);

		/* Set ADC group regular overrun behavior */
		LL_ADC_REG_SetOverrun(ADC1, LL_ADC_REG_OVR_DATA_OVERWRITTEN);

		/* Set ADC group regular sequencer */
		/* Note: On this STM32 serie, ADC group regular sequencer is              */
		/*       not fully configurable: sequencer length and each rank           */
		/*       affectation to a channel are fixed by channel HW number.         */
		/*       Refer to description of function                                 */
		/*       "LL_ADC_REG_SetSequencerChannels()".                             */
		/*       Case of STM32L0xx:                                               */
		/*       ADC Channel ADC_CHANNEL_VREFINT is on ADC channel 17,            */
		/*       there is 1 other channel enabled with lower channel number.      */
		/*       Therefore, ADC_CHANNEL_VREFINT will be converted by the          */
		/*       sequencer as the 2nd rank.                                       */
		/*       ADC Channel ADC_CHANNEL_TEMPSENSOR is on ADC channel 18,         */
		/*       there are 2 other channels enabled with lower channel number.    */
		/*       Therefore, ADC_CHANNEL_TEMPSENSOR will be converted by the       */
		/*       sequencer as the 3rd rank.                                       */
		/* Set ADC group regular sequencer discontinuous mode */
		// LL_ADC_REG_SetSequencerDiscont(ADC1, LL_ADC_REG_SEQ_DISCONT_DISABLE);
		/* Set ADC group regular sequence: channel on rank corresponding to       */
		/* channel number.                                                        */
		LL_ADC_REG_SetSequencerChannels(ADC1,
				LL_ADC_CHANNEL_4 | LL_ADC_CHANNEL_7 | LL_ADC_CHANNEL_VREFINT
						| LL_ADC_CHANNEL_TEMPSENSOR);
	}

	/*## Configuration of ADC hierarchical scope: ADC group injected ###########*/

	/* Note: Feature not available on this STM32 serie */

	/*## Configuration of ADC hierarchical scope: channels #####################*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled or enabled without conversion on going        */
	/*       on either groups regular or injected.                              */
	if ((LL_ADC_IsEnabled(ADC1) == 0)
			|| (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
		/* Set ADC channels sampling time */
		/* Note: On this STM32 serie, sampling time is common to all channels     */
		/*       of the entire ADC instance.                                      */
		/*       See sampling time configured above, at ADC instance scope.       */

	}

	/*## Configuration of ADC transversal scope: analog watchdog ###############*/

	/* Note: On this STM32 serie, there is only 1 analog watchdog available.    */

	/* Set ADC analog watchdog: channels to be monitored */
// LL_ADC_SetAnalogWDMonitChannels(ADC1, LL_ADC_AWD_DISABLE);
	/* Set ADC analog watchdog: thresholds */
// LL_ADC_ConfigAnalogWDThresholds(ADC1, __LL_ADC_DIGITAL_SCALE(LL_ADC_RESOLUTION_12B), 0x000);
	/*## Configuration of ADC transversal scope: oversampling ##################*/

	/* Set ADC oversampling scope */
// LL_ADC_SetOverSamplingScope(ADC1, LL_ADC_OVS_DISABLE);
	/* Set ADC oversampling parameters */
// LL_ADC_ConfigOverSamplingRatioShift(ADC1, LL_ADC_OVS_RATIO_2, LL_ADC_OVS_SHIFT_NONE);
	/*## Configuration of ADC interruptions ####################################*/
	/* Enable interruption ADC group regular end of sequence conversions */
	LL_ADC_EnableIT_EOS(ADC1);

	/* Enable interruption ADC group regular overrun */
	LL_ADC_EnableIT_OVR(ADC1);

	/* Note: in this example, ADC group regular end of conversions              */
	/*       (number of ADC conversions defined by DMA buffer size)             */
	/*       are notified by DMA transfer interruptions).                       */
	/*       ADC interruptions of end of conversion are enabled optionally,     */
	/*       as demonstration purpose in this example.                          */

	/* USER CODE END ADC_Init 2 */

}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void) {

	/* USER CODE BEGIN I2C1_Init 0 */

	/* USER CODE END I2C1_Init 0 */

	/* USER CODE BEGIN I2C1_Init 1 */

	/* USER CODE END I2C1_Init 1 */
	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = 0x00303D5B;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Analogue filter
	 */
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Configure Digital filter
	 */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C1_Init 2 */

	/* USER CODE END I2C1_Init 2 */

}

/**
 * @brief LPTIM1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_LPTIM1_Init(void) {

	/* USER CODE BEGIN LPTIM1_Init 0 */

	/* USER CODE END LPTIM1_Init 0 */

	/* USER CODE BEGIN LPTIM1_Init 1 */

	/* USER CODE END LPTIM1_Init 1 */
	hlptim1.Instance = LPTIM1;
	hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
	hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
	hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
	hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
	hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
	hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
	if (HAL_LPTIM_Init(&hlptim1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN LPTIM1_Init 2 */

	/* USER CODE END LPTIM1_Init 2 */

}

/**
 * @brief LPUART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_LPUART1_UART_Init(void) {

	/* USER CODE BEGIN LPUART1_Init 0 */

	/* USER CODE END LPUART1_Init 0 */

	/* USER CODE BEGIN LPUART1_Init 1 */

	/* USER CODE END LPUART1_Init 1 */
	hlpuart1.Instance = LPUART1;
	hlpuart1.Init.BaudRate = 9600;
	hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
	hlpuart1.Init.StopBits = UART_STOPBITS_1;
	hlpuart1.Init.Parity = UART_PARITY_NONE;
	hlpuart1.Init.Mode = UART_MODE_TX_RX;
	hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&hlpuart1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN LPUART1_Init 2 */

	/* USER CODE END LPUART1_Init 2 */

}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void) {

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	LL_USART_InitTypeDef USART_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
	/**USART1 GPIO Configuration
	 PB6   ------> USART1_TX
	 PB7   ------> USART1_RX
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USART1 interrupt Init */
	NVIC_SetPriority(USART1_IRQn, 0);
	NVIC_EnableIRQ(USART1_IRQn);

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	USART_InitStruct.BaudRate = 9600;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART1, &USART_InitStruct);
	LL_USART_ConfigAsyncMode(USART1);
	LL_USART_Enable(USART1);
	/* USER CODE BEGIN USART1_Init 2 */
	/* Polling USART initialisation */
	while ((!(LL_USART_IsActiveFlag_TEACK(USART1)))
			|| (!(LL_USART_IsActiveFlag_REACK(USART1)))) {
	}
	LL_USART_EnableIT_RXNE(USART1);
	/* USER CODE END USART1_Init 2 */

}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void) {

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 9600;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/**
 * @brief USART4 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART4_UART_Init(void) {

	/* USER CODE BEGIN USART4_Init 0 */

	/* USER CODE END USART4_Init 0 */

	LL_USART_InitTypeDef USART_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART4);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
	/**USART4 GPIO Configuration
	 PA0   ------> USART4_TX
	 PA1   ------> USART4_RX
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* USART4 interrupt Init */
	NVIC_SetPriority(USART4_5_IRQn, 0);
	NVIC_EnableIRQ(USART4_5_IRQn);

	/* USER CODE BEGIN USART4_Init 1 */

	/* USER CODE END USART4_Init 1 */
	USART_InitStruct.BaudRate = 115200;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART4, &USART_InitStruct);
	LL_USART_ConfigAsyncMode(USART4);
	LL_USART_Enable(USART4);
	/* USER CODE BEGIN USART4_Init 2 */
	/* Polling USART initialisation */
	while ((!(LL_USART_IsActiveFlag_TEACK(USART4)))
			|| (!(LL_USART_IsActiveFlag_REACK(USART4)))) {
	}
	LL_USART_EnableIT_RXNE(USART4);
	/* USER CODE END USART4_Init 2 */

}

/**
 * @brief USART5 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART5_UART_Init(void) {

	/* USER CODE BEGIN USART5_Init 0 */

	/* USER CODE END USART5_Init 0 */

	LL_USART_InitTypeDef USART_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART5);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
	/**USART5 GPIO Configuration
	 PB3   ------> USART5_TX
	 PB4   ------> USART5_RX
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_3;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USART5 interrupt Init */
	NVIC_SetPriority(USART4_5_IRQn, 0);
	NVIC_EnableIRQ(USART4_5_IRQn);

	/* USER CODE BEGIN USART5_Init 1 */

	/* USER CODE END USART5_Init 1 */
	USART_InitStruct.BaudRate = 9600;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART5, &USART_InitStruct);
	LL_USART_ConfigAsyncMode(USART5);
	LL_USART_Enable(USART5);
	/* USER CODE BEGIN USART5_Init 2 */
	/* Polling USART initialisation */
	while ((!(LL_USART_IsActiveFlag_TEACK(USART5)))
			|| (!(LL_USART_IsActiveFlag_REACK(USART5)))) {
	}
	LL_USART_EnableIT_RXNE(USART5);
	/* USER CODE END USART5_Init 2 */

}

/**
 * @brief RTC Initialization Function
 * @param None
 * @retval None
 */
static void MX_RTC_Init(void) {

	/* USER CODE BEGIN RTC_Init 0 */
	/*	 Enable Power Control clock
	 __HAL_RCC_PWR_CLK_ENABLE();

	 Enable Ultra low power mode
	 HAL_PWREx_EnableUltraLowPower();

	 Enable the fast wake up from Ultra low power mode
	 HAL_PWREx_EnableFastWakeUp();*/
	/* USER CODE END RTC_Init 0 */

	/* USER CODE BEGIN RTC_Init 1 */

	/* USER CODE END RTC_Init 1 */
	/** Initialize RTC Only
	 */
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 0x7C;
	hrtc.Init.SynchPrediv = 0x127;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

	/** Enable the WakeUp
	 */

	/* USER CODE BEGIN RTC_Init 2 */

	/* USER CODE END RTC_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void) {

	/* USER CODE BEGIN TIM2_Init 0 */
	/* Compute the prescaler value to have TIMx counter clock equal to 2500 Hz */
	uwPrescalerValue = (uint32_t) (SystemCoreClock / 2500) - 1;
	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 0;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 65535;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;

	/* USER CODE BEGIN TIM2_Init 2 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = uwPrescalerValue;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 2500 - 1;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	HAL_TIM_Base_Start_IT(&htim2);
	/* USER CODE END TIM2_Init 2 */

}

/**
 * @brief WWDG Initialization Function
 * @param None
 * @retval None
 */
static void MX_WWDG_Init(void) {

	/* USER CODE BEGIN WWDG_Init 0 */

	/* USER CODE END WWDG_Init 0 */

	/* USER CODE BEGIN WWDG_Init 1 */

	/* USER CODE END WWDG_Init 1 */
	hwwdg.Instance = WWDG;
	hwwdg.Init.Prescaler = WWDG_PRESCALER_1;
	hwwdg.Init.Window = 64;
	hwwdg.Init.Counter = 64;
	hwwdg.Init.EWIMode = WWDG_EWI_DISABLE;
	/* USER CODE BEGIN WWDG_Init 2 */

	/* USER CODE END WWDG_Init 2 */

}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) {

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE()
	;

	/* DMA interrupt init */
	/* DMA1_Channel1_IRQn interrupt configuration */
	NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE()
	;
	__HAL_RCC_GPIOH_CLK_ENABLE()
	;
	__HAL_RCC_GPIOA_CLK_ENABLE()
	;
	__HAL_RCC_GPIOB_CLK_ENABLE()
	;

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LED_EX_GPIO_Port, LED_EX_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GPRS_RST_Pin | WISOL_WKP_Pin | WISOL_RST_Pin,
			GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA,
			GPIO_LED_Pin | EN_US_Pin | EN_GPRS_Pin | EN_GPS_Pin | EN_BLE_Pin
					| EN_SFOX_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPRS_PWR_ON_GPIO_Port, GPRS_PWR_ON_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : LED_EX_Pin */
	GPIO_InitStruct.Pin = LED_EX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LED_EX_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PC14 */
	GPIO_InitStruct.Pin = GPIO_PIN_14;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : INC_SEN_Pin */
	GPIO_InitStruct.Pin = INC_SEN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(INC_SEN_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PH0 PH1 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	/*Configure GPIO pin : PA5 */
	GPIO_InitStruct.Pin = GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : HC_SR04_TRG_Pin */
	GPIO_InitStruct.Pin = HC_SR04_TRG_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(HC_SR04_TRG_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PB0 PB1 PB12 PB13 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_12 | GPIO_PIN_13;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : GPRS_RST_Pin */
	GPIO_InitStruct.Pin = GPRS_RST_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPRS_RST_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : WISOL_WKP_Pin WISOL_RST_Pin GPRS_PWR_ON_Pin */
	GPIO_InitStruct.Pin = WISOL_WKP_Pin | WISOL_RST_Pin | GPRS_PWR_ON_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : GPIO_LED_Pin */
	GPIO_InitStruct.Pin = GPIO_LED_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIO_LED_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : EN_US_Pin EN_GPRS_Pin EN_GPS_Pin EN_BLE_Pin
	 EN_SFOX_Pin */
	GPIO_InitStruct.Pin = EN_US_Pin | EN_GPRS_Pin | EN_GPS_Pin | EN_BLE_Pin
			| EN_SFOX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

//*********************** ADC FUNCTIONS ***************************
void fn_call_adc_conversion() {
	Activate_ADC();
	if (ubDmaTransferStatus != 0) {
		ubDmaTransferStatus = 0;
	}
	if ((LL_ADC_IsEnabled(ADC1) == 1) && (LL_ADC_IsDisableOngoing(ADC1) == 0)
			&& (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
		LL_ADC_REG_StartConversion(ADC1);
	}
	/* Note: ADC group regular conversion start is done into push button      */
	/*       IRQ handler, refer to function "UserButton_Callback()".          */

	/* Note: LED state depending on DMA transfer status is set into DMA       */
	/*       IRQ handler, refer to function "DmaTransferComplete()".          */

	/* Note: For this example purpose, number of ADC group regular sequences  */
	/*       completed are stored into variable                               */
	/*       "ubAdcGrpRegularSequenceConvCount"                               */
	/*       (for debug: see variable content into watch window)              */

	/* Note: ADC conversions data are stored into array "aADCxConvertedData"  */
	/*       (for debug: see variable content into watch window).             */
	/*       Each rank of the sequence is at an address of the array:         */
	/*       - aADCxConvertedData[0]: ADC channel set on rank1                */
	/*                                (ADC1 channel 4)                      */
	/*       - aADCxConvertedData[1]: ADC channel set on rank2                */
	/*                                (ADC1 internal channel VrefInt)             */
	/*       - aADCxConvertedData[2]: ADC channel set on rank3                */
	/*                                (ADC1 internal channel temperature sensor)*/

	/* Wait for ADC conversion and DMA transfer completion to process data */

	/* Computation of ADC conversions raw data to physical values             */
	/* using LL ADC driver helper macro.                                      */
	uhADCxConvertedData_VoltageGPIO_mVolt = __LL_ADC_CALC_DATA_TO_VOLTAGE(
			VDDA_APPLI, aADCxConvertedData[0], LL_ADC_RESOLUTION_12B);
	uhADCxConvertedData_VrefInt_mVolt = __LL_ADC_CALC_DATA_TO_VOLTAGE(
			VDDA_APPLI, aADCxConvertedData[2], LL_ADC_RESOLUTION_12B);
	hADCxConvertedData_Temperature_DegreeCelsius = __LL_ADC_CALC_TEMPERATURE(
			VDDA_APPLI, aADCxConvertedData[3], LL_ADC_RESOLUTION_12B);

	/* Optionally, for this example purpose, calculate analog reference       */
	/* voltage (Vref+) from ADC conversion of internal voltage reference      */
	/* VrefInt.                                                               */
	/* This voltage should correspond to value of literal "VDDA_APPLI".       */
	/* Note: This calculation can be performed when value of voltage Vref+    */
	/* is unknown in the application.                                         */
	uhADCxConvertedData_VrefAnalog_mVolt = __LL_ADC_CALC_VREFANALOG_VOLTAGE(
			aADCxConvertedData[2], LL_ADC_RESOLUTION_12B);

	/* Wait for a new ADC conversion and DMA transfer */

}

/**
 * @brief  Perform ADC activation procedure to make it ready to convert
 *         (ADC instance: ADC1).
 * @note   Operations:
 *         - ADC instance
 *           - Run ADC self calibration
 *           - Enable ADC
 *         - ADC group regular
 *           none: ADC conversion start-stop to be performed
 *                 after this function
 *         - ADC group injected
 *           Feature not available                                  (feature not available on this STM32 serie)
 * @param  None
 * @retval None
 */

void Activate_ADC(void) {
	__IO uint32_t wait_loop_index = 0;
	__IO uint32_t backup_setting_adc_dma_transfer = 0;
#if (USE_TIMEOUT == 1)
	uint32_t Timeout = 0; /* Variable used for timeout management */
#endif /* USE_TIMEOUT */

	/*## Operation on ADC hierarchical scope: ADC instance #####################*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled.                                              */
	/* Note: In this example, all these checks are not necessary but are        */
	/*       implemented anyway to show the best practice usages                */
	/*       corresponding to reference manual procedure.                       */
	/*       Software can be optimized by removing some of these checks, if     */
	/*       they are not relevant considering previous settings and actions    */
	/*       in user application.                                               */
	if (LL_ADC_IsEnabled(ADC1) == 0) {
		/* Disable ADC DMA transfer request during calibration */
		/* Note: Specificity of this STM32 serie: Calibration factor is           */
		/*       available in data register and also transfered by DMA.           */
		/*       To not insert ADC calibration factor among ADC conversion data   */
		/*       in array variable, DMA transfer must be disabled during          */
		/*       calibration.                                                     */
		backup_setting_adc_dma_transfer = LL_ADC_REG_GetDMATransfer(ADC1);
		LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_NONE);

		/* Run ADC self calibration */
		LL_ADC_StartCalibration(ADC1);

		/* Poll for ADC effectively calibrated */
#if (USE_TIMEOUT == 1)
		Timeout = ADC_CALIBRATION_TIMEOUT_MS;
#endif /* USE_TIMEOUT */

		while (LL_ADC_IsCalibrationOnGoing(ADC1) != 0) {
#if (USE_TIMEOUT == 1)
			/* Check Systick counter flag to decrement the time-out value */
			if (LL_SYSTICK_IsActiveCounterFlag())
			{
				if(Timeout-- == 0)
				{
					/* Time-out occurred. Set LED to blinking mode */
					LED_Blinking(LED_BLINK_ERROR);
				}
			}
#endif /* USE_TIMEOUT */
		}

		/* Delay between ADC end of calibration and ADC enable.                   */
		/* Note: Variable divided by 2 to compensate partially                    */
		/*       CPU processing cycles (depends on compilation optimization).     */
		wait_loop_index = (ADC_DELAY_CALIB_ENABLE_CPU_CYCLES >> 1);
		while (wait_loop_index != 0) {
			wait_loop_index--;
		}

		/* Restore ADC DMA transfer request after calibration */
		LL_ADC_REG_SetDMATransfer(ADC1, backup_setting_adc_dma_transfer);

		/* Enable ADC */
		LL_ADC_Enable(ADC1);

		/* Poll for ADC ready to convert */
#if (USE_TIMEOUT == 1)
		Timeout = ADC_ENABLE_TIMEOUT_MS;
#endif /* USE_TIMEOUT */

		while (LL_ADC_IsActiveFlag_ADRDY(ADC1) == 0) {
#if (USE_TIMEOUT == 1)
			/* Check Systick counter flag to decrement the time-out value */
			if (LL_SYSTICK_IsActiveCounterFlag())
			{
				if(Timeout-- == 0)
				{
					/* Time-out occurred. Set LED to blinking mode */
					LED_Blinking(LED_BLINK_ERROR);
				}
			}
#endif /* USE_TIMEOUT */
		}

		/* Note: ADC flag ADRDY is not cleared here to be able to check ADC       */
		/*       status afterwards.                                               */
		/*       This flag should be cleared at ADC Deactivation, before a new    */
		/*       ADC activation, using function "LL_ADC_ClearFlag_ADRDY()".       */
	}

	/*## Operation on ADC hierarchical scope: ADC group regular ################*/
	/* Note: No operation on ADC group regular performed here.                  */
	/*       ADC group regular conversions to be performed after this function  */
	/*       using function:                                                    */
	/*       "LL_ADC_REG_StartConversion();"                                    */

	/*## Operation on ADC hierarchical scope: ADC group injected ###############*/
	/* Note: Feature not available on this STM32 serie */

}

/******************************************************************************/
/*   USER IRQ HANDLER TREATMENT                                               */
/******************************************************************************/

/**
 * @brief  DMA transfer complete callback
 * @note   This function is executed when the transfer complete interrupt
 *         is generated
 * @retval None
 */
void AdcDmaTransferComplete_Callback() {
	/* Update status variable of DMA transfer */
	ubDmaTransferStatus = 1;

	/* Set LED depending on DMA transfer status */
	/* - Turn-on if DMA transfer is completed */
	/* - Turn-off if DMA transfer is not completed */

	/* For this example purpose, check that DMA transfer status is matching     */
	/* ADC group regular sequence status:                                       */
	if (ubAdcGrpRegularSequenceConvStatus != 1) {
		AdcDmaTransferError_Callback();
	}

	/* Reset status variable of ADC group regular sequence */
	ubAdcGrpRegularSequenceConvStatus = 0;
}

/**
 * @brief  DMA transfer error callback
 * @note   This function is executed when the transfer error interrupt
 *         is generated during DMA transfer
 * @retval None
 */
void AdcDmaTransferError_Callback() {

}

/**
 * @brief  ADC group regular end of sequence conversions interruption callback
 * @note   This function is executed when the ADC group regular
 *         sequencer has converted all ranks of the sequence.
 * @retval None
 */
void AdcGrpRegularSequenceConvComplete_Callback() {
	/* Update status variable of ADC group regular sequence */
	ubAdcGrpRegularSequenceConvStatus = 1;
	ubAdcGrpRegularSequenceConvCount++;
}

/**
 * @brief  ADC group regular overrun interruption callback
 * @note   This function is executed when ADC group regular
 *         overrun error occurs.
 * @retval None
 */
void AdcGrpRegularOverrunError_Callback(void) {
	/* Note: Disable ADC interruption that caused this error before entering in */
	/*       infinite loop below.                                               */

	/* Disable ADC group regular overrun interruption */
	LL_ADC_DisableIT_OVR(ADC1);

	/* Error from ADC */

}

//************** STM INTERNAL VALUES FUNCTIONS **********************************

void fn_get_stm32_temperature() {
	fn_call_adc_conversion();
	if (hADCxConvertedData_Temperature_DegreeCelsius < 0) {
		st_stm_adc_variables.temperature =
				hADCxConvertedData_Temperature_DegreeCelsius * (-1);
	} else {
		st_stm_adc_variables.temperature =
				hADCxConvertedData_Temperature_DegreeCelsius;
	}
	if (st_stm_adc_variables.temperature >= 100)
		st_stm_adc_variables.temperature /= 10;
}

void fn_get_stm32_volts() {
	fn_call_adc_conversion();
	st_stm_adc_variables.battery = uhADCxConvertedData_VoltageGPIO_mVolt
			* (1.89 / 1.5);
	HAL_Delay(10);
}

uint16_t fn_get_sample_us_angle(void) {
	volatile uint16_t usAngle = 0, numMax = 0, numMin = 4095,
			copyOf_ADCxConvertedData = 0;
	uint8_t numSamples;
	for (numSamples = 0; numSamples < 10; numSamples++) {
		fn_call_adc_conversion();
		HAL_Delay(50);
		copyOf_ADCxConvertedData = aADCxConvertedData[1];
		usAngle += copyOf_ADCxConvertedData;
		if (copyOf_ADCxConvertedData > numMax) {
			numMax = copyOf_ADCxConvertedData;
		}
		if (copyOf_ADCxConvertedData < numMin) {
			numMin = copyOf_ADCxConvertedData;
		}
	}
	copyOf_ADCxConvertedData = (usAngle - numMax - numMin) / 8; // espero que o compilador n�o seja burro aqui
	return copyOf_ADCxConvertedData;
}

void fn_change_baundrate(int Baud_rate) {
	HAL_Delay(200);
	huart4.Instance = USART4;
	huart4.Init.BaudRate = Baud_rate;
	huart4.Init.WordLength = UART_WORDLENGTH_8B;
	huart4.Init.StopBits = UART_STOPBITS_1;
	huart4.Init.Parity = UART_PARITY_NONE;
	huart4.Init.Mode = UART_MODE_TX_RX;
	huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart4.Init.OverSampling = UART_OVERSAMPLING_16;
	huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart4) != HAL_OK) {
		Error_Handler();
	}
	HAL_Delay(1000);
}

void blinkfast(uint8_t times) {
	for (int var = 0; var < times; ++var) {
		LED_ON
		HAL_Delay(10);
		LED_OFF
		HAL_Delay(10);
	}
}

void blink(uint8_t times) {
	for (int var = 0; var < times; ++var) {
		LED_ON
		HAL_Delay(500);
		LED_OFF
		HAL_Delay(500);
	}
}

void blinkSlow(uint8_t times) {
	for (int var = 0; var < times; ++var) {
		LED_ON
		HAL_Delay(100);
		LED_OFF
		HAL_Delay(400);
	}
}

//************************************** PRINT FUCTIONS ****************************

void fn_fprintnumber(uint64_t number) {
	char numbuff[] = "\0";
	utoa(number, numbuff, 10);
	fn_fprint(numbuff);
}

void fn_fprint(char *data) {
	int tamanho = strlen(data);
	char new_command[tamanho];
	strcpy(new_command, data);
	char new_com[1]; // (uint8_t*)new_command;
	for (int var = 0; var < tamanho; ++var) {
		new_com[0] = new_command[var];
		HAL_UART_Transmit(&hlpuart1, (uint8_t*) new_com, 1, 20);
	}
}

void fn_print_sensor_values() {
	char USangleBUFF[3] = { 0 };
	char distanceBUFF[3] = { 0 };
	char latitudeBUFF[7] = { 0 };
	char longitiudeBUFF[7] = { 0 };
	char batteryBUFF[3] = { 0 };
	char temperatureBUFF[2] = { 0 };

	utoa(st_data_sensor_e.us_angle, USangleBUFF, 10);
	utoa(st_data_sensor_e.distance, distanceBUFF, 10);
	utoa(st_data_sensor_e.temperature, temperatureBUFF, 10);
	utoa((st_data_sensor_e.battery + 255), batteryBUFF, 10);
	utoa(st_data_sensor_e.latitude, latitudeBUFF, 10);
	utoa(st_data_sensor_e.longitude, longitiudeBUFF, 10);

	HAL_Delay(500);
	fn_fprint("\r\n");
	fn_fprint("********* SENSORS VALUES ***********");
	fn_fprint("\r\n");

	fn_fprint("ANGLE OF US_SENSOR: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) USangleBUFF, 3, 100);
	HAL_Delay(50);
	fn_fprint(" GRAUS\r\n");

	fn_fprint("DISTANCE: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) distanceBUFF, 3, 100);
	HAL_Delay(50);
	fn_fprint(" CM\r\n");

	fn_fprint("BLE DISTANCE: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) ble_distance, 2, 100);
	HAL_Delay(50);
	fn_fprint(" [HEXA]\r\n");

	fn_fprint("TEMPERATURE: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) temperatureBUFF, 2, 100);
	HAL_Delay(50);
	fn_fprint(" CELSIUS\r\n");

	fn_fprint("BATTERY: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) batteryBUFF, 3, 100);
	HAL_Delay(50);
	fn_fprint(" VOLTS\r\n");

	fn_fprint("LATITUDE: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) latitudeBUFF, 7, 100);
	HAL_Delay(50);
	fn_fprint("\r\n");

	fn_fprint("LONGITUDE: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) longitiudeBUFF, 7, 100);
	HAL_Delay(50);
	fn_fprint("\r\n");

	fn_fprint("\r\n########################################\r\n");

	if (st_flag.BLE == 1 && st_flag.en_BLE == 1
			&& e_Current_nina_config_type == 1) {
		fn_send_serial_ble("## SENSORES ##\r\n");

		fn_send_serial_ble("US_ANG\r\n");
		fn_send_serial_ble(USangleBUFF);
		fn_send_serial_ble("BAT\r\n");
		fn_send_serial_ble(batteryBUFF);
		fn_send_serial_ble("TMP\r\n");
		fn_send_serial_ble(temperatureBUFF);
		fn_send_serial_ble("DIS\r\n");
		fn_send_serial_ble(distanceBUFF);
		fn_send_serial_ble("BL_DIS\r\n");
		fn_send_serial_ble(longitiudeBUFF);
		fn_send_serial_ble("LAT\r\n");
		fn_send_serial_ble(latitudeBUFF);
		fn_send_serial_ble("LNG\r\n");
		fn_send_serial_ble(longitiudeBUFF);
		fn_send_serial_ble("** FIM **\r\n");

	}

}


void fn_print_lsm_values() {

}

void fn_print_enable_status() {
	fn_fprint("\r\n***ENABLE STATUS***\r\n");

	if (st_flag.en_BLE)
		fn_fprint("BLE = ON\r\n");
	else
		fn_fprint("BLE = OFF\r\n");

	if (st_flag.en_GPS)
		fn_fprint("GPS = ON\r\n");
	else
		fn_fprint("GPS = OFF\r\n");

	if (st_flag.en_tipper)
		fn_fprint("TIPPER = ON\r\n");
	else
		fn_fprint("TIPPER = OFF\r\n");

	fn_fprint("\r\n---------------------\r\n");

	fn_fprint("\r\nTIMER KEEP ALIVE: ");
	fn_fprintnumber(KeepAlive_Timer);

	fn_fprint("\r\nTIMER COUNTER: ");
	fn_fprintnumber(TimeCounter);

	fn_fprint("\r\n---------------------\r\n");

}

// **************************** CALLBACK FUNCTIONS **************************************

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	UartReady = SET;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	TimeCounter++;
	TimerDelay++;

	if (TimeCounter >= (SECONS_ONE_DAY - 100)) {
		st_flag.location_CELL = 0;
		st_flag.location_GPS = 0;
		TimeCounter = 0;
	}

	if ((e_Current_sara_sequence > 2) && (GPRS_Timer_EN > 0))
		GPRS_Timer_EN--;

	if (TimeCounter > (SECONS_ONE_DAY * 2))
		HAL_WWDG_Init(&hwwdg);
}

/**
 * @brief  RTC Wake Up callback
 * @param  None
 * @retval None
 */
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc) {
	/* Clear Wake Up Flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	/* Clear Wake Up Flag */
}

//*********************** AUXILIAR FUNCTIONS ***************************

void fn_check_communication_status_program() {

	fn_fprint("\r\n____________________________ \r\n");

	fn_fprint("\r\n\r\n\r\n*****  STATUS COMMUNICATION RESUME *****\r\n");

	fn_fprint("\r\nGPRS MODULE");
	uint8_t gprs_enable = fn_check_sarag450_status();
	switch (gprs_enable) {
	case 0:
		fn_fprint("\r\nGPRS - NOT FOUND\r\n");
		break;
	case 1:
		fn_fprint("\r\nGPRS - OK\r\n");
		fn_fprint("\r\nSIM CARD GPRS - NOT FOUND\r\n");
		break;
	case 2:

		fn_fprint("\r\nGPRS - OK\r\n");
		fn_fprint("\r\nSIM CARD GPRS - OK\r\n");
		break;
	}

	fn_fprint("\r\nBLE MODULE\r\n");
	st_flag.BLE = fn_ckeck_Nina_status();
	if (st_flag.BLE)
		fn_fprint("\r\n BLE - OK\r\n");
	else
		fn_fprint("\r\n BLE - NOT FOUND\r\n");

	if (gprs_enable != 2) {
		fn_fprint("\r\nRESERCHING GPRS MODULES\r\n");
		gprs_enable = fn_check_sarag450_status();
		switch (gprs_enable) {
		case 0:
			fn_fprint("\r\nGPRS - NOT FOUND\r\n");
			break;
		case 1:

			fn_fprint("\r\nGPRS - OK\r\n");
			fn_fprint("\r\nSIM CARD GPRS - NOT FOUND\r\n");

			break;
		case 2:

			fn_fprint("\r\nGPRS - OK\r\n");
			fn_fprint("\r\nSIM CARD GPRS - OK\r\n");

			break;
		}
	}
	if (!st_flag.BLE) {
		fn_fprint("\r\nRESERCHING BLE MODULES\r\n");
		st_flag.BLE = fn_ckeck_Nina_status();
		if (st_flag.BLE == 1)
			fn_fprint("\r\n BLE - OK\r\n");
		else {
			fn_fprint("\r\n BLE - NOT FOUND\r\n");
			st_flag.erro = 1;
		}
	}

	fn_fprint("\r\n*******************END****************\r\n");
	fn_fprint("\r\n__________________________________________\r\n\r\n");

}

void fn_check_sensors_status_program() {
	fn_fprint("\r\n**** STATUS SENSORS MODULES RESUME ****\r\n");

	fn_fprint("\r\nSERCHING GPS");
	st_flag.GPS = fn_check_camM8_status();
	if (st_flag.GPS == 1)
		fn_fprint("\r\nGPS CAM M8 - OK");
	else
		fn_fprint("\r\nGPS CAM M8 - NOT FOUND");

	fn_fprint("\r\nSERCHING ULTRASONIC SENSOR");
	st_flag.ultrassonic = fn_check_sen031x_status();
	if (st_flag.ultrassonic == 1)
		fn_fprint("\r\nSEN ULTRSSONIC - OK");
	else
		fn_fprint("\r\nSEN ULTRSSONIC - NOT FOUND");

	if (st_flag.GPS == 0) {
		fn_fprint("\r\nRESERCHING GPS");
		st_flag.GPS = fn_check_camM8_status();
		if (st_flag.GPS == 1)
			fn_fprint("\r\nGPS CAM M8 - OK");
		else {
			fn_fprint("\r\nGPS CAM M8 - NOT FOUND");
			st_flag.erro = 1;
		}
	}

	if (st_flag.ultrassonic == 0) {
		fn_fprint("\r\nRESERCHING ULTRASONIC SENSOR");
		st_flag.ultrassonic = fn_check_sen031x_status();
		if (st_flag.ultrassonic == 1)
			fn_fprint("\r\nSEN ULTRSSONIC - OK");
		else {
			fn_fprint("\r\nSEN ULTRSSONIC - NOT FOUND");
			st_flag.erro = 1;
		}
	}

	fn_fprint("\r\n*****************END*********************\r\n");
	fn_fprint("\r\n_________________________________________\r\n\r\n\r\n");
}

//*********************** PAYLOAD FUNCTIONS ***************************

void fn_send_payload() {

	if (st_flag.tipper_detect == 1) {
		fn_fprint("\r\n********** PAYLOAD TRIPPER FRAME *********\r\n");
		st_data_sensor_e.header = 1;
		st_flag.tipper_detect++;
		fn_main_sara();
	} else if ((TimeCounter <= 21600 && downlink_data_ok < 1)
			|| (TimeCounter > 21600 && TimeCounter <= 43200
					&& downlink_data_ok < 2)
			|| (TimeCounter > 43200 && TimeCounter <= 64800
					&& downlink_data_ok < 3)
			|| (TimeCounter > 64800 && downlink_data_ok < 4)) {
		fn_fprint("\r\n********** PAYLOAD CONFIG FRAME *********\r\n");
		st_data_sensor_e.header = 2;
		fn_main_sara();
		if (st_flag.new_downlink) {
			downlink_data_ok++;
			st_flag.new_downlink = 0;
			fn_decoder_downlink();
		}

	} else if (TimeCounter >= SECONS_ONE_DAY) {
		fn_fprint("\r\n********** PAYLOAD DAILY FRAME *********\r\n");
		st_data_sensor_e.header = 2;
		downlink_data_ok = 0;
		fn_main_sara();
		if (st_flag.new_downlink) {
			st_flag.new_downlink = 0;
			fn_decoder_downlink();
		}

	} else {
		fn_fprint("\r\n********** PAYLOAD INFO FRAME *********\r\n");
		st_data_sensor_e.header = 0;
		fn_main_sara();
	}

}

//************************* LOW POWER MODE FUNCTIONS *****************

void fn_desable_GPIOs() {

	HAL_TIM_Base_Stop_IT(&htim2);

	SIGFOX_OFF
	GPRS_OFF
	GPRS_PWR_OFF
	LED_OFF
	GPS_OFF
	US_OFF

// GPIO_A is preserved to keep output status unchanged and have
// Interrupt working for waking Up.

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE()
	;
	__HAL_RCC_GPIOA_CLK_ENABLE()
	;
	__HAL_RCC_GPIOB_CLK_ENABLE()
	;

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	GPIO_InitStruct.Pin = GPIO_PIN_All;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;

// GPIO_B and GPIO_C are disabled
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	__HAL_RCC_GPIOA_CLK_DISABLE();
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	__HAL_RCC_GPIOB_CLK_DISABLE();
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
//__HAL_RCC_GPIOC_CLK_DISABLE();

	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	__HAL_RCC_GPIOH_CLK_DISABLE();

	LL_GPIO_InitTypeDef GPIO_InitStructLL = { 0 };

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);

	/**/
	LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_3);

	/**/
	LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_4);

	GPIO_InitStructLL.Pin = LL_GPIO_PIN_3;
	GPIO_InitStructLL.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStructLL.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructLL.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStructLL.Pull = LL_GPIO_PULL_DOWN;
	LL_GPIO_Init(GPIOB, &GPIO_InitStructLL);

	/**/
	GPIO_InitStructLL.Pin = LL_GPIO_PIN_4;
	GPIO_InitStructLL.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStructLL.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructLL.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStructLL.Pull = LL_GPIO_PULL_DOWN;
	LL_GPIO_Init(GPIOB, &GPIO_InitStructLL);

	LL_GPIO_WriteOutputPort(GPIOB, 0);

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE()
	;

	GPIO_InitStruct.Pin = INC_SEN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(INC_SEN_GPIO_Port, &GPIO_InitStruct);

	/*	Configure GPIO pin : BUTTON_TAMPER_Pin
	 GPIO_InitStruct.Pin = BUTTON_TAMPER_Pin;
	 GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	 GPIO_InitStruct.Pull = GPIO_NOPULL;
	 HAL_GPIO_Init(BUTTON_TAMPER_GPIO_Port, &GPIO_InitStruct);

	 EXTI interrupt init
	 HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
	 HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);*/
}

void LPM_Enter_STOP_Mode() {

	lowPowerTimer = KeepAlive_Timer / 10;


	char numbuff[] = "\0";

	fn_fprint("\r\nTIMER CONTER: ");
	utoa(TimeCounter, numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	fn_fprint("\r\nKEEP ALIVE TIMER: ");
	utoa(KeepAlive_Timer, numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	/*fn_fprint("\r\nTIME DELAY: ");
	utoa(TimerDelay, numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);*/

	fn_fprint("\r\nTIME AFTER LPM: ");
	utoa((TimeCounter + KeepAlive_Timer), numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	fn_fprint("\r\nLPM TIME: ");
	utoa(lowPowerTimer, numbuff, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) numbuff, (unsigned) strlen(numbuff),
			100);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 100);

	fn_fprint("\r\nENTER LPM\r\n");



	/* Low Power Mode SETUP */

	fn_desable_GPIOs();

// DeInit peripheral
	/* Peripheral clock disable LPUSART1 HAL */
//	HAL_UART_DeInit(&hlpuart1); // n�o gerou efeito na redu��o do consumo
	/* Peripheral clock disable USART1 LL */
	MX_USART1_UART_DeInit();

	/* Peripheral clock disable USART2 HAL */
	MX_USART2_UART_DeInit();

	/* Peripheral clock disable USART4 LL*/
	MX_USART4_UART_DeInit();

	/* Peripheral clock disable USART5 LL*/
	MX_USART5_UART_DeInit();

	HAL_LPTIM_DeInit(&hlptim1);

	/* Low Power Mode ENTER */
	//uint8_t local_counter_interrupt = 0;

// Configure RTC to wake up after tWait*1000 ms
//uint32_t _time = (((uint32_t)tWait*1000) * 2314)/1000;
//uint32_t _time = ((uint32_t) tWait);
	for (int var = 0; var < lowPowerTimer; var++) {

		TimeCounter += 10;

		if (KeepAlive_Timer == (KeepAlive_Timer - 3600))
			st_flag.tipper_detect = 0;

		if (HAL_RTC_Init(&hrtc) != HAL_OK) {
			Error_Handler();
		}

		//HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 1,RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
		//HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 2585, RTC_WAKEUPCLOCK_RTCCLK_DIV16);
		//HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 2312,RTC_WAKEUPCLOCK_RTCCLK_DIV16);
		if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 9,
		RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK) {
			Error_Handler();
		}

		__HAL_RCC_PWR_CLK_ENABLE(); // Enable Power Control clock
		HAL_PWREx_EnableUltraLowPower(); // Ultra low power mode
		HAL_PWREx_EnableFastWakeUp(); // Fast wake-up for ultra low power mode

		/* Select MSI as system clock source after Wake Up from Stop mode */
		__HAL_RCC_WAKEUPSTOP_CLK_CONFIG(RCC_STOP_WAKEUPCLOCK_MSI);

		// Switch to STOPMode
		HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON,
		PWR_STOPENTRY_WFI);
		// Reinit clocks
		SystemClock_Config();

		// Deactivate RTC wakeUp
		HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

		if (st_flag.en_tipper && !st_flag.tipper_detect) {
			if ((HAL_GPIO_ReadPin(INC_SEN_GPIO_Port, INC_SEN_Pin)
					&& !st_data_sensor_e.device_pos)
					|| (!HAL_GPIO_ReadPin(INC_SEN_GPIO_Port, INC_SEN_Pin)
							&& st_data_sensor_e.device_pos)) {
				//local_counter_interrupt++;
				//if (local_counter_interrupt == 2) {
					st_flag.tipper_detect = 1;
					st_flag.location_CELL = 0;
					st_flag.location_GPS =0;
					break;
				}
			/*} else
				local_counter_interrupt = 0;*/
		}

		if (TimeCounter >= SECONS_ONE_DAY) {
			break;
		}
	}

}

void LPM_Exit_STOP_Mode(void) {
	/* Low Power Mode RESUME */
	/*// Reinit clocks
	 SystemClock_Config();

	 // Deactivate RTC wakeUp
	 HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);*/

	MX_GPIO_Init();

// Init LPUART
//	HAL_UART_Init(&hlpuart1);
// Reinit LPUART
	HAL_UART_MspInit(&hlpuart1);
	MX_LPUART1_UART_Init();

	MX_USART1_UART_Init();
	HAL_LPTIM_Init(&hlptim1);
	LL_USART_Enable(USART1);
	HAL_UART_Init(&huart2);
	MX_USART4_UART_Init();
	MX_USART5_UART_Init();
	MX_TIM2_Init();
	TimerDelay = 0;
	/*	TimeCounter += KeepAlive_Timer;
	 buffer_time_delay = 0;*/
	fn_fprint("\r\nexit LPM \r\n");
	fn_fprint("\r\n___________________\r\n");
	/* Low Power Mode END */
}

static void MX_USART1_UART_DeInit(void) {
	/* Peripheral clock enable */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

	LL_USART_DeInit(USART1);
	LL_USART_Disable(USART1);
	LL_USART_DisableIT_RXNE(USART1);
}

static void MX_USART2_UART_DeInit(void) {
	HAL_UART_DeInit(&huart2);
}

static void MX_USART4_UART_DeInit(void) {
	/* Peripheral clock Disable */
	LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_USART4);

	LL_USART_DeInit(USART4);
	LL_USART_Disable(USART4);
	LL_USART_DisableIT_RXNE(USART4);
}

static void MX_USART5_UART_DeInit(void) {
	/* Peripheral clock Disable */
	LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_USART5);

	LL_USART_DeInit(USART5);
	LL_USART_Disable(USART5);
	LL_USART_DisableIT_RXNE(USART5);
}

void LedUserInstallationInterface(void) {
	uint16_t nCount; // contagem de amostras repetidas para valida��o do �ngulo
	uint8_t pPosition = st_data_sensor_e.device_pos; // inicializa com a posi��o atual
	uint8_t pAngle = st_data_sensor_e.us_angle; // inicializa com a posi��o atual
	uint16_t pDistance = st_data_sensor_e.distance;

// pisca LED r�pido 10x
	blinkSlow(5);
// inicia contagem de timeout (5min chapado)
	msCount = 0;
	nCount = 0;
	while ((msCount < 300000) && (nCount < 10)) {
		// obt�m a posi��o de instala��o do dispositivo
		if (st_flag.acelerometer == 1) {
			fn_get_installation_position_ahtr(); // obt�m uma nova detec��o da posi��o atual
		} else {
			if (st_flag.acelerometer == 2) {
				fn_get_installation_position_agr(); // obt�m uma nova detec��o da posi��o atual
			}
		}
		// verifica se a posi��o detectada � igual � anterior
		if ((st_data_sensor_e.device_pos == pPosition)
				&& (st_data_sensor_e.device_pos)) {
			nCount++;
			blinkSlow(2); // sinaliza posi��o conhecida
			HAL_Delay(1000);
			fn_fprintnumber(st_data_sensor_e.device_pos);
			fn_fprint(" Posicao detectada IGUAL a anterior.\r\n");
		} else {
			blinkSlow(1); // sinaliza posi��o desconhecida
			HAL_Delay(1000);
			nCount = 0; // reinicia a contagem
			pPosition = st_data_sensor_e.device_pos; // atualiza a posi��o
			if (msCount > 270000) {
				msCount += 30000; // n�o d� mais tempo de coletar 30 medidas de �ngulos
			}
			fn_fprintnumber(st_data_sensor_e.device_pos);
			fn_fprint(
					" Posicao detectada DIFERENTE da anterior ou invalida.\r\n");
		}
	}
	if ((msCount > 300000) || (!pPosition)) {
		// timeout. Reinicia o dispositivo porque n�o reconheceu a posi��o
		while (1) {
			fn_fprint("Posicao detectada NAO RECONHECIDA. Reiniciando...\r\n");

			HAL_WWDG_Init(&hwwdg);
		}
	}
// pPosition tem a posi��o de instala��o usada na calibra��o.

// POSI��O IDENTIFICADA. Segue verifica��o da altura de refer�ncia
	switch (st_data_sensor_e.device_pos) {
	case 1: // instala��o no topo da ca�amba
		// esperado ajuste do �ngulo para 90�
		fn_fprintnumber(st_data_sensor_e.device_pos);
		fn_fprint(" Posicao detectada.\r\n");
		fn_fprint("Obtendo distancia de referencia...\r\n");

		msCount = 0;
		nCount = 0;
		while ((msCount < 300000) && (nCount < 10)) {
			fn_get_us_angle_value(); // tempo estimado de 1 segundo
			fn_get_installation_position(); // obt�m uma nova detec��o da posi��o atual
			if ((st_data_sensor_e.us_angle == 9)
					&& (st_data_sensor_e.device_pos == 1)) { // supondo que deve estar na posi��o de 90�
				nCount++;
				blinkSlow(2); // sinaliza �ngulo correto
				fn_fprintnumber(st_data_sensor_e.us_angle);
				fn_fprint(" Angulo detectado (9).\r\n");
				fn_fprintnumber(st_data_sensor_e.device_pos);
				fn_fprint(" Posicao detectada (1).\r\n");
			} else {
				blinkSlow(1); // sinaliza �ngulo errado
				nCount = 0; // reinicia a contagem
				if (msCount > 270000) {
					msCount += 30000;
				}
				fn_fprintnumber(st_data_sensor_e.us_angle);
				fn_fprint(" Angulo detectado (9).\r\n");
				fn_fprintnumber(st_data_sensor_e.device_pos);
				fn_fprint(" Posicao detectada (1).\r\n");
			}
		}
		if (msCount > 300000) {
			// timeout. Reinicia o dispositivo
			while (1) {
				fn_fprint(
						"Angulo detectado fora do ajuste ou posicao alterada. Reiniciando...\r\n");
				;
				HAL_WWDG_Init(&hwwdg);
			}
		} else {
			pAngle = st_data_sensor_e.us_angle; // anota o �ngulo usado na verifica��o
		}
		break;
	case 2: // instala��o na lateral
		// esperado ajuste do �ngulo para 30�
		msCount = 0;
		nCount = 0;
		while ((msCount < 300000) && (nCount < 10)) {
			fn_get_us_angle_value(); // tempo estimado de 1 segundo
			fn_get_installation_position(); // obt�m uma nova detec��o da posi��o atual
			if ((st_data_sensor_e.us_angle == 3)
					&& (st_data_sensor_e.device_pos == 2)) { // supondo que deve estar na posi��o de 90�
				nCount++;
				blinkSlow(2); // sinaliza �ngulo correto
				fn_fprintnumber(st_data_sensor_e.us_angle);
				fn_fprint(" Angulo detectado (3).\r\n");
				fn_fprintnumber(st_data_sensor_e.device_pos);
				fn_fprint(" Posicao detectada (2).\r\n");
			} else {
				blinkSlow(1); // sinaliza �ngulo errado
				nCount = 0; // reinicia a contagem
				if (msCount > 270000) {
					msCount += 30000;
				}
				fn_fprintnumber(st_data_sensor_e.us_angle);
				fn_fprint(" Angulo detectado (3).\r\n");
				fn_fprintnumber(st_data_sensor_e.device_pos);
				fn_fprint(" Posicao detectada (2).\r\n");
			}
		}
		if (msCount > 300000) {
			// timeout. Reinicia o dispositivo
			while (1) {
				fn_fprint(
						"Angulo detectado fora do ajuste ou posicao alterada. Reiniciando...\r\n");
				;
				HAL_WWDG_Init(&hwwdg);
			}
		} else {
			pAngle = st_data_sensor_e.us_angle; // anota o �ngulo usado na verifica��o
		}
		break;
	default:
		// timeout. Reinicia o dispositivo
		while (1) {
			fn_fprint("Posicao NAO RECONHECIDA. Reiniciando...");
			;
			HAL_WWDG_Init(&hwwdg);
		}
		break;
	}

// pPosition tem a posi��o usada na escolha do �ngulo
// pAngle tem o �ngulo verificado na posi��o inicial

// obt�m amostras de dist�ncia
// repete coleta de amostras at� ter 5 amostras repetidas do volume
	msCount = 0;
	nCount = 0;
	while ((msCount < 300000) && (nCount < 10)) {
		fn_get_volume_value(); // tempo estimado de 1,5 segundos
		if ((st_data_sensor_e.distance - pDistance) < 2) { // toler�ncia de 2cm
			pDistance = (pDistance + st_data_sensor_e.distance) / 2;
			nCount++;
			blinkSlow(2); // sinaliza dist�ncia anotada pr�ximo da anterior
			fn_fprintnumber(st_data_sensor_e.distance);
			fn_fprint(" Distancia detectada.\r\n");
		} else {
			blinkSlow(5); // sinaliza dist�ncia muito diferente da anterior
			nCount = 0; // reinicia a contagem
			fn_fprintnumber(st_data_sensor_e.distance);
			fn_fprint(" Distancia detectada DIVERGENTE.\r\n");
			pDistance = st_data_sensor_e.distance;
			if (msCount > 270000) {
				msCount += 30000;
			}
		}
	}
	if (msCount > 300000) {
		// timeout. Reinicia o dispositivo
		while (1) {
			fn_fprint("Distancia de referencia instavel. Reiniciando...\r\n");
			;
			HAL_WWDG_Init(&hwwdg);
		}
	}
	fn_get_us_angle_value(); // obt�m uma nova detec��o do �ngulo
	fn_get_installation_position();
	if ((pPosition == st_data_sensor_e.device_pos) && (nCount >= 10)
			&& (pAngle == st_data_sensor_e.us_angle)) {
		LED_ON
		HAL_Delay(5000);
		LED_OFF
		fn_fprint("\r\nINSTALACAO FINALIZADA!\r\n");
	} else {
		// tirou da posi��o
		while (1) {
			fn_fprint("Dispositivo removido da posicao calibrada.\r\n");
			;
			HAL_WWDG_Init(&hwwdg);
		}
	}
	__NOP(); // breakpoint here!
}

uint8_t fn_get_installation_position(void) {
	if (st_flag.acelerometer == 1) {
		fn_get_installation_position_ahtr(); // obt�m uma nova detec��o da posi��o atual
	} else {
		if (st_flag.acelerometer == 2) {
			fn_get_installation_position_agr(); // obt�m uma nova detec��o da posi��o atual
		}
	}
	return st_data_sensor_e.device_pos;
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line) {
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
