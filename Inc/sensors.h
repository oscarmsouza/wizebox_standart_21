/*
 * sensors.h
 *
 *  Created on: 16 de mar de 2020
 *      Author: oscar
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define VOL_MIN		20

  /** I2C Device Address 8 bit format**/
#define LSM303AGR_I2C_ADD_XL       0x33U
#define LSM303AH_I2C_ADD_XL       0x3BU
#define LSM303AGR_I2C_ADD_MG       0x3DU


typedef struct {
	//e_uplink_frames_type_t header;
	uint8_t device_pos;
	uint32_t
		//angle,
		us_angle,
		battery,
		distance,
		temperature,
		latitude,
		longitude,
		referenceVol,
		volume,
		header;
} st_sensors_data_t;

st_sensors_data_t st_data_sensor_e;
st_sensors_data_t st_data_sensor_previwes_e;

struct{
	unsigned int
	temperature[6];
}st_samples_values;

struct{
	char
	temperature[4],
	angle[2],
	battery[4],
	latitude[6],
	longitude[6],
	referenceVol[4],
	volume[2],
	distance[4];
}st_sensor_char_values;


void fn_init_sensors();
void fn_get_sensors_values();
void fn_get_angle_value();
void fn_get_volume_value();
void fn_get_latitude_value();
void fn_get_longitude_value();
void fn_get_temperature_value();

uint8_t fn_get_us_angle_value(void);

void fn_get_battery_value();
void fn_get_lat_lon_values();
void fn_get_sensors_char_values();


//**************LSM303 COMMANDS AND OTHERS*********************************


//AH INIT AND COMMAND
uint8_t fn_init_lsm303ah();
void fn_get_lsm303ah();

//AGR INIT AND COMMAND
uint8_t fn_init_lsm303agr();
void fn_get_lsm303agr();

void fn_get_lsm303_and_print(void);
void fn_get_installation_position_agr(void);
void fn_get_installation_position_ahtr(void);

//GLOBAL LSM VARIBLES BELOW

//TEMPERATURE AVAILABLE ONLY ON AGR DEVICES
float temperature_degC;
//ACCELEROMETER VALUES
 struct{
	int16_t a_x, a_y, a_z,pitch_y, pitch, roll, roll_x, yaw, yaw_y;
}st_accelerometer;
//MAGNETOMETER VALUES
 struct{
	int16_t m_x, m_y, m_z;
}st_magnetometer;

int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp, uint16_t len);
int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp, uint16_t len);



#endif /* SENSORS_H_ */
