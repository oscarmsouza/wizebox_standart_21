/*
 * sigfox.h
 *
 *  Created on: 9 de mar de 2020
 *      Author: oscar
 */

#ifndef IOT_H_
#define IOT_H_

#include <main.h>

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define SIGFOX_BYTES_TO_RECEIVER	500

//#define DONGLE_KEY
UART_HandleTypeDef huart1;
UART_HandleTypeDef hlpuart1;

int int_temp_sigfox, int_volt_sigfox;

struct {
	char
	id[8],
	pac[16],
	temperature[3],
	volts[4],
	at[2];
} st_sigfox_parameters;

uint8_t fn_start_iot();

__IO uint8_t ubSend_SIGFOX;
uint8_t aStringToSend_SIGFOX[100];
uint8_t ubSizeToSend_SIGFOX; //sizeof(aStringToSend_SARA);
uint8_t aStringToReceiver_SIGFOX[SIGFOX_BYTES_TO_RECEIVER];
uint8_t ubSizeToReceiver_SIGFOX;
uint8_t flag_sigfox;

//************************ SIGFOX COMMANDS ******************************
int fn_init_sigfox();

uint8_t fn_at_sigfox();
uint8_t fn_get_id_sigfox();
uint8_t fn_get_pac_sigfox();
uint8_t fn_get_volt_sigfox();
uint8_t fn_get_temperature_sigfox();


void fn_send_frame_sigfox(char* frame);

void fn_send_start_frame_sigfox();
void fn_send_report_frame_sigfox();
uint8_t fn_send_daily_frame_sigfox();

void fn_info_sigfox();
void fn_status_sigfox();
void fn_reset_sigfox();

uint8_t fn_send_config_sigfox();
uint8_t fn_set_RX_frequency_sigfox();
uint8_t fn_set_TX_frequency_sigfox();
uint8_t fn_set_save_configuration_sigfox();
uint8_t fn_set_reset_FCC_sigfox();
uint8_t fn_set_reset_module_sigfox();
uint8_t fn_set_dongle_module_sigfox();

uint8_t fn_check_sigfox_status();

//************************ LORA COMMANDS ******************************
uint8_t fn_send_frame_lora();
//************************ UART FN ******************************

void Start_SIGFOX_Transfers(char* sfox_info);
void USART1_CharTransmitComplete_Callback(void);
void USART1_TXEmpty_Callback(void);
void USART1_CharReception_Callback(void);
uint8_t fn_at_uart_reception_sigfox() ;


#endif /* IOT_H_ */
