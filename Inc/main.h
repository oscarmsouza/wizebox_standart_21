/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stm32l0xx_ll_adc.h"
#include "stm32l0xx_hal.h"
#include "stm32l0xx_ll_usart.h"
#include "stm32l0xx_ll_rcc.h"
#include "stm32l0xx_ll_bus.h"
#include "stm32l0xx_ll_cortex.h"
#include "stm32l0xx_ll_system.h"
#include "stm32l0xx_ll_utils.h"
#include "stm32l0xx_ll_pwr.h"
#include "stm32l0xx_ll_gpio.h"
#include "stm32l0xx_ll_dma.h"

#include "stm32l0xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "iot.h"
#include "encoder.h"
#include "saraG450.h"
/*
 */
#include "camM8.h"
#include "sen031x.h"
#include "sensors.h"
#include "program.h"
#include "ninaB1.h"
#include "stm32l0xx_it.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

#define SECONS_ONE_DAY			86400

struct {
	uint8_t ultrassonic, GPS, acelerometer, GPRS, BLE, en_GPS, en_HTTP, en_SMS,
			en_BLE, en_tipper, location_GPS, location_CELL, tipper_detect,
			motion_detect, tilt_sensor, new_downlink, volume_measurement_type,
			erro, wBeacon_slave, alarm, sensors;
} st_flag;

uint8_t downlink_data_ok;

char downlink_data[16];

struct {
	uint32_t temperature, battery;
	uint16_t us_angle;
} st_stm_adc_variables;

struct {
	uint32_t GPSenable, GPRSenable, BLEenable, TRIPPERenable;
} st_timer;

uint32_t TimeCounter;
uint32_t KeepAlive_Timer;

uint16_t GPStimer_EN;
uint32_t BLE_timer_EN;
uint32_t GPRS_Timer_EN;

uint32_t TimerDelay;
uint32_t lowPowerTimer;

__IO uint32_t msCount;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

void blink(uint8_t times);
void blinkSlow(uint8_t times);
void blinkfast(uint8_t times);
void fn_fprint(char *data);
float get_temp(uint32_t variable);
void fn_get_stm32_temperature();
void fn_get_stm32_volts();
void fn_fprintnumber(uint64_t number);
void fn_call_adc_conversion();
void AdcDmaTransferComplete_Callback(void);
void AdcDmaTransferError_Callback(void);
void AdcGrpRegularSequenceConvComplete_Callback(void);
void AdcGrpRegularOverrunError_Callback(void);
void Activate_ADC(void);
void fn_send_payload();
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc);
void fn_print_sensor_values();
void fn_check_sensors_status_program();
void fn_check_communication_status_program();
void fn_enter_lowPowerMode();
void fn_exit_lowPowerMode();
void fn_desable_GPIOs();
void fn_print_timers_values();
void fn_print_lsm_values();
void fn_print_enable_status();
void LPM_Enter_STOP_Mode();
void LPM_Exit_STOP_Mode(void);

uint16_t fn_get_sample_us_angle(void);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_EX_Pin GPIO_PIN_13
#define LED_EX_GPIO_Port GPIOC
#define INC_SEN_Pin GPIO_PIN_15
#define INC_SEN_GPIO_Port GPIOC
#define BLE_RX_Pin GPIO_PIN_0
#define BLE_RX_GPIO_Port GPIOA
#define BLE_TX_Pin GPIO_PIN_1
#define BLE_TX_GPIO_Port GPIOA
#define US_RX_Pin GPIO_PIN_2
#define US_RX_GPIO_Port GPIOA
#define US_TX_Pin GPIO_PIN_3
#define US_TX_GPIO_Port GPIOA
#define V_BAT_FB_Pin GPIO_PIN_4
#define V_BAT_FB_GPIO_Port GPIOA
#define HC_SR04_TRG_Pin GPIO_PIN_6
#define HC_SR04_TRG_GPIO_Port GPIOA
#define US_ANGLE_Pin GPIO_PIN_7
#define US_ANGLE_GPIO_Port GPIOA
#define GPRS_RST_Pin GPIO_PIN_2
#define GPRS_RST_GPIO_Port GPIOB
#define PC_DEBUG_RX_Pin GPIO_PIN_10
#define PC_DEBUG_RX_GPIO_Port GPIOB
#define GPS_TX_Pin GPIO_PIN_11
#define GPS_TX_GPIO_Port GPIOB
#define WISOL_WKP_Pin GPIO_PIN_14
#define WISOL_WKP_GPIO_Port GPIOB
#define WISOL_RST_Pin GPIO_PIN_15
#define WISOL_RST_GPIO_Port GPIOB
#define GPIO_LED_Pin GPIO_PIN_8
#define GPIO_LED_GPIO_Port GPIOA
#define EN_US_Pin GPIO_PIN_9
#define EN_US_GPIO_Port GPIOA
#define EN_GPRS_Pin GPIO_PIN_10
#define EN_GPRS_GPIO_Port GPIOA
#define EN_GPS_Pin GPIO_PIN_11
#define EN_GPS_GPIO_Port GPIOA
#define EN_BLE_Pin GPIO_PIN_12
#define EN_BLE_GPIO_Port GPIOA
#define EN_SFOX_Pin GPIO_PIN_15
#define EN_SFOX_GPIO_Port GPIOA
#define GPRS_RX_Pin GPIO_PIN_3
#define GPRS_RX_GPIO_Port GPIOB
#define GPRS_TX_Pin GPIO_PIN_4
#define GPRS_TX_GPIO_Port GPIOB
#define GPRS_PWR_ON_Pin GPIO_PIN_5
#define GPRS_PWR_ON_GPIO_Port GPIOB
#define SFOX_RX_Pin GPIO_PIN_6
#define SFOX_RX_GPIO_Port GPIOB
#define SFOX_TX_Pin GPIO_PIN_7
#define SFOX_TX_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define LED_ON               	HAL_GPIO_WritePin(GPIO_LED_GPIO_Port,GPIO_LED_Pin,0);
#define LED_OFF              	HAL_GPIO_WritePin(GPIO_LED_GPIO_Port,GPIO_LED_Pin,1);
#define LED_CHANGE           	HAL_GPIO_TogglePin(GPIO_LED_GPIO_Port,GPIO_LED_Pin);

#define GPRS_RESET_ON        	HAL_GPIO_WritePin(GPRS_RST_GPIO_Port,GPRS_RST_Pin,0);
#define GPRS_RESET_OFF       	HAL_GPIO_WritePin(GPRS_RST_GPIO_Port,GPRS_RST_Pin,1);
#define GPRS_ON              	HAL_GPIO_WritePin(EN_GPRS_GPIO_Port,EN_GPRS_Pin,1);
#define GPRS_OFF             	HAL_GPIO_WritePin(EN_GPRS_GPIO_Port,EN_GPRS_Pin,0);
#define GPRS_PWR_ON          	HAL_GPIO_WritePin(GPRS_PWR_ON_GPIO_Port,GPRS_PWR_ON_Pin,1);
#define GPRS_PWR_OFF         	HAL_GPIO_WritePin(GPRS_PWR_ON_GPIO_Port,GPRS_PWR_ON_Pin,0);

#define SIGFOX_ON			 	HAL_GPIO_WritePin(EN_SFOX_GPIO_Port,EN_SFOX_Pin,1);
#define SIGFOX_OFF				HAL_GPIO_WritePin(EN_SFOX_GPIO_Port,EN_SFOX_Pin,0);
#define SIGFOX_RESET_ON			HAL_GPIO_WritePin(EN_SFOX_GPIO_Port,EN_SFOX_Pin,0);
#define SIGFOX_RESET_OFF		HAL_GPIO_WritePin(EN_SFOX_GPIO_Port,EN_SFOX_Pin,1);
#define SIGFOX_WAKEUP_ON		HAL_GPIO_WritePin(EN_SFOX_GPIO_Port,EN_SFOX_Pin,0);
#define SIGFOX_WAKEUP_OFF		HAL_GPIO_WritePin(EN_SFOX_GPIO_Port,EN_SFOX_Pin,1);

#define GPS_ON              	HAL_GPIO_WritePin(EN_GPS_GPIO_Port,EN_GPS_Pin,1);
#define GPS_OFF             	HAL_GPIO_WritePin(EN_GPS_GPIO_Port,EN_GPS_Pin,0);

#define BLE_ON              	HAL_GPIO_WritePin(EN_BLE_GPIO_Port,EN_BLE_Pin,1);
#define BLE_OFF             	HAL_GPIO_WritePin(EN_BLE_GPIO_Port,EN_BLE_Pin,0);

#define US_ON              		HAL_GPIO_WritePin(EN_US_GPIO_Port,EN_US_Pin,1);
#define US_OFF             		HAL_GPIO_WritePin(EN_US_GPIO_Port,EN_US_Pin,0);

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
