/*
 * ninaB1.h
 *
 *  Created on: 6 de abr de 2020
 *      Author: oscar
 */

#ifndef NINAB1_H_
#define NINAB1_H_
#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>





UART_HandleTypeDef huart4;
UART_HandleTypeDef hlpuart1;


#define BYTES_NINA_TO_RECEIVER	512
#define DATA_SIZE_1  100
#define DATA_SIZE_2  100


/*

#define FROMMEM(x)                          ((const char *)(x))
//AT COMMAND DUMMY
#define BLE_AT                              FROMMEM("AT\r")
//ANSWER OK
#define BLE_OK                              FROMMEM("\r\nOK\r\n")
//ANSWER ERRO (NO OK)
#define BLE_ERROR                           FROMMEM("\r\nERROR\r\n")
//
#define BLE_CRLF                            FROMMEM("\r\n")
//GET MILIVOLT
#define BLE_VOLT                            FROMMEM("AT$V?\r")
//GET TEMPERATURE
#define BLE_TEMP                            FROMMEM("AT$T?\r")
//GET DISTANCE (mm)
#define BLE_DIST                            FROMMEM("AT$D?\r")
//GET ALL SENSORS
#define BLE_ALL                             FROMMEM("AT$A?\r")
//GET THE INFORMATION STATUS
#define BLE_INFO                            FROMMEM("AT$I?\r")
//GET THE CONFIGURATION STATUS
#define BLE_CFG 	                     	FROMMEM("AT$CF?\r")
//SET BLE CONFIGURATION (PERIMETRAL OR CENTRA|DATA MODE OR AT MODE)
#define BLE_CFG_P                       	FROMMEM("AT$CF=1\r")
#define BLE_CFG_C                       	FROMMEM("AT$CF=2\r")
//SET BLE LIKE A IBEACON OR EDDYSTONE BEACON
#define BLE_CFG_I_BCN						FROMMEM("AT$CF=3\r")
#define BLE_CFG_E_BCN						FROMMEM("AT$CF=4\r")
//RESET BLE (OFF/ON)
#define BLE_RST							   	FROMMEM("AT$R=1\r")
//BLE FACTORY RESET
#define BLE_RST_FACTORY						FROMMEM("AT$R=0\r")
//GET THE CONFIGURATION DEFAULT PEER
#define BLE_CFG_PEER						FROMMEM("AT$DP=")
//
*/


char responce_nina_buff[BYTES_NINA_TO_RECEIVER];
//char at_data_receiver_ble_to_nina[DATA_SIZE_2];
uint8_t data, flag_nina, flag_default_peer, flag_centra_config;
uint8_t byte_nina, counter_bytes_receiver;
char receiver_nina_info[10];
uint32_t NINAbytesToReceive;
char recever_nina_uart[100];
char peer_adress[13];


typedef enum
{
	OFF_MODE,
	SERIAL_BLE_MODE,
	IBEACON_MODE,
	EDDYSTONE_BEACON_MODE,
	SCAN_BEACON_MODE,
	FACTORY_MODE,
	CENTRAL_MODE,
	PERIMETRAL_MODE,
	CONFIG_MODE,

}e_nina_config_type;

e_nina_config_type e_Current_nina_config_type,
e_Previous_nina_config_type;


typedef struct
{

	char
	ble_nina_id[8],
	batt_nina_value[3],
	dist_nina_value[7],
	temp_nina_value[7],
	othr_nina_value[7];


}st_ble_data_receiver_t;

st_ble_data_receiver_t nina_ble_1;
st_ble_data_receiver_t nina_ble_2;
st_ble_data_receiver_t nina_ble_3;

char ble_distance[2];


void fn_main_nina(void);
uint8_t fn_ckeck_Nina_status();

int fn_factory_reset_nina();

uint8_t fn_send_at_command_nina(char* at_command, int msDelay);
void fn_find_device_rssi(int devices_strength_seguence);


uint8_t fn_set_config_perimetral_nina();
uint8_t fn_set_config_central_nina();

uint8_t fn_receiver_data_at_command_nina();
uint8_t fn_send_data_at_command_nina(char* data_send, char* data_receiver, int dataDelay);
uint8_t fn_send_at_commands_nina(char* command);
uint8_t fn_ask_battery_level(int delay_time_seconds);

uint8_t fn_answer_at_data_mode_nina();

void fn_process_nina_info_receiver(char* information);


uint8_t fn_set_at_mode_nina();
uint8_t fn_set_data_mode_nina();
uint8_t fn_discovery_peer_nina();
uint8_t fn_init_configuration_nina();
uint8_t fn_wait_peer_conection_nina();
uint8_t fn_discovery_devices_nina();
uint8_t fn_at_command_set_central();


void fn_reset_nina(void);
void fn_turn_off_nina(void);
void fn_turn_on_nina(void);
void fn_send_serial_ble(char* data);

void fn_work_perimetral_data_mode_nina(void);


void fn_Change_Nina_Config_Type(e_nina_config_type state) ;

uint8_t fn_at_command_escape_at();
uint8_t fn_at_command_data_mode();

uint8_t fn_at_command_attention();
uint8_t fn_at_command_storing();
uint8_t fn_at_command_factory_reset();
uint8_t fn_at_command_reboot_device();
uint8_t fn_at_command_set_perimetral();
uint8_t fn_at_command_get_serial_number();
uint8_t fn_at_command_get_model_id();
uint8_t fn_at_command_get_manufacture_id() ;
uint8_t fn_at_command_set_new_name(char*name);
uint8_t fn_at_connect_peer();
uint8_t fn_at_disconect_peer();
uint8_t fn_at_command_set_server_config();
uint8_t fn_at_command_set_connectable();
uint8_t fn_at_command_set_discoverable();
uint8_t fn_at_command_set_pairable();
uint8_t fn_at_command_set_wbeacon_name();
uint8_t fn_at_command_set_eddyston_TLM_payload();
uint8_t fn_at_command_set_beacon_payload();
uint8_t fn_at_command_scan_peer();
uint8_t fn_at_commnad_get_configuration_status();
uint8_t fn_find_id_peer(char * init_id);

void StartTransfers(char* info);
void USART_CharTransmitComplete_Callback(void);
void USART_TXEmpty_Callback(void);
void USART_CharReception_Callback(void);
uint8_t fn_at_uart_reception_nina() ;

uint8_t fn_wait_for_connection();

uint8_t fn_data_at_command();
uint8_t fn_data_get_at_temperature();
uint8_t fn_data_get_at_battery();
uint8_t fn_data_get_at_distance();
uint8_t fn_data_get_at_all_sensors();
uint8_t fn_data_get_at_config();
uint8_t fn_data_set_at_central_mode_operation();
uint8_t fn_data_set_at_ibeacon_mode_operation();
uint8_t fn_data_set_at_eddystone_mode_operation();
uint8_t fn_data_set_at_perimetral_mode_operation();

void fn_transfer_lsm_values_ble();

uint8_t fn_receiver_tx();
void fn_serial_data_responce(void);

#endif /* NINAB1_H_ */
