/*
 * saraG450.h
 *
 *  Created on: 30 de mar de 2020
 *      Author: oscar
 */

#ifndef SARAG450_H_
#define SARAG450_H_


#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


UART_HandleTypeDef huart5;
UART_HandleTypeDef hlpuart1;

int sequencialMensage;

char sara_imei[15], sara_RSSI[2], sara_TIMESTAMP[5], sara_SEQNUMBER[3],
		sara_KEEPALIVE[5], SMS_buffer[160], sara_data[24];

typedef enum {
	TURN_OFF_MODE,
	RESET_MODE,
	TURN_ON_MODE,
	INICIALIZATION,
	INFO_MODE,
	QUALITY_SIGNAL_MODE,
	OPERATOR_INFO_MODE,
	NETWORK_OPERATION_MODE,
	CELL_LOCATION_MODE,
	HTTP_MODE,
	SMS_MODE

} e_sara_command_sequence_type;

e_sara_command_sequence_type e_Current_sara_sequence, e_Previous_sara_sequence;


//***************************************************************************************

uint64_t fn_get_imei_sara();
int fn_at_command_sara(char* at_command, unsigned int msDelay);
void fn_power_on_sara(void);
void fn_power_off_sara(void);
void fn_get_module_info_sara(void);

uint8_t fn_start_at_commands_sara();

void fn_mount_frame_sara();
void fn_http_mensage_sara(int type_mensage);
uint8_t fn_check_sarag450_status();

uint8_t fn_network_activation_sara();
uint8_t fn_network_configuration_sara();

void Start_Sara_Transfers(char* info);
void USART5_CharTransmitComplete_Callback(void);
void USART5_TXEmpty_Callback(void);
void USART5_CharReception_Callback(void);
uint8_t fn_at_uart_reception_sara();

uint8_t fn_at_command_saraG450(char* at_command);
uint8_t fn_at_command_word_responce_saraG450(char* at_command, uint32_t msDelay,
		char* responce);


void fn_print_indicator_control(void);
void fn_get_RSSI_QS(uint8_t signal);
void fn_main_sara(void);

uint8_t fn_data_change();

uint8_t fn_indicator_control_command();
uint8_t fn_check_signal_quality_status();
uint8_t fn_check_attach_GPRS();

uint8_t fn_set_GPRS_network_registration_enable();
uint8_t fn_set_Packet_switched_data_action();
uint8_t fn_set_attach_GPRS();
uint8_t fn_send_sms_payload();
uint8_t fn_send_sms();

uint8_t fn_get_operator_information();
uint8_t fn_set_automatic_network_selection();
uint8_t fn_get_list_of_operators();
uint8_t fn_get_registration_status();
uint8_t fn_get_defined_APN(uint8_t profile);
uint8_t fn_get_dynamic_IP(uint8_t profile);
uint8_t fn_get_activation_status(uint8_t profile);
uint8_t fn_activate_PDP_context(uint8_t profile);
uint8_t fn_reset_HTTP_profile(uint8_t profile);
uint8_t fn_set_HTTP_wizeBox_serve_name(uint8_t profile);
uint8_t fn_GET_request_to_wizebox_HTTP_server(uint8_t profile);
uint8_t fn_get_ip_adres(uint8_t profile);
uint8_t fn_set_sms_center();
uint8_t fn_delete_all_read_sms();
uint8_t fn_read_all_sms_mensages();
uint8_t fn_set_preferred_sms_format_text();
uint8_t fn_cellLocate_localization_info();
uint8_t fn_read_data_folder_responseFilename_HTTP();
uint8_t fn_delete_data_folder_responseFilename_HTTP();
uint8_t fn_get_cellLocate_values();
uint8_t fn_check_sms();
uint8_t fn_delete_all_sms();
uint8_t fn_check_new_sms();
uint8_t fn_answer_sms_message();
uint8_t fn_check_sms_upload();

#endif /* SARAG450_H_ */
